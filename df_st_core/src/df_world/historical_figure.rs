use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A notable figure that lived in the world.
/// Not all characters that live in the world are notable.
/// They have to have done or encountered something that made them notable.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct HistoricalFigure {
    /// Identifier for the historical figure.
    /// `id` must be unique for the whole world.
    /// This `id` is often referred to as `hfid`.
    pub id: i32,
    pub name: Option<String>,
    pub race: Option<String>,
    pub race_id: Option<String>, // creature_id
    /// The gender or sex of the figure.
    /// Possible values: female, male, default.
    pub caste: Option<String>, // TODO Change to enum
    pub appeared: Option<i32>,
    pub birth_year: Option<i32>,      // TODO Convert to date
    pub birth_seconds72: Option<i32>, // TODO Convert to date
    pub death_year: Option<i32>,      // TODO Convert to date
    pub death_seconds72: Option<i32>, // TODO Convert to date
    pub associated_type: Option<String>,
    pub entity_link: Vec<HFEntityLink>,
    pub entity_position_link: Vec<HFEntityPositionLink>,
    pub site_link: Vec<HFSiteLink>,
    pub sphere: Vec<String>,
    pub interaction_knowledge: Vec<String>,
    pub deity: Option<String>,
    pub journey_pet: Vec<String>,
    pub goal: Option<String>,
    pub relationship_profile_hf_historical: Vec<HFRelationshipProfileHFHistorical>,
    pub relationship_profile_hf_visual: Vec<HFRelationshipProfileHFVisual>,
    pub intrigue_actor: Vec<HFIntrigueActor>,
    pub intrigue_plot: Vec<HFIntriguePlot>,
    pub ent_pop_id: Option<i32>,
    pub entity_reputation: Vec<HFEntityReputation>,
    pub vague_relationship: Vec<HFVagueRelationship>,
    pub active_interaction: Option<String>,
    pub force: Option<bool>,
    pub current_identity_id: Option<i32>,
    pub entity_squad_link: Vec<HFEntitySquadLink>,
    pub holds_artifact: Option<i32>,
    pub honor_entity: Vec<HFHonorEntity>,
    pub site_property: Vec<HFSiteProperty>,
    pub used_identity_id: Option<i32>,
    pub animated: Option<bool>,
    pub animated_string: Option<String>,

    pub skills: Vec<HFSkill>,
    pub links: Vec<HFLink>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFEntityLink {
    pub hf_id: i32,
    pub entity_id: i32,
    pub link_type: Option<String>,
    pub link_strength: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFEntityPositionLink {
    /// Unique ID for database
    pub id: i32,
    pub hf_id: i32,
    pub entity_id: i32,
    pub position_profile_id: Option<i32>,
    pub start_year: Option<i32>, // TODO Convert to date
    pub end_year: Option<i32>,   // TODO Convert to date
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFSiteLink {
    pub hf_id: i32,
    pub site_id: i32,
    pub link_type: Option<String>,
    pub entity_id: Option<i32>,
    pub occupation_id: Option<i32>,
    pub sub_id: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFSkill {
    pub hf_id: i32,
    pub skill: String, // TODO: change to enum
    pub total_ip: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFRelationshipProfileHFHistorical {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub love: Option<i32>,
    pub respect: Option<i32>,
    pub trust: Option<i32>,
    pub loyalty: Option<i32>,
    pub fear: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFRelationshipProfileHFVisual {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub meet_count: Option<i32>,
    pub last_meet_year: Option<i32>,      // TODO Convert to date
    pub last_meet_seconds72: Option<i32>, // TODO Convert to date
    pub love: Option<i32>,
    pub respect: Option<i32>,
    pub trust: Option<i32>,
    pub loyalty: Option<i32>,
    pub fear: Option<i32>,
    pub known_identity_id: Option<i32>,
    pub rep_friendly: Option<i32>,
    pub rep_information_source: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFIntrigueActor {
    pub hf_id: i32,
    pub local_id: i32,
    pub entity_id: Option<i32>,
    pub hf_id_other: Option<i32>,
    pub role: Option<String>,
    pub strategy: Option<String>,
    pub strategy_en_id: Option<i32>,
    pub strategy_epp_id: Option<i32>,
    pub handle_actor_id: Option<i32>,
    pub promised_actor_immortality: Option<bool>,
    pub promised_me_immortality: Option<bool>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFIntriguePlot {
    pub hf_id: i32,
    pub local_id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub on_hold: Option<bool>,
    pub actor_id: Option<i32>,
    pub artifact_id: Option<i32>,
    pub delegated_plot_id: Option<i32>,
    pub delegated_plot_hf_id: Option<i32>,
    pub entity_id: Option<i32>,
    pub plot_actor: Vec<HFPlotActor>,
    pub parent_plot_hf_id: Option<i32>,
    pub parent_plot_id: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFPlotActor {
    pub hf_id: i32,
    pub local_id: i32,
    pub actor_id: i32,
    pub plot_role: Option<String>,
    pub agreement_id: Option<i32>,
    pub agreement_has_messenger: Option<bool>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFEntityReputation {
    pub hf_id: i32,
    pub entity_id: i32,
    pub first_ageless_year: Option<i32>, // TODO Convert to date
    pub first_ageless_season_count: Option<i32>, // TODO Convert to date
    pub unsolved_murders: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFVagueRelationship {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub war_buddy: Option<bool>,
    pub artistic_buddy: Option<bool>,
    pub atheletic_rival: Option<bool>,
    pub athlete_buddy: Option<bool>,
    pub business_rival: Option<bool>,
    pub childhood_friend: Option<bool>,
    pub grudge: Option<bool>,
    pub jealous_obsession: Option<bool>,
    pub jealous_relationship_grudge: Option<bool>,
    pub persecution_grudge: Option<bool>,
    pub religious_persecution_grudge: Option<bool>,
    pub scholar_buddy: Option<bool>,
    pub supernatural_grudge: Option<bool>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFEntitySquadLink {
    pub hf_id: i32,
    pub entity_id: i32,
    pub squad_id: i32,
    pub squad_position: Option<i32>,
    pub start_year: Option<i32>, // TODO Convert to date
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFLink {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub link_type: Option<String>,
    pub link_strength: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFHonorEntity {
    pub hf_id: i32,
    pub entity_id: i32,
    pub battles: Option<i32>,
    pub honor_id: Vec<i32>,
    pub kills: Option<i32>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Fillable, Filler, Default, JsonSchema, GraphQLObject,
)]
pub struct HFSiteProperty {
    pub hf_id: i32,
    pub site_id: i32,
    pub property_id: i32,
}

impl HistoricalFigure {
    pub fn new() -> Self {
        Self::default()
    }
}

impl CreateNew for HistoricalFigure {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for HistoricalFigure {
    fn example() -> Self {
        Self::default()
    }
}

impl HFEntityLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFEntityLink {
    fn example() -> Self {
        Self::default()
    }
}

impl HFEntityPositionLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFEntityPositionLink {
    fn example() -> Self {
        Self::default()
    }
}

impl HFSiteLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFSiteLink {
    fn example() -> Self {
        Self::default()
    }
}

impl HFSkill {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFSkill {
    fn example() -> Self {
        Self::default()
    }
}

impl HFRelationshipProfileHFHistorical {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFRelationshipProfileHFHistorical {
    fn example() -> Self {
        Self::default()
    }
}

impl HFRelationshipProfileHFVisual {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFRelationshipProfileHFVisual {
    fn example() -> Self {
        Self::default()
    }
}

impl HFIntrigueActor {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFIntrigueActor {
    fn example() -> Self {
        Self::default()
    }
}

impl HFIntriguePlot {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFIntriguePlot {
    fn example() -> Self {
        Self::default()
    }
}

impl HFPlotActor {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFPlotActor {
    fn example() -> Self {
        Self::default()
    }
}

impl HFEntityReputation {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFEntityReputation {
    fn example() -> Self {
        Self::default()
    }
}

impl HFVagueRelationship {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFVagueRelationship {
    fn example() -> Self {
        Self::default()
    }
}

impl HFEntitySquadLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFEntitySquadLink {
    fn example() -> Self {
        Self::default()
    }
}

impl HFLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFLink {
    fn example() -> Self {
        Self::default()
    }
}

impl HFHonorEntity {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFHonorEntity {
    fn example() -> Self {
        Self::default()
    }
}

impl HFSiteProperty {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for HFSiteProperty {
    fn example() -> Self {
        Self::default()
    }
}

impl PartialEq for HFEntityLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl std::hash::Hash for HFEntityLink {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
    }
}

impl PartialEq for HFEntityPositionLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl std::hash::Hash for HFEntityPositionLink {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
    }
}

impl PartialEq for HFSiteLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.site_id == other.site_id
    }
}

impl std::hash::Hash for HFSiteLink {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.site_id.hash(state);
    }
}

impl PartialEq for HFSkill {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.skill == other.skill
    }
}

impl std::hash::Hash for HFSkill {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.skill.hash(state);
    }
}

impl PartialEq for HFRelationshipProfileHFHistorical {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl std::hash::Hash for HFRelationshipProfileHFHistorical {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}

impl PartialEq for HFRelationshipProfileHFVisual {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl std::hash::Hash for HFRelationshipProfileHFVisual {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}

impl PartialEq for HFIntrigueActor {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl std::hash::Hash for HFIntrigueActor {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq for HFIntriguePlot {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl std::hash::Hash for HFIntriguePlot {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq for HFPlotActor {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.local_id == other.local_id
            && self.actor_id == other.actor_id
    }
}

impl std::hash::Hash for HFPlotActor {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.local_id.hash(state);
        self.actor_id.hash(state);
    }
}

impl PartialEq for HFEntityReputation {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl std::hash::Hash for HFEntityReputation {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
    }
}

impl PartialEq for HFVagueRelationship {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl std::hash::Hash for HFVagueRelationship {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}

impl PartialEq for HFEntitySquadLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && self.squad_id == other.squad_id
    }
}

impl std::hash::Hash for HFEntitySquadLink {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
        self.squad_id.hash(state);
    }
}

impl PartialEq for HFLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl std::hash::Hash for HFLink {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}

impl PartialEq for HFHonorEntity {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.entity_id == other.entity_id
    }
}

impl std::hash::Hash for HFHonorEntity {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
    }
}

impl PartialEq for HFSiteProperty {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.site_id == other.site_id
            && self.property_id == other.property_id
    }
}

impl std::hash::Hash for HFSiteProperty {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.site_id.hash(state);
        self.property_id.hash(state);
    }
}
