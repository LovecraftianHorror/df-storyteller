use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_d_d;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_d_d"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventDD {
    pub he_id: i32,
    pub world_id: i32,

    pub d_support_merc_en_id: Option<i32>,
    pub death_penalty: Option<bool>,
    pub defender_civ_id: Option<i32>,
    pub defender_general_hf_id: Option<i32>,
    pub defender_merc_en_id: Option<i32>,
    pub delegated: Option<bool>,
    pub dest_entity_id: Option<i32>,
    pub dest_site_id: Option<i32>,
    pub dest_structure_id: Option<i32>,
    pub destroyed_structure_id: Option<i32>,
    pub destroyer_en_id: Option<i32>,
    pub detected: Option<bool>,
    pub did_not_reveal_all_in_interrogation: Option<bool>,
    pub disturbance: Option<bool>,
    pub dispute: Option<String>,
    pub doer_hf_id: Option<i32>,
    pub death_cause: Option<String>,
    pub destination: Option<i32>,
    pub doer: Option<i32>,
    pub dye_mat: Option<String>,
    pub dye_mat_index: Option<i32>,
    pub dye_mat_type: Option<i32>,
}

impl HistoricalEventDD {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventDD> for HistoricalEventDD {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_d_d: &[HistoricalEventDD]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_d_d::table)
            .values(historical_events_d_d)
            .on_conflict((
                historical_events_d_d::he_id,
                historical_events_d_d::world_id,
            ))
            .do_update()
            .set((
                historical_events_d_d::d_support_merc_en_id
                    .eq(excluded(historical_events_d_d::d_support_merc_en_id)),
                historical_events_d_d::death_penalty
                    .eq(excluded(historical_events_d_d::death_penalty)),
                historical_events_d_d::defender_civ_id
                    .eq(excluded(historical_events_d_d::defender_civ_id)),
                historical_events_d_d::defender_general_hf_id
                    .eq(excluded(historical_events_d_d::defender_general_hf_id)),
                historical_events_d_d::defender_merc_en_id
                    .eq(excluded(historical_events_d_d::defender_merc_en_id)),
                historical_events_d_d::delegated.eq(excluded(historical_events_d_d::delegated)),
                historical_events_d_d::dest_entity_id
                    .eq(excluded(historical_events_d_d::dest_entity_id)),
                historical_events_d_d::dest_site_id
                    .eq(excluded(historical_events_d_d::dest_site_id)),
                historical_events_d_d::dest_structure_id
                    .eq(excluded(historical_events_d_d::dest_structure_id)),
                historical_events_d_d::destroyed_structure_id
                    .eq(excluded(historical_events_d_d::destroyed_structure_id)),
                historical_events_d_d::destroyer_en_id
                    .eq(excluded(historical_events_d_d::destroyer_en_id)),
                historical_events_d_d::detected.eq(excluded(historical_events_d_d::detected)),
                historical_events_d_d::did_not_reveal_all_in_interrogation.eq(excluded(
                    historical_events_d_d::did_not_reveal_all_in_interrogation,
                )),
                historical_events_d_d::disturbance.eq(excluded(historical_events_d_d::disturbance)),
                historical_events_d_d::dispute.eq(excluded(historical_events_d_d::dispute)),
                historical_events_d_d::doer_hf_id.eq(excluded(historical_events_d_d::doer_hf_id)),
                historical_events_d_d::death_cause.eq(excluded(historical_events_d_d::death_cause)),
                historical_events_d_d::destination.eq(excluded(historical_events_d_d::destination)),
                historical_events_d_d::doer.eq(excluded(historical_events_d_d::doer)),
                historical_events_d_d::dye_mat.eq(excluded(historical_events_d_d::dye_mat)),
                historical_events_d_d::dye_mat_index
                    .eq(excluded(historical_events_d_d::dye_mat_index)),
                historical_events_d_d::dye_mat_type
                    .eq(excluded(historical_events_d_d::dye_mat_type)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_d_d");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_d_d: &[HistoricalEventDD]) {
        diesel::insert_into(historical_events_d_d::table)
            .values(historical_events_d_d)
            .execute(conn)
            .expect("Error saving historical_events_d_d");
    }

    /// Get a list of HistoricalEventDD from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventDD>, Error> {
        /*
        use crate::schema::historical_events_d_d::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_d_d.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "d_support_merc_en_id" => d_support_merc_en_id,
                "death_penalty" => death_penalty,
                "defender_civ_id" => defender_civ_id,
                "defender_general_hf_id" => defender_general_hf_id,
                "defender_merc_en_id" => defender_merc_en_id,
                "delegated" => delegated,
                "dest_entity_id" => dest_entity_id,
                "dest_site_id" => dest_site_id,
                "dest_structure_id" => dest_structure_id,
                "destroyed_structure_id" => destroyed_structure_id,
                "destroyer_en_id" => destroyer_en_id,
                "detected" => detected,
                "did_not_reveal_all_in_interrogation" => did_not_reveal_all_in_interrogation,
                "disturbance" => disturbance,
                "dispute" => dispute,
                "doer_hf_id" => doer_hf_id,
                "death_cause" => death_cause,
                "destination" => destination,
                "doer" => doer,
                "dye_mat" => dye_mat,
                "dye_mat_index" => dye_mat_index,
                "dye_mat_type" => dye_mat_type,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventDD>, Error> {
        use crate::schema::historical_events_d_d::dsl::*;
        let query = historical_events_d_d;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventDD>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "d_support_merc_en_id" => "d_support_merc_en_id",
            "death_penalty" => "death_penalty",
            "defender_civ_id" => "defender_civ_id",
            "defender_general_hf_id" => "defender_general_hf_id",
            "defender_merc_en_id" => "defender_merc_en_id",
            "delegated" => "delegated",
            "dest_entity_id" => "dest_entity_id",
            "dest_site_id" => "dest_site_id",
            "dest_structure_id" => "dest_structure_id",
            "destroyed_structure_id" => "destroyed_structure_id",
            "destroyer_en_id" => "destroyer_en_id",
            "detected" => "detected",
            "did_not_reveal_all_in_interrogation" => "did_not_reveal_all_in_interrogation",
            "disturbance" => "disturbance",
            "dispute" => "dispute",
            "doer_hf_id" => "doer_hf_id",
            "death_cause" => "death_cause",
            "destination" => "destination",
            "doer" => "doer",
            "dye_mat" => "dye_mat",
            "dye_mat_index" => "dye_mat_index",
            "dye_mat_type" => "dye_mat_type",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventDD],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_d_d::dsl::*;
        let query = historical_events_d_d.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "d_support_merc_en_id" => {d_support_merc_en_id: Option<i32>},
                "death_penalty" => {death_penalty: Option<bool>},
                "defender_civ_id" => {defender_civ_id: Option<i32>},
                "defender_general_hf_id" => {defender_general_hf_id: Option<i32>},
                "defender_merc_en_id" => {defender_merc_en_id: Option<i32>},
                "delegated" => {delegated: Option<bool>},
                "dest_entity_id" => {dest_entity_id: Option<i32>},
                "dest_site_id" => {dest_site_id: Option<i32>},
                "dest_structure_id" => {dest_structure_id: Option<i32>},
                "destroyed_structure_id" => {destroyed_structure_id: Option<i32>},
                "destroyer_en_id" => {destroyer_en_id: Option<i32>},
                "detected" => {detected: Option<bool>},
                "did_not_reveal_all_in_interrogation" => {did_not_reveal_all_in_interrogation: Option<bool>},
                "disturbance" => {disturbance: Option<bool>},
                "dispute" => {dispute: Option<String>},
                "doer_hf_id" => {doer_hf_id: Option<i32>},
                "death_cause" => {death_cause: Option<String>},
                "destination" => {destination: Option<i32>},
                "doer" => {doer: Option<i32>},
                "dye_mat" => {dye_mat: Option<String>},
                "dye_mat_index" => {dye_mat_index: Option<i32>},
                "dye_mat_type" => {dye_mat_type: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventDD, df_st_core::HistoricalEvent> for HistoricalEventDD {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.d_support_merc_en_id
            .add_missing_data(&source.d_support_merc_en_id);
        self.death_penalty.add_missing_data(&source.death_penalty);
        self.defender_civ_id
            .add_missing_data(&source.defender_civ_id);
        self.defender_general_hf_id
            .add_missing_data(&source.defender_general_hf_id);
        self.defender_merc_en_id
            .add_missing_data(&source.defender_merc_en_id);
        self.delegated.add_missing_data(&source.delegated);
        self.dest_entity_id.add_missing_data(&source.dest_entity_id);
        self.dest_site_id.add_missing_data(&source.dest_site_id);
        self.dest_structure_id
            .add_missing_data(&source.dest_structure_id);
        self.destroyed_structure_id
            .add_missing_data(&source.destroyed_structure_id);
        self.destroyer_en_id
            .add_missing_data(&source.destroyer_en_id);
        self.detected.add_missing_data(&source.detected);
        self.did_not_reveal_all_in_interrogation
            .add_missing_data(&source.did_not_reveal_all_in_interrogation);
        self.disturbance.add_missing_data(&source.disturbance);
        self.dispute.add_missing_data(&source.dispute);
        self.doer_hf_id.add_missing_data(&source.doer_hf_id);
        self.death_cause.add_missing_data(&source.death_cause);
        self.destination.add_missing_data(&source.destination);
        self.doer.add_missing_data(&source.doer);
        self.dye_mat.add_missing_data(&source.dye_mat);
        self.dye_mat_index.add_missing_data(&source.dye_mat_index);
        self.dye_mat_type.add_missing_data(&source.dye_mat_type);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventDD> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventDD) {
        self.id.add_missing_data(&source.he_id);
        self.d_support_merc_en_id
            .add_missing_data(&source.d_support_merc_en_id);
        self.death_penalty.add_missing_data(&source.death_penalty);
        self.defender_civ_id
            .add_missing_data(&source.defender_civ_id);
        self.defender_general_hf_id
            .add_missing_data(&source.defender_general_hf_id);
        self.defender_merc_en_id
            .add_missing_data(&source.defender_merc_en_id);
        self.delegated.add_missing_data(&source.delegated);
        self.dest_entity_id.add_missing_data(&source.dest_entity_id);
        self.dest_site_id.add_missing_data(&source.dest_site_id);
        self.dest_structure_id
            .add_missing_data(&source.dest_structure_id);
        self.destroyed_structure_id
            .add_missing_data(&source.destroyed_structure_id);
        self.destroyer_en_id
            .add_missing_data(&source.destroyer_en_id);
        self.detected.add_missing_data(&source.detected);
        self.did_not_reveal_all_in_interrogation
            .add_missing_data(&source.did_not_reveal_all_in_interrogation);
        self.disturbance.add_missing_data(&source.disturbance);
        self.dispute.add_missing_data(&source.dispute);
        self.doer_hf_id.add_missing_data(&source.doer_hf_id);
        self.death_cause.add_missing_data(&source.death_cause);
        self.destination.add_missing_data(&source.destination);
        self.doer.add_missing_data(&source.doer);
        self.dye_mat.add_missing_data(&source.dye_mat);
        self.dye_mat_index.add_missing_data(&source.dye_mat_index);
        self.dye_mat_type.add_missing_data(&source.dye_mat_type);
    }
}

impl PartialEq for HistoricalEventDD {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventDD {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventDD> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventDD) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventDD {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
