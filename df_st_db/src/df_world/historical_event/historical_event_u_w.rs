use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_u_w;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_u_w"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventUW {
    pub he_id: i32,
    pub world_id: i32,

    pub unit_id: Option<i32>,
    pub unit_type: Option<String>,
    pub victim: Option<i32>,
    pub victim_entity: Option<i32>,
    pub victim_hf_id: Option<i32>,

    pub wanted_and_recognized: Option<bool>,
    pub was_torture: Option<bool>,
    pub wc_id: Option<i32>,
    pub winner_hf_id: Option<i32>,
    pub woundee_hf_id: Option<i32>,
    pub wounder_hf_id: Option<i32>,
    pub wrongful_conviction: Option<bool>,
    pub woundee_caste: Option<i32>,
    pub woundee_race: Option<i32>,
}

impl HistoricalEventUW {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventUW> for HistoricalEventUW {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_u_w: &[HistoricalEventUW]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_u_w::table)
            .values(historical_events_u_w)
            .on_conflict((
                historical_events_u_w::he_id,
                historical_events_u_w::world_id,
            ))
            .do_update()
            .set((
                historical_events_u_w::unit_id.eq(excluded(historical_events_u_w::unit_id)),
                historical_events_u_w::unit_type.eq(excluded(historical_events_u_w::unit_type)),
                historical_events_u_w::victim.eq(excluded(historical_events_u_w::victim)),
                historical_events_u_w::victim_entity
                    .eq(excluded(historical_events_u_w::victim_entity)),
                historical_events_u_w::victim_hf_id
                    .eq(excluded(historical_events_u_w::victim_hf_id)),
                historical_events_u_w::wanted_and_recognized
                    .eq(excluded(historical_events_u_w::wanted_and_recognized)),
                historical_events_u_w::was_torture.eq(excluded(historical_events_u_w::was_torture)),
                historical_events_u_w::wc_id.eq(excluded(historical_events_u_w::wc_id)),
                historical_events_u_w::winner_hf_id
                    .eq(excluded(historical_events_u_w::winner_hf_id)),
                historical_events_u_w::woundee_hf_id
                    .eq(excluded(historical_events_u_w::woundee_hf_id)),
                historical_events_u_w::wounder_hf_id
                    .eq(excluded(historical_events_u_w::wounder_hf_id)),
                historical_events_u_w::wrongful_conviction
                    .eq(excluded(historical_events_u_w::wrongful_conviction)),
                historical_events_u_w::woundee_caste
                    .eq(excluded(historical_events_u_w::woundee_caste)),
                historical_events_u_w::woundee_race
                    .eq(excluded(historical_events_u_w::woundee_race)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_u_w");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_u_w: &[HistoricalEventUW]) {
        diesel::insert_into(historical_events_u_w::table)
            .values(historical_events_u_w)
            .execute(conn)
            .expect("Error saving historical_events_u_w");
    }

    /// Get a list of HistoricalEventUW from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventUW>, Error> {
        /*
        use crate::schema::historical_events_u_w::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_u_w.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "unit_id" => unit_id,
                "unit_type" => unit_type,
                "victim" => victim,
                "victim_entity" => victim_entity,
                "victim_hf_id" => victim_hf_id,
                "wanted_and_recognized" => wanted_and_recognized,
                "was_torture" => was_torture,
                "wc_id" => wc_id,
                "winner_hf_id" => winner_hf_id,
                "woundee_hf_id" => woundee_hf_id,
                "wounder_hf_id" => wounder_hf_id,
                "wrongful_conviction" => wrongful_conviction,
                "woundee_caste" => woundee_caste,
                "woundee_race" => woundee_race,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventUW>, Error> {
        use crate::schema::historical_events_u_w::dsl::*;
        let query = historical_events_u_w;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventUW>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "unit_id" => "unit_id",
            "unit_type" => "unit_type",
            "victim" => "victim",
            "victim_entity" => "victim_entity",
            "victim_hf_id" => "victim_hf_id",
            "wanted_and_recognized" => "wanted_and_recognized",
            "was_torture" => "was_torture",
            "wc_id" => "wc_id",
            "winner_hf_id" => "winner_hf_id",
            "woundee_hf_id" => "woundee_hf_id",
            "wounder_hf_id" => "wounder_hf_id",
            "wrongful_conviction" => "wrongful_conviction",
            "woundee_caste" => "woundee_caste",
            "woundee_race" => "woundee_race",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventUW],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_u_w::dsl::*;
        let query = historical_events_u_w.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "unit_id" => {unit_id: Option<i32>},
                "unit_type" => {unit_type: Option<String>},
                "victim" => {victim: Option<i32>},
                "victim_entity" => {victim_entity: Option<i32>},
                "victim_hf_id" => {victim_hf_id: Option<i32>},
                "wanted_and_recognized" => {wanted_and_recognized: Option<bool>},
                "was_torture" => {was_torture: Option<bool>},
                "wc_id" => {wc_id: Option<i32>},
                "winner_hf_id" => {winner_hf_id: Option<i32>},
                "woundee_hf_id" => {woundee_hf_id: Option<i32>},
                "wounder_hf_id" => {wounder_hf_id: Option<i32>},
                "wrongful_conviction" => {wrongful_conviction: Option<bool>},
                "woundee_caste" => {woundee_caste: Option<i32>},
                "woundee_race" => {woundee_race: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventUW, df_st_core::HistoricalEvent> for HistoricalEventUW {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.unit_id.add_missing_data(&source.unit_id);
        self.unit_type.add_missing_data(&source.unit_type);
        self.victim.add_missing_data(&source.victim);
        self.victim_entity.add_missing_data(&source.victim_entity);
        self.victim_hf_id.add_missing_data(&source.victim_hf_id);
        self.wanted_and_recognized
            .add_missing_data(&source.wanted_and_recognized);
        self.was_torture.add_missing_data(&source.was_torture);
        self.wc_id.add_missing_data(&source.wc_id);
        self.winner_hf_id.add_missing_data(&source.winner_hf_id);
        self.woundee_hf_id.add_missing_data(&source.woundee_hf_id);
        self.wounder_hf_id.add_missing_data(&source.wounder_hf_id);
        self.wrongful_conviction
            .add_missing_data(&source.wrongful_conviction);
        self.woundee_caste.add_missing_data(&source.woundee_caste);
        self.woundee_race.add_missing_data(&source.woundee_race);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventUW> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventUW) {
        self.id.add_missing_data(&source.he_id);
        self.unit_id.add_missing_data(&source.unit_id);
        self.unit_type.add_missing_data(&source.unit_type);
        self.victim.add_missing_data(&source.victim);
        self.victim_entity.add_missing_data(&source.victim_entity);
        self.victim_hf_id.add_missing_data(&source.victim_hf_id);
        self.wanted_and_recognized
            .add_missing_data(&source.wanted_and_recognized);
        self.was_torture.add_missing_data(&source.was_torture);
        self.wc_id.add_missing_data(&source.wc_id);
        self.winner_hf_id.add_missing_data(&source.winner_hf_id);
        self.woundee_hf_id.add_missing_data(&source.woundee_hf_id);
        self.wounder_hf_id.add_missing_data(&source.wounder_hf_id);
        self.wrongful_conviction
            .add_missing_data(&source.wrongful_conviction);
        self.woundee_caste.add_missing_data(&source.woundee_caste);
        self.woundee_race.add_missing_data(&source.woundee_race);
    }
}

impl PartialEq for HistoricalEventUW {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventUW {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventUW> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventUW) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventUW {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
