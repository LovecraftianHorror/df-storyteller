use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_intrigue_plots;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_intrigue_plots"]
#[primary_key(hf_id, local_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFIntriguePlot {
    pub hf_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub on_hold: Option<bool>,
    pub actor_id: Option<i32>,
    pub artifact_id: Option<i32>,
    pub delegated_plot_id: Option<i32>,
    pub delegated_plot_hf_id: Option<i32>,
    pub entity_id: Option<i32>,
    pub parent_plot_hf_id: Option<i32>,
    pub parent_plot_id: Option<i32>,
}

impl HFIntriguePlot {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFIntriguePlot, HFIntriguePlot> for HFIntriguePlot {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_intrigue_plots: &[HFIntriguePlot]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_intrigue_plots::table)
            .values(hf_intrigue_plots)
            .on_conflict((
                hf_intrigue_plots::hf_id,
                hf_intrigue_plots::local_id,
                hf_intrigue_plots::world_id,
            ))
            .do_update()
            .set((
                hf_intrigue_plots::type_.eq(excluded(hf_intrigue_plots::type_)),
                hf_intrigue_plots::on_hold.eq(excluded(hf_intrigue_plots::on_hold)),
                hf_intrigue_plots::actor_id.eq(excluded(hf_intrigue_plots::actor_id)),
                hf_intrigue_plots::artifact_id.eq(excluded(hf_intrigue_plots::artifact_id)),
                hf_intrigue_plots::delegated_plot_id
                    .eq(excluded(hf_intrigue_plots::delegated_plot_id)),
                hf_intrigue_plots::delegated_plot_hf_id
                    .eq(excluded(hf_intrigue_plots::delegated_plot_hf_id)),
                hf_intrigue_plots::entity_id.eq(excluded(hf_intrigue_plots::entity_id)),
                hf_intrigue_plots::parent_plot_hf_id
                    .eq(excluded(hf_intrigue_plots::parent_plot_hf_id)),
                hf_intrigue_plots::parent_plot_id.eq(excluded(hf_intrigue_plots::parent_plot_id)),
            ))
            .execute(conn)
            .expect("Error saving hf_intrigue_plots");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_intrigue_plots: &[HFIntriguePlot]) {
        diesel::insert_into(hf_intrigue_plots::table)
            .values(hf_intrigue_plots)
            .execute(conn)
            .expect("Error saving hf_intrigue_plots");
    }

    /// Get a list of HFIntriguePlot from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFIntriguePlot>, Error> {
        use crate::schema::hf_intrigue_plots::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_intrigue_plots.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "local_id" => local_id,
                "type" => type_,
                "on_hold" => on_hold,
                "actor_id" => actor_id,
                "artifact_id" => artifact_id,
                "delegated_plot_id" => delegated_plot_id,
                "delegated_plot_hf_id" => delegated_plot_hf_id,
                "entity_id" => entity_id,
                "parent_plot_hf_id" => parent_plot_hf_id,
                "parent_plot_id" => parent_plot_id,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFIntriguePlot>, Error> {
        use crate::schema::hf_intrigue_plots::dsl::*;
        let query = hf_intrigue_plots;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
            ],
            {return Ok(query.first::<HFIntriguePlot>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "local_id" => "local_id",
            "type" => "type",
            "on_hold" => "on_hold",
            "actor_id" => "actor_id",
            "artifact_id" => "artifact_id",
            "delegated_plot_id" => "delegated_plot_id",
            "delegated_plot_hf_id" => "delegated_plot_hf_id",
            "entity_id" => "entity_id",
            "parent_plot_hf_id" => "parent_plot_hf_id",
            "parent_plot_id" => "parent_plot_id",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFIntriguePlot],
        core_list: Vec<df_st_core::HFIntriguePlot>,
    ) -> Result<Vec<df_st_core::HFIntriguePlot>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_intrigue_plots::dsl::*;
        let query = hf_intrigue_plots.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "local_id" => {local_id: i32},
                "type" => {type_: Option<String>},
                "on_hold" => {on_hold: Option<bool>},
                "actor_id" => {actor_id: Option<i32>},
                "artifact_id" => {artifact_id: Option<i32>},
                "delegated_plot_id" => {delegated_plot_id: Option<i32>},
                "delegated_plot_hf_id" => {delegated_plot_hf_id: Option<i32>},
                "entity_id" => {entity_id: Option<i32>},
                "parent_plot_hf_id" => {parent_plot_hf_id: Option<i32>},
                "parent_plot_id" => {parent_plot_id: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFIntriguePlot, df_st_core::HFIntriguePlot> for HFIntriguePlot {
    fn add_missing_data(&mut self, source: &df_st_core::HFIntriguePlot) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.on_hold.add_missing_data(&source.on_hold);
        self.actor_id.add_missing_data(&source.actor_id);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.delegated_plot_id
            .add_missing_data(&source.delegated_plot_id);
        self.delegated_plot_hf_id
            .add_missing_data(&source.delegated_plot_hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.parent_plot_hf_id
            .add_missing_data(&source.parent_plot_hf_id);
        self.parent_plot_id.add_missing_data(&source.parent_plot_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFIntriguePlot, HFIntriguePlot> for df_st_core::HFIntriguePlot {
    fn add_missing_data(&mut self, source: &HFIntriguePlot) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.on_hold.add_missing_data(&source.on_hold);
        self.actor_id.add_missing_data(&source.actor_id);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.delegated_plot_id
            .add_missing_data(&source.delegated_plot_id);
        self.delegated_plot_hf_id
            .add_missing_data(&source.delegated_plot_hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.parent_plot_hf_id
            .add_missing_data(&source.parent_plot_hf_id);
        self.parent_plot_id.add_missing_data(&source.parent_plot_id);
    }
}

impl PartialEq<df_st_core::HFIntriguePlot> for HFIntriguePlot {
    fn eq(&self, other: &df_st_core::HFIntriguePlot) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl PartialEq<HFIntriguePlot> for df_st_core::HFIntriguePlot {
    fn eq(&self, other: &HFIntriguePlot) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl PartialEq for HFIntriguePlot {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl Hash for HFIntriguePlot {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.local_id.hash(state);
    }
}
