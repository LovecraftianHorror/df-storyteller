use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A ... in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct HERelationshipSupplement {
    /// Identifier for the historical event relationship supplement.
    /// `id` must be unique for the whole world.
    /// id is given a value after parsing not from files.
    pub id: i32,
    pub event: Option<i32>,
    pub occasion_type: Option<i32>,
    pub site_id: Option<i32>,
    pub unk_1: Option<i32>,
}

impl HERelationshipSupplement {
    pub fn new() -> Self {
        Self::default()
    }
}

impl CreateNew for HERelationshipSupplement {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for HERelationshipSupplement {
    fn example() -> Self {
        Self::default()
    }
}
