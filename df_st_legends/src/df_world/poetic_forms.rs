use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct PoeticForm {
    pub id: i32,
    pub description: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct PoeticForms {
    pub poetic_form: Option<Vec<PoeticForm>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::PoeticForm, PoeticForm> for df_st_core::PoeticForm {
    fn add_missing_data(&mut self, source: &PoeticForm) {
        self.id.add_missing_data(&source.id);
        self.description.add_missing_data(&source.description);
    }
}

impl PartialEq<df_st_core::PoeticForm> for PoeticForm {
    fn eq(&self, other: &df_st_core::PoeticForm) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::PoeticForm>, PoeticForms> for Vec<df_st_core::PoeticForm> {
    fn add_missing_data(&mut self, source: &PoeticForms) {
        self.add_missing_data(&source.poetic_form);
    }
}

impl Filler<IndexMap<u64, df_st_core::PoeticForm>, PoeticForms>
    for IndexMap<u64, df_st_core::PoeticForm>
{
    fn add_missing_data(&mut self, source: &PoeticForms) {
        self.add_missing_data(&source.poetic_form);
    }
}
