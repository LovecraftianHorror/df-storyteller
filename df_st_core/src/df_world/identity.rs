use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// A (assumed) Identity in the world in the world.
/// This identity is (usually) fake. (TODO: Check)
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Identity {
    /// Identifier for the dance form.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub name: Option<String>,
    pub hf_id: Option<i32>,
    pub race: Option<String>,
    pub caste: Option<String>,
    pub birth_year: Option<i32>, // TODO Date
    pub birth_second: Option<i32>,
    pub profession: Option<String>,
    pub entity_id: Option<i32>,
    pub nemesis_id: Option<i32>,
}

impl Identity {
    pub fn new() -> Self {
        Self::default()
    }
}

impl CreateNew for Identity {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Identity {
    fn example() -> Self {
        Self::default()
    }
}
