use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::artifacts;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use failure::Error;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "artifacts"]
pub struct Artifact {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub site_id: Option<i32>,
    pub structure_local_id: Option<i32>,
    pub holder_hfid: Option<i32>,

    pub abs_tile_x: Option<i32>,
    pub abs_tile_y: Option<i32>,
    pub abs_tile_z: Option<i32>,
    pub subregion_id: Option<i32>,

    pub item_name: Option<String>,
    pub item_type: Option<String>,
    pub item_subtype: Option<String>,
    pub item_writing: Option<i32>,
    pub item_page_number: Option<i32>,
    pub item_page_written_content_id: Option<i32>,
    pub item_writing_written_content_id: Option<i32>,
    pub item_description: Option<String>,
    pub item_mat: Option<String>,
}

impl Artifact {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Artifact, Artifact> for Artifact {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {}

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, artifacts: &[Artifact]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(artifacts::table)
            .values(artifacts)
            .on_conflict((artifacts::id, artifacts::world_id))
            .do_update()
            .set((
                artifacts::name.eq(excluded(artifacts::name)),
                artifacts::site_id.eq(excluded(artifacts::site_id)),
                artifacts::structure_local_id.eq(excluded(artifacts::structure_local_id)),
                artifacts::holder_hfid.eq(excluded(artifacts::holder_hfid)),
                artifacts::abs_tile_x.eq(excluded(artifacts::abs_tile_x)),
                artifacts::abs_tile_y.eq(excluded(artifacts::abs_tile_y)),
                artifacts::abs_tile_z.eq(excluded(artifacts::abs_tile_z)),
                artifacts::subregion_id.eq(excluded(artifacts::subregion_id)),
                artifacts::item_name.eq(excluded(artifacts::item_name)),
                artifacts::item_type.eq(excluded(artifacts::item_type)),
                artifacts::item_subtype.eq(excluded(artifacts::item_subtype)),
                artifacts::item_writing.eq(excluded(artifacts::item_writing)),
                artifacts::item_page_number.eq(excluded(artifacts::item_page_number)),
                artifacts::item_page_written_content_id
                    .eq(excluded(artifacts::item_page_written_content_id)),
                artifacts::item_writing_written_content_id
                    .eq(excluded(artifacts::item_writing_written_content_id)),
                artifacts::item_description.eq(excluded(artifacts::item_description)),
                artifacts::item_mat.eq(excluded(artifacts::item_mat)),
            ))
            .execute(conn)
            .expect("Error saving artifacts");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, artifacts: &[Artifact]) {
        diesel::insert_into(artifacts::table)
            .values(artifacts)
            .execute(conn)
            .expect("Error saving artifacts");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<Artifact>, Error> {
        use crate::schema::artifacts::dsl::*;
        let query = artifacts;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        return Ok(query.first::<Artifact>(conn).optional()?);
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<Artifact>, Error> {
        use crate::schema::artifacts::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = artifacts.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
                "site_id" => site_id,
                "structure_local_id" => structure_local_id,
                "holder_hfid" => holder_hfid,
                "subregion_id" => subregion_id,
                "item_page_written_content_id" => item_page_written_content_id,
                "item_writing_written_content_id" => item_writing_written_content_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "site_id" => site_id,
                "structure_local_id" => structure_local_id,
                "holder_hfid" => holder_hfid,
                "abs_tile_x" => abs_tile_x,
                "abs_tile_y" => abs_tile_y,
                "abs_tile_z" => abs_tile_z,
                "subregion_id" => subregion_id,
                "item_name" => item_name,
                "item_type" => item_type,
                "item_subtype" => item_subtype,
                "item_writing" => item_writing,
                "item_page_number" => item_page_number,
                "item_page_written_content_id" => item_page_written_content_id,
                "item_writing_written_content_id" => item_writing_written_content_id,
                "item_description" => item_description,
                "item_mat" => item_mat,
            });},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "name" => "name",
            "site_id" => "site_id",
            "structure_local_id" => "structure_local_id",
            "holder_hfid" => "holder_hfid",
            "abs_tile_x" => "abs_tile_x",
            "abs_tile_y" => "abs_tile_y",
            "abs_tile_z" => "abs_tile_z",
            "subregion_id" => "subregion_id",
            "item_name" => "item_name",
            "item_type" => "item_type",
            "item_subtype" => "item_subtype",
            "item_writing" => "item_writing",
            "item_page_number" => "item_page_number",
            "item_page_written_content_id" => "item_page_written_content_id",
            "item_writing_written_content_id" => "item_writing_written_content_id",
            "item_description" => "item_description",
            "item_mat" => "item_mat",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[Artifact],
        core_list: Vec<df_st_core::Artifact>,
    ) -> Result<Vec<df_st_core::Artifact>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::artifacts::dsl::*;
        let query = artifacts.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
                "site_id" => site_id,
                "structure_local_id" => structure_local_id,
                "holder_hfid" => holder_hfid,
                "subregion_id" => subregion_id,
                "item_page_written_content_id" => item_page_written_content_id,
                "item_writing_written_content_id" => item_writing_written_content_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
                "site_id" => {site_id: Option<i32>},
                "structure_local_id" => {structure_local_id: Option<i32>},
                "holder_hfid" => {holder_hfid: Option<i32>},
                "abs_tile_x" => {abs_tile_x: Option<i32>},
                "abs_tile_y" => {abs_tile_y: Option<i32>},
                "abs_tile_z" => {abs_tile_z: Option<i32>},
                "subregion_id" => {subregion_id: Option<i32>},
                "item_name" => {item_name: Option<String>},
                "item_type" => {item_type: Option<String>},
                "item_subtype" => {item_subtype: Option<String>},
                "item_writing" => {item_writing: Option<i32>},
                "item_page_number" => {item_page_number: Option<i32>},
                "item_page_written_content_id" => {item_page_written_content_id: Option<i32>},
                "item_writing_written_content_id" => {item_writing_written_content_id: Option<i32>},
                "item_description" => {item_description: Option<String>},
                "item_mat" => {item_mat: Option<String>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<Artifact, df_st_core::Artifact> for Artifact {
    fn add_missing_data(&mut self, source: &df_st_core::Artifact) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.site_id.add_missing_data(&source.site_id);
        self.structure_local_id
            .add_missing_data(&source.structure_local_id);
        self.holder_hfid.add_missing_data(&source.holder_hfid);
        self.abs_tile_x.add_missing_data(&source.abs_tile_x);
        self.abs_tile_y.add_missing_data(&source.abs_tile_y);
        self.abs_tile_z.add_missing_data(&source.abs_tile_z);
        self.item_name.add_missing_data(&source.item_name);
        self.item_type.add_missing_data(&source.item_type);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.item_writing.add_missing_data(&source.item_writing);
        self.item_page_number
            .add_missing_data(&source.item_page_number);
        self.item_page_written_content_id
            .add_missing_data(&source.item_page_written_content_id);
        self.item_writing_written_content_id
            .add_missing_data(&source.item_writing_written_content_id);
        self.item_description
            .add_missing_data(&source.item_description);
        self.item_mat.add_missing_data(&source.item_mat);
    }
}

/// From DB to Core
impl Filler<df_st_core::Artifact, Artifact> for df_st_core::Artifact {
    fn add_missing_data(&mut self, source: &Artifact) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.site_id.add_missing_data(&source.site_id);
        self.structure_local_id
            .add_missing_data(&source.structure_local_id);
        self.holder_hfid.add_missing_data(&source.holder_hfid);
        self.abs_tile_x.add_missing_data(&source.abs_tile_x);
        self.abs_tile_y.add_missing_data(&source.abs_tile_y);
        self.abs_tile_z.add_missing_data(&source.abs_tile_z);
        self.item_name.add_missing_data(&source.item_name);
        self.item_type.add_missing_data(&source.item_type);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.item_writing.add_missing_data(&source.item_writing);
        self.item_page_number
            .add_missing_data(&source.item_page_number);
        self.item_page_written_content_id
            .add_missing_data(&source.item_page_written_content_id);
        self.item_writing_written_content_id
            .add_missing_data(&source.item_writing_written_content_id);
        self.item_description
            .add_missing_data(&source.item_description);
        self.item_mat.add_missing_data(&source.item_mat);
    }
}

impl PartialEq<Artifact> for df_st_core::Artifact {
    fn eq(&self, other: &Artifact) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::Artifact> for Artifact {
    fn eq(&self, other: &df_st_core::Artifact) -> bool {
        self.id == other.id
    }
}
