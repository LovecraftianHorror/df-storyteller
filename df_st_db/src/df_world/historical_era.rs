use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::historical_eras;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use failure::Error;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "historical_eras"]
pub struct HistoricalEra {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub start_year: Option<i32>,
}

impl HistoricalEra {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEra, HistoricalEra> for HistoricalEra {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_eras: &[HistoricalEra]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_eras::table)
            .values(historical_eras)
            .on_conflict((historical_eras::id, historical_eras::world_id))
            .do_update()
            .set((
                historical_eras::name.eq(excluded(historical_eras::name)),
                historical_eras::start_year.eq(excluded(historical_eras::start_year)),
            ))
            .execute(conn)
            .expect("Error saving historical_eras");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_eras: &[HistoricalEra]) {
        diesel::insert_into(historical_eras::table)
            .values(historical_eras)
            .execute(conn)
            .expect("Error saving historical_eras");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEra>, Error> {
        use crate::schema::historical_eras::dsl::*;
        let query = historical_eras;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        return Ok(query.first::<HistoricalEra>(conn).optional()?);
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HistoricalEra>, Error> {
        use crate::schema::historical_eras::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = historical_eras.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "start_year" => start_year,
            });},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "name" => "name",
            "start_year" => "start_year",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEra],
        core_list: Vec<df_st_core::HistoricalEra>,
    ) -> Result<Vec<df_st_core::HistoricalEra>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::historical_eras::dsl::*;
        let query = historical_eras.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
                "start_year" => {start_year: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HistoricalEra, df_st_core::HistoricalEra> for HistoricalEra {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEra) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.start_year.add_missing_data(&source.start_year);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEra, HistoricalEra> for df_st_core::HistoricalEra {
    fn add_missing_data(&mut self, source: &HistoricalEra) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.start_year.add_missing_data(&source.start_year);
    }
}

impl PartialEq<HistoricalEra> for df_st_core::HistoricalEra {
    fn eq(&self, other: &HistoricalEra) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::HistoricalEra> for HistoricalEra {
    fn eq(&self, other: &df_st_core::HistoricalEra) -> bool {
        self.id == other.id
    }
}
