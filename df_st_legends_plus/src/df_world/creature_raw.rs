use df_st_core::{Filler, HasUnknown};
use df_st_derive::HasUnknown;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Creature {
    // always present
    pub creature_id: Option<String>,
    pub name_singular: Option<String>,
    pub name_plural: Option<String>,

    // sometimes present
    pub all_castes_alive: Option<String>,
    pub artificial_hiveable: Option<String>,

    pub biome_desert_badland: Option<String>,
    pub biome_desert_rock: Option<String>,
    pub biome_desert_sand: Option<String>,
    pub biome_forest_taiga: Option<String>,
    pub biome_forest_temperate_broadleaf: Option<String>,
    pub biome_forest_temperate_conifer: Option<String>,
    pub biome_forest_tropical_conifer: Option<String>,
    pub biome_forest_tropical_dry_broadleaf: Option<String>,
    pub biome_forest_tropical_moist_broadleaf: Option<String>,
    pub biome_glacier: Option<String>,
    pub biome_grassland_temperate: Option<String>,
    pub biome_grassland_tropical: Option<String>,
    pub biome_lake_temperate_brackishwater: Option<String>,
    pub biome_lake_temperate_freshwater: Option<String>,
    pub biome_lake_temperate_saltwater: Option<String>,
    pub biome_lake_tropical_brackishwater: Option<String>,
    pub biome_lake_tropical_freshwater: Option<String>,
    pub biome_lake_tropical_saltwater: Option<String>,
    pub biome_marsh_temperate_freshwater: Option<String>,
    pub biome_marsh_temperate_saltwater: Option<String>,
    pub biome_marsh_tropical_freshwater: Option<String>,
    pub biome_marsh_tropical_saltwater: Option<String>,
    pub biome_mountain: Option<String>,
    pub biome_ocean_arctic: Option<String>,
    pub biome_ocean_temperate: Option<String>,
    pub biome_ocean_tropical: Option<String>,
    pub biome_pool_temperate_brackishwater: Option<String>,
    pub biome_pool_temperate_freshwater: Option<String>,
    pub biome_pool_temperate_saltwater: Option<String>,
    pub biome_pool_tropical_brackishwater: Option<String>,
    pub biome_pool_tropical_freshwater: Option<String>,
    pub biome_pool_tropical_saltwater: Option<String>,
    pub biome_river_temperate_brackishwater: Option<String>,
    pub biome_river_temperate_freshwater: Option<String>,
    pub biome_river_temperate_saltwater: Option<String>,
    pub biome_river_tropical_brackishwater: Option<String>,
    pub biome_river_tropical_freshwater: Option<String>,
    pub biome_river_tropical_saltwater: Option<String>,
    pub biome_savanna_temperate: Option<String>,
    pub biome_savanna_tropical: Option<String>,
    pub biome_shrubland_temperate: Option<String>,
    pub biome_shrubland_tropical: Option<String>,
    pub biome_subterranean_chasm: Option<String>,
    pub biome_subterranean_lava: Option<String>,
    pub biome_subterranean_water: Option<String>,
    pub biome_swamp_mangrove: Option<String>,
    pub biome_swamp_temperate_freshwater: Option<String>,
    pub biome_swamp_temperate_saltwater: Option<String>,
    pub biome_swamp_tropical_freshwater: Option<String>,
    pub biome_swamp_tropical_saltwater: Option<String>,
    pub biome_tundra: Option<String>,

    pub does_not_exist: Option<String>,
    pub equipment: Option<String>,
    pub equipment_wagon: Option<String>,
    pub evil: Option<String>,

    pub fanciful: Option<String>,
    pub generated: Option<String>,
    pub good: Option<String>,

    pub has_any_benign: Option<String>,
    pub has_any_can_swim: Option<String>,
    pub has_any_cannot_breathe_air: Option<String>,
    pub has_any_cannot_breathe_water: Option<String>,
    pub has_any_carnivore: Option<String>,
    pub has_any_common_domestic: Option<String>,
    pub has_any_curious_beast: Option<String>,
    pub has_any_demon: Option<String>,
    pub has_any_feature_beast: Option<String>,
    pub has_any_flier: Option<String>,
    pub has_any_fly_race_gait: Option<String>,
    pub has_any_grasp: Option<String>,
    pub has_any_grazer: Option<String>,
    pub has_any_has_blood: Option<String>,
    pub has_any_immobile: Option<String>,
    pub has_any_intelligent_learns: Option<String>,
    pub has_any_intelligent_speaks: Option<String>,
    pub has_any_large_predator: Option<String>,
    pub has_any_local_pops_controllable: Option<String>,
    pub has_any_local_pops_produce_heroes: Option<String>,
    pub has_any_megabeast: Option<String>,
    pub has_any_mischievous: Option<String>,
    pub has_any_natural_animal: Option<String>,
    pub has_any_night_creature: Option<String>,
    pub has_any_night_creature_bogeyman: Option<String>,
    pub has_any_night_creature_experimenter: Option<String>,
    pub has_any_night_creature_hunter: Option<String>,
    pub has_any_night_creature_nightmare: Option<String>,
    pub has_any_not_fireimmune: Option<String>,
    pub has_any_not_flier: Option<String>,
    pub has_any_not_living: Option<String>,
    pub has_any_outsider_controllable: Option<String>,
    pub has_any_power: Option<String>,
    pub has_any_race_gait: Option<String>,
    pub has_any_semimegabeast: Option<String>,
    pub has_any_slow_learner: Option<String>,
    pub has_any_supernatural: Option<String>,
    pub has_any_titan: Option<String>,
    pub has_any_unique_demon: Option<String>,
    pub has_any_utterances: Option<String>,
    pub has_any_vermin_hateable: Option<String>,
    pub has_any_vermin_micro: Option<String>,
    pub has_female: Option<String>,
    pub has_male: Option<String>,

    pub large_roaming: Option<String>,
    pub loose_clusters: Option<String>,
    pub mates_to_breed: Option<String>,
    pub mundane: Option<String>,

    pub occurs_as_entity_race: Option<String>,
    pub savage: Option<String>,
    pub small_race: Option<String>,

    pub two_genders: Option<String>,
    pub ubiquitous: Option<String>,

    pub vermin_eater: Option<String>,
    pub vermin_fish: Option<String>,
    pub vermin_grounder: Option<String>,
    pub vermin_rotter: Option<String>,
    pub vermin_soil: Option<String>,
    pub vermin_soil_colony: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct CreatureRaw {
    pub creature: Option<Vec<Creature>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Creature, Creature> for df_st_core::Creature {
    #[rustfmt::skip]
    fn add_missing_data(&mut self, source: &Creature) {
        self.creature_id.add_missing_data(&source.creature_id);
        self.name_singular.add_missing_data(&source.name_singular);
        self.name_plural.add_missing_data(&source.name_plural);

        self.all_castes_alive.add_missing_data(&source.all_castes_alive);
        self.artificial_hiveable.add_missing_data(&source.artificial_hiveable);

        self.biomes.desert_badland.add_missing_data(&source.biome_desert_badland);
        self.biomes.desert_rock.add_missing_data(&source.biome_desert_rock);
        self.biomes.desert_sand.add_missing_data(&source.biome_desert_sand);
        self.biomes.forest_taiga.add_missing_data(&source.biome_forest_taiga);
        self.biomes.forest_temperate_broadleaf.add_missing_data(&source.biome_forest_temperate_broadleaf);
        self.biomes.forest_temperate_conifer.add_missing_data(&source.biome_forest_temperate_conifer);
        self.biomes.forest_tropical_conifer.add_missing_data(&source.biome_forest_tropical_conifer);
        self.biomes.forest_tropical_dry_broadleaf.add_missing_data(&source.biome_forest_tropical_dry_broadleaf);
        self.biomes.forest_tropical_moist_broadleaf.add_missing_data(&source.biome_forest_tropical_moist_broadleaf);
        self.biomes.glacier.add_missing_data(&source.biome_glacier);
        self.biomes.grassland_temperate.add_missing_data(&source.biome_grassland_temperate);
        self.biomes.grassland_tropical.add_missing_data(&source.biome_grassland_tropical);
        self.biomes.lake_temperate_brackishwater.add_missing_data(&source.biome_lake_temperate_brackishwater);
        self.biomes.lake_temperate_freshwater.add_missing_data(&source.biome_lake_temperate_freshwater);
        self.biomes.lake_temperate_saltwater.add_missing_data(&source.biome_lake_temperate_saltwater);
        self.biomes.lake_tropical_brackishwater.add_missing_data(&source.biome_lake_tropical_brackishwater);
        self.biomes.lake_tropical_freshwater.add_missing_data(&source.biome_lake_tropical_freshwater);
        self.biomes.lake_tropical_saltwater.add_missing_data(&source.biome_lake_tropical_saltwater);
        self.biomes.marsh_temperate_freshwater.add_missing_data(&source.biome_marsh_temperate_freshwater);
        self.biomes.marsh_temperate_saltwater.add_missing_data(&source.biome_marsh_temperate_saltwater);
        self.biomes.marsh_tropical_freshwater.add_missing_data(&source.biome_marsh_tropical_freshwater);
        self.biomes.marsh_tropical_saltwater.add_missing_data(&source.biome_marsh_tropical_saltwater);
        self.biomes.mountain.add_missing_data(&source.biome_mountain);
        self.biomes.ocean_arctic.add_missing_data(&source.biome_ocean_arctic);
        self.biomes.ocean_temperate.add_missing_data(&source.biome_ocean_temperate);
        self.biomes.ocean_tropical.add_missing_data(&source.biome_ocean_tropical);
        self.biomes.pool_temperate_brackishwater.add_missing_data(&source.biome_pool_temperate_brackishwater);
        self.biomes.pool_temperate_freshwater.add_missing_data(&source.biome_pool_temperate_freshwater);
        self.biomes.pool_temperate_saltwater.add_missing_data(&source.biome_pool_temperate_saltwater);
        self.biomes.pool_tropical_brackishwater.add_missing_data(&source.biome_pool_tropical_brackishwater);
        self.biomes.pool_tropical_freshwater.add_missing_data(&source.biome_pool_tropical_freshwater);
        self.biomes.pool_tropical_saltwater.add_missing_data(&source.biome_pool_tropical_saltwater);
        self.biomes.river_temperate_brackishwater.add_missing_data(&source.biome_river_temperate_brackishwater);
        self.biomes.river_temperate_freshwater.add_missing_data(&source.biome_river_temperate_freshwater);
        self.biomes.river_temperate_saltwater.add_missing_data(&source.biome_river_temperate_saltwater);
        self.biomes.river_tropical_brackishwater.add_missing_data(&source.biome_river_tropical_brackishwater);
        self.biomes.river_tropical_freshwater.add_missing_data(&source.biome_river_tropical_freshwater);
        self.biomes.river_tropical_saltwater.add_missing_data(&source.biome_river_tropical_saltwater);
        self.biomes.savanna_temperate.add_missing_data(&source.biome_savanna_temperate);
        self.biomes.savanna_tropical.add_missing_data(&source.biome_savanna_tropical);
        self.biomes.shrubland_temperate.add_missing_data(&source.biome_shrubland_temperate);
        self.biomes.shrubland_tropical.add_missing_data(&source.biome_shrubland_tropical);
        self.biomes.subterranean_chasm.add_missing_data(&source.biome_subterranean_chasm);
        self.biomes.subterranean_lava.add_missing_data(&source.biome_subterranean_lava);
        self.biomes.subterranean_water.add_missing_data(&source.biome_subterranean_water);
        self.biomes.swamp_mangrove.add_missing_data(&source.biome_swamp_mangrove);
        self.biomes.swamp_temperate_freshwater.add_missing_data(&source.biome_swamp_temperate_freshwater);
        self.biomes.swamp_temperate_saltwater.add_missing_data(&source.biome_swamp_temperate_saltwater);
        self.biomes.swamp_tropical_freshwater.add_missing_data(&source.biome_swamp_tropical_freshwater);
        self.biomes.swamp_tropical_saltwater.add_missing_data(&source.biome_swamp_tropical_saltwater);
        self.biomes.tundra.add_missing_data(&source.biome_tundra);

        self.does_not_exist.add_missing_data(&source.does_not_exist);
        self.equipment.add_missing_data(&source.equipment);
        self.equipment_wagon.add_missing_data(&source.equipment_wagon);
        self.evil.add_missing_data(&source.evil);

        self.fanciful.add_missing_data(&source.fanciful);
        self.generated.add_missing_data(&source.generated);
        self.good.add_missing_data(&source.good);

        self.has_any_benign.add_missing_data(&source.has_any_benign);
        self.has_any_can_swim.add_missing_data(&source.has_any_can_swim);
        self.has_any_cannot_breathe_air.add_missing_data(&source.has_any_cannot_breathe_air);
        self.has_any_cannot_breathe_water.add_missing_data(&source.has_any_cannot_breathe_water);
        self.has_any_carnivore.add_missing_data(&source.has_any_carnivore);
        self.has_any_common_domestic.add_missing_data(&source.has_any_common_domestic);
        self.has_any_curious_beast.add_missing_data(&source.has_any_curious_beast);
        self.has_any_demon.add_missing_data(&source.has_any_demon);
        self.has_any_feature_beast.add_missing_data(&source.has_any_feature_beast);
        self.has_any_flier.add_missing_data(&source.has_any_flier);
        self.has_any_fly_race_gait.add_missing_data(&source.has_any_fly_race_gait);
        self.has_any_grasp.add_missing_data(&source.has_any_grasp);
        self.has_any_grazer.add_missing_data(&source.has_any_grazer);
        self.has_any_has_blood.add_missing_data(&source.has_any_has_blood);
        self.has_any_immobile.add_missing_data(&source.has_any_immobile);
        self.has_any_intelligent_learns.add_missing_data(&source.has_any_intelligent_learns);
        self.has_any_intelligent_speaks.add_missing_data(&source.has_any_intelligent_speaks);
        self.has_any_large_predator.add_missing_data(&source.has_any_large_predator);
        self.has_any_local_pops_controllable.add_missing_data(&source.has_any_local_pops_controllable);
        self.has_any_local_pops_produce_heroes.add_missing_data(&source.has_any_local_pops_produce_heroes);
        self.has_any_megabeast.add_missing_data(&source.has_any_megabeast);
        self.has_any_mischievous.add_missing_data(&source.has_any_mischievous);
        self.has_any_natural_animal.add_missing_data(&source.has_any_natural_animal);
        self.has_any_night_creature.add_missing_data(&source.has_any_night_creature);
        self.has_any_night_creature_bogeyman.add_missing_data(&source.has_any_night_creature_bogeyman);
        self.has_any_night_creature_experimenter.add_missing_data(&source.has_any_night_creature_experimenter);
        self.has_any_night_creature_hunter.add_missing_data(&source.has_any_night_creature_hunter);
        self.has_any_night_creature_nightmare.add_missing_data(&source.has_any_night_creature_nightmare);
        self.has_any_not_fireimmune.add_missing_data(&source.has_any_not_fireimmune);
        self.has_any_not_flier.add_missing_data(&source.has_any_not_flier);
        self.has_any_not_living.add_missing_data(&source.has_any_not_living);
        self.has_any_outsider_controllable.add_missing_data(&source.has_any_outsider_controllable);
        self.has_any_power.add_missing_data(&source.has_any_power);
        self.has_any_race_gait.add_missing_data(&source.has_any_race_gait);
        self.has_any_semimegabeast.add_missing_data(&source.has_any_semimegabeast);
        self.has_any_slow_learner.add_missing_data(&source.has_any_slow_learner);
        self.has_any_supernatural.add_missing_data(&source.has_any_supernatural);
        self.has_any_titan.add_missing_data(&source.has_any_titan);
        self.has_any_unique_demon.add_missing_data(&source.has_any_unique_demon);
        self.has_any_utterances.add_missing_data(&source.has_any_utterances);
        self.has_any_vermin_hateable.add_missing_data(&source.has_any_vermin_hateable);
        self.has_any_vermin_micro.add_missing_data(&source.has_any_vermin_micro);
        self.has_female.add_missing_data(&source.has_female);
        self.has_male.add_missing_data(&source.has_male);

        self.large_roaming.add_missing_data(&source.large_roaming);
        self.loose_clusters.add_missing_data(&source.loose_clusters);
        self.mates_to_breed.add_missing_data(&source.mates_to_breed);
        self.mundane.add_missing_data(&source.mundane);

        self.occurs_as_entity_race.add_missing_data(&source.occurs_as_entity_race);
        self.savage.add_missing_data(&source.savage);
        self.small_race.add_missing_data(&source.small_race);

        self.two_genders.add_missing_data(&source.two_genders);
        self.ubiquitous.add_missing_data(&source.ubiquitous);

        self.vermin_eater.add_missing_data(&source.vermin_eater);
        self.vermin_fish.add_missing_data(&source.vermin_fish);
        self.vermin_grounder.add_missing_data(&source.vermin_grounder);
        self.vermin_rotter.add_missing_data(&source.vermin_rotter);
        self.vermin_soil.add_missing_data(&source.vermin_soil);
        self.vermin_soil_colony.add_missing_data(&source.vermin_soil_colony);
    }

    fn add_missing_data_indexed(&mut self, source: &Creature, index: u64) {
        self.id.add_missing_data(&(index as i32));
        self.biomes.cr_id.add_missing_data(&(index as i32));
        self.add_missing_data(source);
    }
}

impl PartialEq<df_st_core::Creature> for Creature {
    fn eq(&self, other: &df_st_core::Creature) -> bool {
        self.creature_id == other.creature_id
    }
}

impl std::hash::Hash for Creature {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.creature_id.hash(state);
    }
}

impl Filler<Vec<df_st_core::Creature>, CreatureRaw> for Vec<df_st_core::Creature> {
    fn add_missing_data(&mut self, source: &CreatureRaw) {
        self.add_missing_data(&source.creature);
    }
}

impl Filler<IndexMap<u64, df_st_core::Creature>, CreatureRaw>
    for IndexMap<u64, df_st_core::Creature>
{
    fn add_missing_data(&mut self, source: &CreatureRaw) {
        self.add_missing_data(&source.creature);
    }
}
