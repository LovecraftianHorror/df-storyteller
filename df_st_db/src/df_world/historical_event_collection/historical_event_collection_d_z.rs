use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::historical_event_collections_d_z;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_event_collections_d_z"]
#[primary_key(hec_id)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
pub struct HistoricalEventCollectionDZ {
    pub hec_id: i32,
    pub world_id: i32,

    pub d_support_merc_en_id: Option<i32>,
    pub defender_ent_id: Option<i32>,
    pub defending_en_id: Option<i32>,
    pub defending_merc_en_id: Option<i32>,
    pub feature_layer_id: Option<i32>,
    pub name: Option<String>,
    pub occasion_id: Option<i32>,
    pub ordinal: Option<i32>,
    pub parent_hec_id: Option<i32>,
    pub site_id: Option<i32>,
    pub subregion_id: Option<i32>,
    pub target_entity_id: Option<i32>,
    pub war_hec_id: Option<i32>,
}

impl HistoricalEventCollectionDZ {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEventCollection, HistoricalEventCollectionDZ>
    for HistoricalEventCollectionDZ
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        historical_event_collections_d_z: &[HistoricalEventCollectionDZ],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_event_collections_d_z::table)
            .values(historical_event_collections_d_z)
            .on_conflict((
                historical_event_collections_d_z::hec_id,
                historical_event_collections_d_z::world_id,
            ))
            .do_update()
            .set((
                historical_event_collections_d_z::d_support_merc_en_id.eq(excluded(
                    historical_event_collections_d_z::d_support_merc_en_id,
                )),
                historical_event_collections_d_z::defender_ent_id
                    .eq(excluded(historical_event_collections_d_z::defender_ent_id)),
                historical_event_collections_d_z::defending_en_id
                    .eq(excluded(historical_event_collections_d_z::defending_en_id)),
                historical_event_collections_d_z::defending_merc_en_id.eq(excluded(
                    historical_event_collections_d_z::defending_merc_en_id,
                )),
                historical_event_collections_d_z::feature_layer_id
                    .eq(excluded(historical_event_collections_d_z::feature_layer_id)),
                historical_event_collections_d_z::name
                    .eq(excluded(historical_event_collections_d_z::name)),
                historical_event_collections_d_z::occasion_id
                    .eq(excluded(historical_event_collections_d_z::occasion_id)),
                historical_event_collections_d_z::ordinal
                    .eq(excluded(historical_event_collections_d_z::ordinal)),
                historical_event_collections_d_z::parent_hec_id
                    .eq(excluded(historical_event_collections_d_z::parent_hec_id)),
                historical_event_collections_d_z::site_id
                    .eq(excluded(historical_event_collections_d_z::site_id)),
                historical_event_collections_d_z::subregion_id
                    .eq(excluded(historical_event_collections_d_z::subregion_id)),
                historical_event_collections_d_z::target_entity_id
                    .eq(excluded(historical_event_collections_d_z::target_entity_id)),
                historical_event_collections_d_z::war_hec_id
                    .eq(excluded(historical_event_collections_d_z::war_hec_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_event_collections_d_z");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        historical_event_collections_d_z: &[HistoricalEventCollectionDZ],
    ) {
        diesel::insert_into(historical_event_collections_d_z::table)
            .values(historical_event_collections_d_z)
            .execute(conn)
            .expect("Error saving historical_event_collections_d_z");
    }

    /// Get a list of HistoricalEventCollectionDZ from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventCollectionDZ>, Error> {
        /*
        use crate::schema::historical_event_collections_d_z::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_event_collections_d_z.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "d_support_merc_en_id" => d_support_merc_en_id,
                "defender_ent_id" => defender_ent_id,
                "defending_en_id" => defending_en_id,
                "defending_merc_en_id" => defending_merc_en_id,
                "feature_layer_id" => feature_layer_id,
                "name" => name,
                "occasion_id" => occasion_id,
                "ordinal" => ordinal,
                "parent_hec_id" => parent_hec_id,
                "site_id" => site_id,
                "subregion_id" => subregion_id,
                "target_entity_id" => target_entity_id,
                "war_hec_id" => war_hec_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventCollectionDZ>, Error> {
        use crate::schema::historical_event_collections_d_z::dsl::*;
        let query = historical_event_collections_d_z;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(query.first::<HistoricalEventCollectionDZ>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "d_support_merc_en_id" => "d_support_merc_en_id",
            "defender_ent_id" => "defender_ent_id",
            "defending_en_id" => "defending_en_id",
            "defending_hf_id" => "defending_hf_id",
            "defending_merc_en_id" => "defending_merc_en_id",
            "feature_layer_id" => "feature_layer_id",
            "name" => "name",
            "occasion_id" => "occasion_id",
            "ordinal" => "ordinal",
            "parent_hec_id" => "parent_hec_id",
            "site_id" => "site_id",
            "subregion_id" => "subregion_id",
            "target_entity_id" => "target_entity_id",
            "war_hec_id" => "war_hec_id",
            "hec_id" | _ => "hec_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventCollectionDZ],
        core_list: Vec<df_st_core::HistoricalEventCollection>,
    ) -> Result<Vec<df_st_core::HistoricalEventCollection>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_event_collections_d_z::dsl::*;
        let query = historical_event_collections_d_z.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "d_support_merc_en_id" => {d_support_merc_en_id: Option<i32>},
                "defender_ent_id" => {defender_ent_id: Option<i32>},
                "defending_en_id" => {defending_en_id: Option<i32>},
                "defending_merc_en_id" => {defending_merc_en_id: Option<i32>},
                "feature_layer_id" => {feature_layer_id: Option<i32>},
                "name" => {name: Option<String>},
                "occasion_id" => {occasion_id: Option<i32>},
                "ordinal" => {ordinal: Option<i32>},
                "parent_hec_id" => {parent_hec_id: Option<i32>},
                "site_id" => {site_id: Option<i32>},
                "subregion_id" => {subregion_id: Option<i32>},
                "target_entity_id" => {target_entity_id: Option<i32>},
                "war_hec_id" => {war_hec_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventCollectionDZ, df_st_core::HistoricalEventCollection>
    for HistoricalEventCollectionDZ
{
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEventCollection) {
        self.hec_id.add_missing_data(&source.id);
        self.d_support_merc_en_id
            .add_missing_data(&source.d_support_merc_en_id);
        self.defender_ent_id
            .add_missing_data(&source.defender_ent_id);
        self.defending_en_id
            .add_missing_data(&source.defending_en_id);
        self.defending_merc_en_id
            .add_missing_data(&source.defending_merc_en_id);
        self.feature_layer_id
            .add_missing_data(&source.feature_layer_id);
        self.name.add_missing_data(&source.name);
        self.occasion_id.add_missing_data(&source.occasion_id);
        self.ordinal.add_missing_data(&source.ordinal);
        self.parent_hec_id.add_missing_data(&source.parent_hec_id);
        self.site_id.add_missing_data(&source.site_id);
        self.subregion_id.add_missing_data(&source.subregion_id);
        self.target_entity_id
            .add_missing_data(&source.target_entity_id);
        self.war_hec_id.add_missing_data(&source.war_hec_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEventCollection, HistoricalEventCollectionDZ>
    for df_st_core::HistoricalEventCollection
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollectionDZ) {
        self.id.add_missing_data(&source.hec_id);
        self.d_support_merc_en_id
            .add_missing_data(&source.d_support_merc_en_id);
        self.defender_ent_id
            .add_missing_data(&source.defender_ent_id);
        self.defending_en_id
            .add_missing_data(&source.defending_en_id);
        self.defending_merc_en_id
            .add_missing_data(&source.defending_merc_en_id);
        self.feature_layer_id
            .add_missing_data(&source.feature_layer_id);
        self.name.add_missing_data(&source.name);
        self.occasion_id.add_missing_data(&source.occasion_id);
        self.ordinal.add_missing_data(&source.ordinal);
        self.parent_hec_id.add_missing_data(&source.parent_hec_id);
        self.site_id.add_missing_data(&source.site_id);
        self.subregion_id.add_missing_data(&source.subregion_id);
        self.target_entity_id
            .add_missing_data(&source.target_entity_id);
        self.war_hec_id.add_missing_data(&source.war_hec_id);
    }
}

impl PartialEq for HistoricalEventCollectionDZ {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id
    }
}

impl Hash for HistoricalEventCollectionDZ {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
    }
}

impl PartialEq<HistoricalEventCollectionDZ> for df_st_core::HistoricalEventCollection {
    fn eq(&self, other: &HistoricalEventCollectionDZ) -> bool {
        self.id == other.hec_id
    }
}

impl PartialEq<df_st_core::HistoricalEventCollection> for HistoricalEventCollectionDZ {
    fn eq(&self, other: &df_st_core::HistoricalEventCollection) -> bool {
        self.hec_id == other.id
    }
}
