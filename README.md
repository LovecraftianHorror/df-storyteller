# DF Storyteller
<!-- 
Badges to add later:
- pipeline/build status (windows, mac, linux)
- vulnerabilities see audit
- Forum (bay12)
-->
[![Pipeline status](https://gitlab.com/df_storyteller/df-storyteller/badges/master/pipeline.svg)](https://gitlab.com/df_storyteller/df-storyteller/-/commits/master)
[![Download](https://img.shields.io/badge/download-v0.1.1-green)](https://gitlab.com/df_storyteller/df-storyteller/-/releases)
[![Discord](https://img.shields.io/discord/708049042516738130?logo=discord)](https://discord.gg/aAXt6uu)
[![API Docs](https://img.shields.io/badge/docs-API-blue)](https://docs.dfstoryteller.com/rapidoc/)
[![Rust Docs](https://img.shields.io/badge/docs-Rust-blue)](https://docs.dfstoryteller.com/rust-docs/)
[![Coverage report](https://gitlab.com/df_storyteller/df-storyteller/badges/master/coverage.svg)](https://codecov.io/gl/df_storyteller/df-storyteller)

Dwarf Fortress Storyteller is an application that parses Dwarf Fortress legends
files, stores them internally and makes them queryable using API's.
DF Storyteller currently supports the parsing of:

* ...-legends.xml
* ...-legends_plus.xml
* ...-world_history.txt (Work in progress)
* ...-world_site_and_pops.txt (Work in progress)

DF Storyteller currently supports querying the data using:
* RESTfull API (read-only for now)
* GraphQL (read-only for now) (Work in progress)

## Why use DF Storyteller
In the past legends viewers had to implement there own legends parser to get
the data from the files. Keeping them up-to-data takes valuable time away from
making a nice interface. This application changes that. We take care of all the
complicated things. You can just ask for the exact data you need and we give
it to you. All you have to do is visualize the data in unique ways.

DF Storyteller also combines all the parsed files so you have all data available
to you. You can then chose which data you are interested in.

Documentation is important so we do our best to make all descriptions as as
clear as possible. For our API's we have build in documentation that is
automatically generated so all documentation is up-to-date.

Want to become a painter and create some beautiful visuals, [look here](#painter).

## Features

DF Storyteller is packed with features and more are planned, here are a few:

* **Low memory usage**: Application (+ PostgreSQL) < 100 MB RAM
  * Parsing of files still needs to be optimized and will use much more memory
    depending on the size of the legend files. But only has to be done once.
* **Small binary**: < 100 MB (and included most/all dependencies)
* **RESTfull and GraphQL**: interface to query the data.
* **View you world in detail on a Map!**: [Planned feature](https://gitlab.com/df_storyteller/df-storyteller/issues/54), not available yet.
* **Great documentation**: [API documentation](https://docs.dfstoryteller.com/rapidoc/), [Developer documentation](https://docs.dfstoryteller.com/rust-docs/), build in documentation and [other support documents](https://gitlab.com/df_storyteller/df-storyteller/-/tree/master/docs).
* **Best Effort Parsing**: Should work for [every version of Dwarf Fortress and DF Hack](https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/docs/versions.md#file-support-for-dwarf-fortress-versions).
* **Very stable and secure!**: We ❤️ Rust, it will always tell you what went wrong 
and has build in bug report creation.
* **Privacy**: We don't track you in any way! [Look here for more info about privacy](docs/privacy.md).

## How does it work?
DF Storyteller takes the legend files from Dwarf Fortress and DFHack and imports them into a database.
It then provides and API to view the data.

![Diagram of import and start commands](docs/images/How_it_works.svg)

## Install your own storyteller!
<a name="install"></a>
DF Storyteller is written to be used by other applications so it might
be included in the visualizer you want to use. Look here for a
[list of visualizers (painters)](docs/paintings.md).
To get started, download the executable from the
[release page](https://gitlab.com/df_storyteller/df-storyteller/-/releases).

[Here you can find instructions on how to install it for your system.](docs/install.md)

## Command your storyteller
<a name="use"></a>
DF Storyteller is designed as a command line tool. This allows us to provide
different functionalities depending on what the user wants. The commands can
also be used by other applications to open the application in different ways.

### Get started

To get started with the command line you can use the `help` flag in the command line like this: 

```bash
# Get the general help from the app
./df_storyteller --help
# For windows this changes to:
./df_storyteller.exe --help
```

The first subcommands you might want to use are `guide`, `import` and `start`.
For example:
```bash
./df_storyteller guide
./df_storyteller import -w 1 ./region4-00125-01-01-legends.xml
./df_storyteller start -w 1
```

You can then open http://localhost:20350 to find more info. 
The `start` command will print the exact url in the terminal, like this:
```text
INFO :launch - 🚀 Rocket has launched from http://127.0.0.1:20350
```

For more commands and more info look at [this page](docs/commands.md).

## Documentation

Every instance of DF Storyteller comes with its own
[build in documentation](docs/commands.md#docs). But we also provide documentation for
the most recent build [here](https://docs.dfstoryteller.com).

## Become a Painter
<a name="painter"></a>

Do you want to visualize legends data yourself! You are in the right place.
We will get you started in no time. Just [take a look over here](docs/become_a_painter.md).

## Need help?
Join us on [Discord](https://discord.gg/aAXt6uu) or [bay12forums](http://www.bay12forums.com/smf/index.php?topic=177074.0).

## System Requirements & Supported

DF Storyteller has very minimal system requirements that highly depend on the files that you want to import.

|                     | Minimal Requirements                                         | Recommended Requirements                |
| ------------------- | ------------------------------------------------------------ | --------------------------------------- |
| OS                  | Windows 64bit, Linux 64bit, MaxOS ...                        | Windows 64bit, Linux 64bit, MaxOS ...   |
| RAM[^1]             | 512MB (not accounting for PostgreSQL)                        | 1-2GB (not accounting for PostgreSQL)   |
| CPU                 | Works on any decent system, <br />might just take a bit longer to import | See minimal                             |
| Video card          | Not needed and not used.                                     | Not needed and not used.                |
| Internet connection | Not needed (only used to check for updated).<br />Although it useses the browser (almost) all assets are local. | See minimal                             |

[^1]: This highly depends on files you want to import using the `import` subcommand. The `start` subcommand uses very little RAM should will work on all modern systems.

For information about what versions of Dwarf Fortress, DFHack and mods we support [look here](docs/versions.md)

## Want to contribute?

* [Contribution guidelines for this project.](CONTRIBUTING.md)
* [Create your own application using DF Storyteller.](docs/paintings.md)
* I want to help, but not a programmer, can I still help?
Yes! [Look here for more info](docs/non-coding_help.md).

Our [Code of Conduct](docs/code_of_conduct.md).

## Other info

* [Security](docs/security.md)
* [Privacy](docs/privacy.md)
* [Technical info](docs/technical.md)

## License

This project is licensed under the [GPLv3 license](LICENSE).

All documentation[^2] is also licensed under 
[GNU FDL](https://www.gnu.org/licenses/fdl-1.3.html), 
[MIT license](https://choosealicense.com/licenses/mit/) and
[Creative Commons Attribution-ShareAlike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/)

This makes the documentation both compatible with the 
[Dwarf Fortress Wiki](https://dwarffortresswiki.org) and [Wikipedia](https://www.wikipedia.org/).

All contributions to this project will be similarly licensed.

[^2]: This includes all Rust Doc, API documentation and other info in this codebase.
