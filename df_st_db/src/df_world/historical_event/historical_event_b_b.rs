use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_b_b;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_b_b"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventBB {
    pub he_id: i32,
    pub world_id: i32,

    pub body_state: Option<String>,
    pub builder_hf_id: Option<i32>,
    pub building_profile_id: Option<i32>,
    pub body_part: Option<i32>,
    pub building_type: Option<String>,
    pub building_subtype: Option<String>,
}

impl HistoricalEventBB {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventBB> for HistoricalEventBB {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_b_b: &[HistoricalEventBB]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_b_b::table)
            .values(historical_events_b_b)
            .on_conflict((
                historical_events_b_b::he_id,
                historical_events_b_b::world_id,
            ))
            .do_update()
            .set((
                historical_events_b_b::body_state.eq(excluded(historical_events_b_b::body_state)),
                historical_events_b_b::builder_hf_id
                    .eq(excluded(historical_events_b_b::builder_hf_id)),
                historical_events_b_b::building_profile_id
                    .eq(excluded(historical_events_b_b::building_profile_id)),
                historical_events_b_b::body_part.eq(excluded(historical_events_b_b::body_part)),
                historical_events_b_b::building_type
                    .eq(excluded(historical_events_b_b::building_type)),
                historical_events_b_b::building_subtype
                    .eq(excluded(historical_events_b_b::building_subtype)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_b_b");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_b_b: &[HistoricalEventBB]) {
        diesel::insert_into(historical_events_b_b::table)
            .values(historical_events_b_b)
            .execute(conn)
            .expect("Error saving historical_events_b_b");
    }

    /// Get a list of HistoricalEventBB from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventBB>, Error> {
        /*
        use crate::schema::historical_events_b_b::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_b_b.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "body_state" => body_state,
                "builder_hf_id" => builder_hf_id,
                "building_profile_id" => building_profile_id,
                "body_part" => body_part,
                "building_type" => building_type,
                "building_subtype" => building_subtype,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventBB>, Error> {
        use crate::schema::historical_events_b_b::dsl::*;
        let query = historical_events_b_b;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventBB>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "body_state" => "body_state",
            "builder_hf_id" => "builder_hf_id",
            "building_profile_id" => "building_profile_id",
            "body_part" => "body_part",
            "building_type" => "building_type",
            "building_subtype" => "building_subtype",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventBB],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_b_b::dsl::*;
        let query = historical_events_b_b.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "body_state" => {body_state: Option<String>},
                "builder_hf_id" => {builder_hf_id: Option<i32>},
                "building_profile_id" => {building_profile_id: Option<i32>},
                "body_part" => {body_part: Option<i32>},
                "building_type" => {building_type: Option<String>},
                "building_subtype" => {building_subtype: Option<String>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        return Ok(vec![]);
    }
}

/// From Core to DB
impl Filler<HistoricalEventBB, df_st_core::HistoricalEvent> for HistoricalEventBB {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.body_state.add_missing_data(&source.body_state);
        self.builder_hf_id.add_missing_data(&source.builder_hf_id);
        self.building_profile_id
            .add_missing_data(&source.building_profile_id);
        self.body_part.add_missing_data(&source.body_part);
        self.building_type.add_missing_data(&source.building_type);
        self.building_subtype
            .add_missing_data(&source.building_subtype);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventBB> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventBB) {
        self.id.add_missing_data(&source.he_id);
        self.body_state.add_missing_data(&source.body_state);
        self.builder_hf_id.add_missing_data(&source.builder_hf_id);
        self.building_profile_id
            .add_missing_data(&source.building_profile_id);
        self.body_part.add_missing_data(&source.body_part);
        self.building_type.add_missing_data(&source.building_type);
        self.building_subtype
            .add_missing_data(&source.building_subtype);
    }
}

impl PartialEq for HistoricalEventBB {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventBB {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventBB> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventBB) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventBB {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
