# List of crates and other software used in this application and there licenses

All software in this list might require attribution and/or a copyright notice on
there part of the software. The version of the crate might not match the exact
version used in the application. But we presume the licenses does not change in
minor version changes.

| Name           | Version | License               | Link (mostly to repository)                  |
| -------------- | ------- | --------------------- | -------------------------------------------- |
| failure        | 0.1.8   | [MIT] OR [Apache-2.0] | https://github.com/rust-lang-nursery/failure |
| futures        | 0.3.4   | [MIT] OR [Apache-2.0] | https://github.com/rust-lang/futures-rs      |
| colored        | 1.9.3   | [MPL-2.0]             | https://github.com/mackwic/colored           |
| structopt      | 0.3.14  | [MIT] OR [Apache-2.0] | https://github.com/TeXitoi/structopt         |
| log            | 0.4.8   | [MIT] OR [Apache-2.0] | https://github.com/rust-lang/log             |
| serde          | 1.0.106 | [MIT] OR [Apache-2.0] | https://github.com/serde-rs/serde            |
| serde_json     | 1.0.52  | [MIT] OR [Apache-2.0] | https://github.com/serde-rs/json             |
| juniper        | 0.14.2  | [BSD-2-Clause]        | https://github.com/graphql-rust/juniper      |
| juniper_rocket | 0.5.2   | [BSD-2-Clause]        | https://github.com/graphql-rust/juniper      |
| rocket         | 0.4.4   | [MIT] OR [Apache-2.0] | https://github.com/SergioBenitez/Rocket      |
| rust-embed     | 5.5.1   | [MIT]                 | https://github.com/pyros2097/rust-embed      |
| rocket_contrib | 0.4.4   | [MIT] OR [Apache-2.0] | https://github.com/SergioBenitez/Rocket      |
| regex          | 1.3.7   | [MIT] OR [Apache-2.0] | https://github.com/rust-lang/regex           |
| schemars       | 0.7.2   | [MIT]                 | https://github.com/GREsau/schemars           |
| diesel         | 1.4.4   | [MIT] OR [Apache-2.0] | https://github.com/diesel-rs/diesel          |
| ~~dotenv~~     | ~~0.15.0~~ | ~~[MIT]~~          | ~~https://github.com/dotenv-rs/dotenv ~~     |
| r2d2           | 0.8.8   | [MIT] OR [Apache-2.0] | https://github.com/sfackler/r2d2             |
| syn            | 1.0.19  | [MIT] OR [Apache-2.0] | https://github.com/dtolnay/syn               |
| quote          | 1.0.4   | [MIT] OR [Apache-2.0] | https://github.com/dtolnay/quote             |
| proc-macro2    | 1.0.12  | [MIT] OR [Apache-2.0] | https://github.com/alexcrichton/proc-macro2  |
| serde-xml-rs   | 0.4.0   | [MIT]                 | https://github.com/RReverser/serde-xml-rs    |
| serde_path_to_error | 0.1.2 | [MIT] OR [Apache-2.0] | https://github.com/dtolnay/path-to-error  |
| okapi <sup>[1](#fn1)</sup> | 0.4.0 | [MIT]       | https://github.com/GREsau/okapi              |
| rocket_okapi <sup>[1](#fn1)</sup> | 0.4.1 | [MIT] | https://github.com/GREsau/okapi             |
| rocket_okapi_codegen <sup>[1](#fn1)</sup> | 0.4.1 | [MIT] | https://github.com/GREsau/okapi     |
| rapidoc        | 7.5.1   | [MIT]                 | https://github.com/mrin9/RapiDoc             |
| swagger-ui     | 3.25.2  | [Apache-2.0]          | https://github.com/swagger-api/swagger-ui    |
| PostgreSQL <sup>[2](#fn2)</sup> | 12.2 | [PostgreSQL] | https://www.postgresql.org/             |
| <!---add new crates here --> |  |                |                                              |
|                |         |                       |                                              |

<a name="fn1">1</a>: These crates might be altered or changed from there main repository.
<a name="fn2">2</a>: Although not included in application binary, still used by it.

[MIT]: https://choosealicense.com/licenses/mit/
[Apache-2.0]: https://choosealicense.com/licenses/apache-2.0/
[MPL-2.0]: https://choosealicense.com/licenses/mpl-2.0/
[BSD-2-Clause]: https://choosealicense.com/licenses/bsd-2-clause/
[PostgreSQL]: https://choosealicense.com/licenses/postgresql/

----

This document was last update on: 2020/05/08
