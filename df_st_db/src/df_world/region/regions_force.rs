use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, Region};
use crate::schema::regions_forces;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::fmt;

#[derive(Clone, Queryable, Insertable, Default, Associations, Identifiable, Fillable, Filler)]
#[table_name = "regions_forces"]
#[primary_key(region_id, force_id)]
#[belongs_to(Region)]
pub struct RegionForce {
    pub region_id: i32,
    pub force_id: i32,
    pub world_id: i32,
}

impl RegionForce {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<RegionForce, RegionForce> for RegionForce {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for region in core_world.regions.values() {
            for force_id in &region.force_id {
                world.regions_forces.push(RegionForce {
                    region_id: region.id,
                    force_id: *force_id,
                    ..Default::default()
                });
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, regions_forces: &[RegionForce]) {
        diesel::insert_into(regions_forces::table)
            .values(regions_forces)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving regions_forces");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, regions_forces: &[RegionForce]) {
        diesel::insert_into(regions_forces::table)
            .values(regions_forces)
            .execute(conn)
            .expect("Error saving regions_forces");
    }

    /// Get a list of RegionForce from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<RegionForce>, Error> {
        use crate::schema::regions_forces::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = regions_forces.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "region_id" => region_id,
                "force_id" => force_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "region_id" => region_id,
                "force_id" => force_id,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<RegionForce>, Error> {
        use crate::schema::regions_forces::dsl::*;
        let query = regions_forces;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(region_id.eq(id_filter.get("region_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "force_id" => force_id,
            ],
            {return Ok(query.first::<RegionForce>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "force_id" => "force_id",
            "region_id" | _ => "region_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[RegionForce],
        core_list: Vec<RegionForce>,
    ) -> Result<Vec<RegionForce>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::regions_forces::dsl::*;
        let query = regions_forces.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "region_id" => region_id,
                "force_id" => force_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "region_id" => {region_id: i32},
                "force_id" => {force_id: i32},
            };},
        };
    }
}

impl PartialEq for RegionForce {
    fn eq(&self, other: &Self) -> bool {
        self.region_id == other.region_id && self.force_id == other.force_id
    }
}

/// Force a new format for `RegionForce` to not clutter the screen
impl fmt::Debug for RegionForce {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&format! {"(region_id: {}, force_id: {})",
        self.region_id, self.force_id})
    }
}

impl fmt::Display for RegionForce {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "(region_id: {}, force_id: {})",
            self.region_id, self.force_id
        )
    }
}
