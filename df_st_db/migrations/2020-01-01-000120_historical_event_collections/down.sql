
DROP TABLE historical_event_collections;
DROP TABLE historical_event_collections_a_c;
DROP TABLE historical_event_collections_d_z;
DROP TABLE hec_he_ids;
DROP TABLE hec_related_ids;
DROP TABLE hec_individual_mercs;
DROP TABLE hec_noncom_hf_ids;
DROP TABLE hec_outcomes;
DROP TABLE hec_attacking_hf_ids;
DROP TABLE hec_defending_hf_ids;
DROP TABLE hec_a_support_merc_hf_ids;
DROP TABLE hec_d_support_merc_hf_ids;
