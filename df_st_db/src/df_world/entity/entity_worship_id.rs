use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_worship_ids;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_worship_ids"]
#[primary_key(en_id, worship_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityWorshipID {
    pub en_id: i32,
    pub worship_id: i32,
    pub world_id: i32,
}

impl EntityWorshipID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<EntityWorshipID, EntityWorshipID> for EntityWorshipID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_worship_ids: &[EntityWorshipID]) {
        diesel::insert_into(entity_worship_ids::table)
            .values(entity_worship_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving entity_worship_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_worship_ids: &[EntityWorshipID]) {
        diesel::insert_into(entity_worship_ids::table)
            .values(entity_worship_ids)
            .execute(conn)
            .expect("Error saving entity_worship_ids");
    }

    /// Get a list of EntityWorshipID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<EntityWorshipID>, Error> {
        use crate::schema::entity_worship_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_worship_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "worship_id" => worship_id,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityWorshipID>, Error> {
        use crate::schema::entity_worship_ids::dsl::*;
        let query = entity_worship_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {return Ok(query.first::<EntityWorshipID>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "worship_id" => "worship_id",
            "en_id" | _ => "en_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityWorshipID],
        core_list: Vec<EntityWorshipID>,
    ) -> Result<Vec<EntityWorshipID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_worship_ids::dsl::*;
        let query = entity_worship_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "worship_id" => {worship_id: i32},
            };},
        };
    }
}

impl PartialEq for EntityWorshipID {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.worship_id == other.worship_id
    }
}

impl Hash for EntityWorshipID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.worship_id.hash(state);
    }
}
