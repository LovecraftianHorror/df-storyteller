use crate::deserializers::{coordinate_deserializer, path_deserializer};
use df_st_core::{Coordinate, Filler, HasUnknown, Path};
use df_st_derive::HasUnknown;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct River {
    // ID not given in xml, but later generated.
    // Might be added later to xml
    pub id: Option<i32>,
    pub name: Option<String>,
    #[serde(deserialize_with = "path_deserializer")]
    #[serde(default)]
    pub path: Option<Vec<Path>>,
    #[serde(deserialize_with = "coordinate_deserializer")]
    #[serde(default)]
    pub end_pos: Option<Coordinate>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Rivers {
    pub river: Option<Vec<River>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::River, River> for df_st_core::River {
    fn add_missing_data(&mut self, source: &River) {
        if let Some(id) = source.id {
            // Override generated ID if ever added to output
            self.id.add_missing_data(&id);
        }
        self.name.add_missing_data(&source.name);
        self.path.add_missing_data(&source.path);
        self.end_pos.add_missing_data(&source.end_pos);
    }

    fn add_missing_data_indexed(&mut self, source: &River, index: u64) {
        if source.id.is_none() {
            // If no ID set, use index
            self.id.add_missing_data(&(index as i32));
        }
        self.add_missing_data(source);
    }
}

impl PartialEq<df_st_core::River> for River {
    fn eq(&self, other: &df_st_core::River) -> bool {
        self.name == other.name
            && match &self.path {
                Some(path) => path == &other.path,
                None => other.path.is_empty(),
            }
    }
}

impl std::hash::Hash for River {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
        self.path.hash(state);
    }
}

impl Filler<Vec<df_st_core::River>, Rivers> for Vec<df_st_core::River> {
    fn add_missing_data(&mut self, source: &Rivers) {
        self.add_missing_data(&source.river);
    }
}

impl Filler<IndexMap<u64, df_st_core::River>, Rivers> for IndexMap<u64, df_st_core::River> {
    fn add_missing_data(&mut self, source: &Rivers) {
        self.add_missing_data(&source.river);
    }
}
