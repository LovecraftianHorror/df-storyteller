use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::historical_event_collections_a_c;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_event_collections_a_c"]
#[primary_key(hec_id)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
pub struct HistoricalEventCollectionAC {
    pub hec_id: i32,
    pub world_id: i32,

    pub a_support_merc_en_id: Option<i32>,
    pub adjective: Option<String>,
    pub aggressor_ent_id: Option<i32>,
    pub attacking_en_id: Option<i32>,
    pub attacking_merc_enid: Option<i32>,
    pub civ_id: Option<i32>,
    pub company_merc: Option<bool>,
    pub coords: Option<String>,
}

impl HistoricalEventCollectionAC {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEventCollection, HistoricalEventCollectionAC>
    for HistoricalEventCollectionAC
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        historical_event_collections_a_c: &[HistoricalEventCollectionAC],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_event_collections_a_c::table)
            .values(historical_event_collections_a_c)
            .on_conflict((
                historical_event_collections_a_c::hec_id,
                historical_event_collections_a_c::world_id,
            ))
            .do_update()
            .set((
                historical_event_collections_a_c::a_support_merc_en_id.eq(excluded(
                    historical_event_collections_a_c::a_support_merc_en_id,
                )),
                historical_event_collections_a_c::adjective
                    .eq(excluded(historical_event_collections_a_c::adjective)),
                historical_event_collections_a_c::aggressor_ent_id
                    .eq(excluded(historical_event_collections_a_c::aggressor_ent_id)),
                historical_event_collections_a_c::attacking_en_id
                    .eq(excluded(historical_event_collections_a_c::attacking_en_id)),
                historical_event_collections_a_c::attacking_merc_enid.eq(excluded(
                    historical_event_collections_a_c::attacking_merc_enid,
                )),
                historical_event_collections_a_c::civ_id
                    .eq(excluded(historical_event_collections_a_c::civ_id)),
                historical_event_collections_a_c::company_merc
                    .eq(excluded(historical_event_collections_a_c::company_merc)),
                historical_event_collections_a_c::coords
                    .eq(excluded(historical_event_collections_a_c::coords)),
            ))
            .execute(conn)
            .expect("Error saving historical_event_collections_a_c");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        historical_event_collections_a_c: &[HistoricalEventCollectionAC],
    ) {
        diesel::insert_into(historical_event_collections_a_c::table)
            .values(historical_event_collections_a_c)
            .execute(conn)
            .expect("Error saving historical_event_collections_a_c");
    }

    /// Get a list of HistoricalEventCollectionAC from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventCollectionAC>, Error> {
        /*
        use crate::schema::historical_event_collections_a_c::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_event_collections_a_c.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "a_support_merc_en_id" => a_support_merc_en_id,
                "adjective" => adjective,
                "aggressor_ent_id" => aggressor_ent_id,
                "attacking_en_id" => attacking_en_id,
                "attacking_merc_enid" => attacking_merc_enid,
                "civ_id" => civ_id,
                "company_merc" => company_merc,
                "coords" => coords,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventCollectionAC>, Error> {
        use crate::schema::historical_event_collections_a_c::dsl::*;
        let query = historical_event_collections_a_c;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(query.first::<HistoricalEventCollectionAC>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "a_support_merc_en_id" => "a_support_merc_en_id",
            "adjective" => "adjective",
            "aggressor_ent_id" => "aggressor_ent_id",
            "attacking_en_id" => "attacking_en_id",
            "attacking_merc_enid" => "attacking_merc_enid",
            "civ_id" => "civ_id",
            "company_merc" => "company_merc",
            "coords" => "coords",
            "hec_id" | _ => "hec_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventCollectionAC],
        core_list: Vec<df_st_core::HistoricalEventCollection>,
    ) -> Result<Vec<df_st_core::HistoricalEventCollection>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_event_collections_a_c::dsl::*;
        let query = historical_event_collections_a_c.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "a_support_merc_en_id" => {a_support_merc_en_id: Option<i32>},
                "adjective" => {adjective: Option<String>},
                "aggressor_ent_id" => {aggressor_ent_id: Option<i32>},
                "attacking_en_id" => {attacking_en_id: Option<i32>},
                "attacking_merc_enid" => {attacking_merc_enid: Option<i32>},
                "civ_id" => {civ_id: Option<i32>},
                "company_merc" => {company_merc: Option<bool>},
                "coords" => {coords: Option<String>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventCollectionAC, df_st_core::HistoricalEventCollection>
    for HistoricalEventCollectionAC
{
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEventCollection) {
        self.hec_id.add_missing_data(&source.id);
        self.a_support_merc_en_id
            .add_missing_data(&source.a_support_merc_en_id);
        self.adjective.add_missing_data(&source.adjective);
        self.aggressor_ent_id
            .add_missing_data(&source.aggressor_ent_id);
        self.attacking_en_id
            .add_missing_data(&source.attacking_en_id);
        self.attacking_merc_enid
            .add_missing_data(&source.attacking_merc_enid);
        self.civ_id.add_missing_data(&source.civ_id);
        self.company_merc.add_missing_data(&source.company_merc);
        self.coords.add_missing_data(&source.coords);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEventCollection, HistoricalEventCollectionAC>
    for df_st_core::HistoricalEventCollection
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollectionAC) {
        self.id.add_missing_data(&source.hec_id);
        self.a_support_merc_en_id
            .add_missing_data(&source.a_support_merc_en_id);
        self.adjective.add_missing_data(&source.adjective);
        self.aggressor_ent_id
            .add_missing_data(&source.aggressor_ent_id);
        self.attacking_en_id
            .add_missing_data(&source.attacking_en_id);
        self.attacking_merc_enid
            .add_missing_data(&source.attacking_merc_enid);
        self.civ_id.add_missing_data(&source.civ_id);
        self.company_merc.add_missing_data(&source.company_merc);
        self.coords.add_missing_data(&source.coords);
    }
}

impl PartialEq for HistoricalEventCollectionAC {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id
    }
}

impl Hash for HistoricalEventCollectionAC {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
    }
}

impl PartialEq<HistoricalEventCollectionAC> for df_st_core::HistoricalEventCollection {
    fn eq(&self, other: &HistoricalEventCollectionAC) -> bool {
        self.id == other.hec_id
    }
}

impl PartialEq<df_st_core::HistoricalEventCollection> for HistoricalEventCollectionAC {
    fn eq(&self, other: &df_st_core::HistoricalEventCollection) -> bool {
        self.hec_id == other.id
    }
}
