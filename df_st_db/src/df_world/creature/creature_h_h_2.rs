use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{Creature, DBDFWorld};
use crate::schema::creatures_h_h_2;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "creatures_h_h_2"]
#[primary_key(cr_id)]
#[belongs_to(Creature, foreign_key = "cr_id")]
pub struct CreatureHH2 {
    pub cr_id: i32,
    pub world_id: i32,

    pub has_any_megabeast: Option<bool>,
    pub has_any_mischievous: Option<bool>,
    pub has_any_natural_animal: Option<bool>,
    pub has_any_night_creature: Option<bool>,
    pub has_any_night_creature_bogeyman: Option<bool>,
    pub has_any_night_creature_experimenter: Option<bool>,
    pub has_any_night_creature_hunter: Option<bool>,
    pub has_any_night_creature_nightmare: Option<bool>,
    pub has_any_not_fireimmune: Option<bool>,
    pub has_any_not_flier: Option<bool>,
    pub has_any_not_living: Option<bool>,
    pub has_any_outsider_controllable: Option<bool>,
    pub has_any_power: Option<bool>,
    pub has_any_race_gait: Option<bool>,
    pub has_any_semimegabeast: Option<bool>,
    pub has_any_slow_learner: Option<bool>,
    pub has_any_supernatural: Option<bool>,
    pub has_any_titan: Option<bool>,
    pub has_any_unique_demon: Option<bool>,
    pub has_any_utterances: Option<bool>,
    pub has_any_vermin_hateable: Option<bool>,
    pub has_any_vermin_micro: Option<bool>,
    pub has_female: Option<bool>,
    pub has_male: Option<bool>,
}

impl CreatureHH2 {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Creature, CreatureHH2> for CreatureHH2 {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, creatures_h_h_2: &[CreatureHH2]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(creatures_h_h_2::table)
            .values(creatures_h_h_2)
            .on_conflict((creatures_h_h_2::cr_id, creatures_h_h_2::world_id))
            .do_update()
            .set((
                creatures_h_h_2::has_any_megabeast.eq(excluded(creatures_h_h_2::has_any_megabeast)),
                creatures_h_h_2::has_any_mischievous
                    .eq(excluded(creatures_h_h_2::has_any_mischievous)),
                creatures_h_h_2::has_any_natural_animal
                    .eq(excluded(creatures_h_h_2::has_any_natural_animal)),
                creatures_h_h_2::has_any_night_creature
                    .eq(excluded(creatures_h_h_2::has_any_night_creature)),
                creatures_h_h_2::has_any_night_creature_bogeyman
                    .eq(excluded(creatures_h_h_2::has_any_night_creature_bogeyman)),
                creatures_h_h_2::has_any_night_creature_experimenter.eq(excluded(
                    creatures_h_h_2::has_any_night_creature_experimenter,
                )),
                creatures_h_h_2::has_any_night_creature_hunter
                    .eq(excluded(creatures_h_h_2::has_any_night_creature_hunter)),
                creatures_h_h_2::has_any_night_creature_nightmare
                    .eq(excluded(creatures_h_h_2::has_any_night_creature_nightmare)),
                creatures_h_h_2::has_any_not_fireimmune
                    .eq(excluded(creatures_h_h_2::has_any_not_fireimmune)),
                creatures_h_h_2::has_any_not_flier.eq(excluded(creatures_h_h_2::has_any_not_flier)),
                creatures_h_h_2::has_any_not_living
                    .eq(excluded(creatures_h_h_2::has_any_not_living)),
                creatures_h_h_2::has_any_outsider_controllable
                    .eq(excluded(creatures_h_h_2::has_any_outsider_controllable)),
                creatures_h_h_2::has_any_power.eq(excluded(creatures_h_h_2::has_any_power)),
                creatures_h_h_2::has_any_race_gait.eq(excluded(creatures_h_h_2::has_any_race_gait)),
                creatures_h_h_2::has_any_semimegabeast
                    .eq(excluded(creatures_h_h_2::has_any_semimegabeast)),
                creatures_h_h_2::has_any_slow_learner
                    .eq(excluded(creatures_h_h_2::has_any_slow_learner)),
                creatures_h_h_2::has_any_supernatural
                    .eq(excluded(creatures_h_h_2::has_any_supernatural)),
                creatures_h_h_2::has_any_titan.eq(excluded(creatures_h_h_2::has_any_titan)),
                creatures_h_h_2::has_any_unique_demon
                    .eq(excluded(creatures_h_h_2::has_any_unique_demon)),
                creatures_h_h_2::has_any_utterances
                    .eq(excluded(creatures_h_h_2::has_any_utterances)),
                creatures_h_h_2::has_any_vermin_hateable
                    .eq(excluded(creatures_h_h_2::has_any_vermin_hateable)),
                creatures_h_h_2::has_any_vermin_micro
                    .eq(excluded(creatures_h_h_2::has_any_vermin_micro)),
                creatures_h_h_2::has_female.eq(excluded(creatures_h_h_2::has_female)),
                creatures_h_h_2::has_male.eq(excluded(creatures_h_h_2::has_male)),
            ))
            .execute(conn)
            .expect("Error saving creatures_h_h_2");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, creatures_h_h_2: &[CreatureHH2]) {
        diesel::insert_into(creatures_h_h_2::table)
            .values(creatures_h_h_2)
            .execute(conn)
            .expect("Error saving creatures_h_h_2");
    }

    /// Get a list of CreatureHH2 from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<CreatureHH2>, Error> {
        /*
        use crate::schema::creatures_h_h_2::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = creatures_h_h_2.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "cr_id" => cr_id,
                "has_any_megabeast" => has_any_megabeast,
                "has_any_mischievous" => has_any_mischievous,
                "has_any_natural_animal" => has_any_natural_animal,
                "has_any_night_creature" => has_any_night_creature,
                "has_any_night_creature_bogeyman" => has_any_night_creature_bogeyman,
                "has_any_night_creature_experimenter" => has_any_night_creature_experimenter,
                "has_any_night_creature_hunter" => has_any_night_creature_hunter,
                "has_any_night_creature_nightmare" => has_any_night_creature_nightmare,
                "has_any_not_fireimmune" => has_any_not_fireimmune,
                "has_any_not_flier" => has_any_not_flier,
                "has_any_not_living" => has_any_not_living,
                "has_any_outsider_controllable" => has_any_outsider_controllable,
                "has_any_power" => has_any_power,
                "has_any_race_gait" => has_any_race_gait,
                "has_any_semimegabeast" => has_any_semimegabeast,
                "has_any_slow_learner" => has_any_slow_learner,
                "has_any_supernatural" => has_any_supernatural,
                "has_any_titan" => has_any_titan,
                "has_any_unique_demon" => has_any_unique_demon,
                "has_any_utterances" => has_any_utterances,
                "has_any_vermin_hateable" => has_any_vermin_hateable,
                "has_any_vermin_micro" => has_any_vermin_micro,
                "has_female" => has_female,
                "has_male" => has_male,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<CreatureHH2>, Error> {
        use crate::schema::creatures_h_h_2::dsl::*;
        let query = creatures_h_h_2;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(cr_id.eq(id_filter.get("cr_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(query.first::<CreatureHH2>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "has_any_megabeast" => "has_any_megabeast",
            "has_any_mischievous" => "has_any_mischievous",
            "has_any_natural_animal" => "has_any_natural_animal",
            "has_any_night_creature" => "has_any_night_creature",
            "has_any_night_creature_bogeyman" => "has_any_night_creature_bogeyman",
            "has_any_night_creature_experimenter" => "has_any_night_creature_experimenter",
            "has_any_night_creature_hunter" => "has_any_night_creature_hunter",
            "has_any_night_creature_nightmare" => "has_any_night_creature_nightmare",
            "has_any_not_fireimmune" => "has_any_not_fireimmune",
            "has_any_not_flier" => "has_any_not_flier",
            "has_any_not_living" => "has_any_not_living",
            "has_any_outsider_controllable" => "has_any_outsider_controllable",
            "has_any_power" => "has_any_power",
            "has_any_race_gait" => "has_any_race_gait",
            "has_any_semimegabeast" => "has_any_semimegabeast",
            "has_any_slow_learner" => "has_any_slow_learner",
            "has_any_supernatural" => "has_any_supernatural",
            "has_any_titan" => "has_any_titan",
            "has_any_unique_demon" => "has_any_unique_demon",
            "has_any_utterances" => "has_any_utterances",
            "has_any_vermin_hateable" => "has_any_vermin_hateable",
            "has_any_vermin_micro" => "has_any_vermin_micro",
            "has_female" => "has_female",
            "has_male" => "has_male",
            "cr_id" | _ => "cr_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[CreatureHH2],
        core_list: Vec<df_st_core::Creature>,
    ) -> Result<Vec<df_st_core::Creature>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::creatures_h_h_2::dsl::*;
        let query = creatures_h_h_2.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "cr_id" => {cr_id: i32},
                "has_any_megabeast" => {has_any_megabeast: Option<bool>},
                "has_any_mischievous" => {has_any_mischievous: Option<bool>},
                "has_any_natural_animal" => {has_any_natural_animal: Option<bool>},
                "has_any_night_creature" => {has_any_night_creature: Option<bool>},
                "has_any_night_creature_bogeyman" => {has_any_night_creature_bogeyman: Option<bool>},
                "has_any_night_creature_experimenter" => {has_any_night_creature_experimenter: Option<bool>},
                "has_any_night_creature_hunter" => {has_any_night_creature_hunter: Option<bool>},
                "has_any_night_creature_nightmare" => {has_any_night_creature_nightmare: Option<bool>},
                "has_any_not_fireimmune" => {has_any_not_fireimmune: Option<bool>},
                "has_any_not_flier" => {has_any_not_flier: Option<bool>},
                "has_any_not_living" => {has_any_not_living: Option<bool>},
                "has_any_outsider_controllable" => {has_any_outsider_controllable: Option<bool>},
                "has_any_power" => {has_any_power: Option<bool>},
                "has_any_race_gait" => {has_any_race_gait: Option<bool>},
                "has_any_semimegabeast" => {has_any_semimegabeast: Option<bool>},
                "has_any_slow_learner" => {has_any_slow_learner: Option<bool>},
                "has_any_supernatural" => {has_any_supernatural: Option<bool>},
                "has_any_titan" => {has_any_titan: Option<bool>},
                "has_any_unique_demon" => {has_any_unique_demon: Option<bool>},
                "has_any_utterances" => {has_any_utterances: Option<bool>},
                "has_any_vermin_hateable" => {has_any_vermin_hateable: Option<bool>},
                "has_any_vermin_micro" => {has_any_vermin_micro: Option<bool>},
                "has_female" => {has_female: Option<bool>},
                "has_male" => {has_male: Option<bool>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<CreatureHH2, df_st_core::Creature> for CreatureHH2 {
    fn add_missing_data(&mut self, source: &df_st_core::Creature) {
        self.cr_id.add_missing_data(&source.id);
        self.has_any_megabeast
            .add_missing_data(&source.has_any_megabeast);
        self.has_any_mischievous
            .add_missing_data(&source.has_any_mischievous);
        self.has_any_natural_animal
            .add_missing_data(&source.has_any_natural_animal);
        self.has_any_night_creature
            .add_missing_data(&source.has_any_night_creature);
        self.has_any_night_creature_bogeyman
            .add_missing_data(&source.has_any_night_creature_bogeyman);
        self.has_any_night_creature_experimenter
            .add_missing_data(&source.has_any_night_creature_experimenter);
        self.has_any_night_creature_hunter
            .add_missing_data(&source.has_any_night_creature_hunter);
        self.has_any_night_creature_nightmare
            .add_missing_data(&source.has_any_night_creature_nightmare);
        self.has_any_not_fireimmune
            .add_missing_data(&source.has_any_not_fireimmune);
        self.has_any_not_flier
            .add_missing_data(&source.has_any_not_flier);
        self.has_any_not_living
            .add_missing_data(&source.has_any_not_living);
        self.has_any_outsider_controllable
            .add_missing_data(&source.has_any_outsider_controllable);
        self.has_any_power.add_missing_data(&source.has_any_power);
        self.has_any_race_gait
            .add_missing_data(&source.has_any_race_gait);
        self.has_any_semimegabeast
            .add_missing_data(&source.has_any_semimegabeast);
        self.has_any_slow_learner
            .add_missing_data(&source.has_any_slow_learner);
        self.has_any_supernatural
            .add_missing_data(&source.has_any_supernatural);
        self.has_any_titan.add_missing_data(&source.has_any_titan);
        self.has_any_unique_demon
            .add_missing_data(&source.has_any_unique_demon);
        self.has_any_utterances
            .add_missing_data(&source.has_any_utterances);
        self.has_any_vermin_hateable
            .add_missing_data(&source.has_any_vermin_hateable);
        self.has_any_vermin_micro
            .add_missing_data(&source.has_any_vermin_micro);
        self.has_female.add_missing_data(&source.has_female);
        self.has_male.add_missing_data(&source.has_male);
    }
}

/// From DB to Core
impl Filler<df_st_core::Creature, CreatureHH2> for df_st_core::Creature {
    fn add_missing_data(&mut self, source: &CreatureHH2) {
        self.id.add_missing_data(&source.cr_id);
        self.has_any_megabeast
            .add_missing_data(&source.has_any_megabeast);
        self.has_any_mischievous
            .add_missing_data(&source.has_any_mischievous);
        self.has_any_natural_animal
            .add_missing_data(&source.has_any_natural_animal);
        self.has_any_night_creature
            .add_missing_data(&source.has_any_night_creature);
        self.has_any_night_creature_bogeyman
            .add_missing_data(&source.has_any_night_creature_bogeyman);
        self.has_any_night_creature_experimenter
            .add_missing_data(&source.has_any_night_creature_experimenter);
        self.has_any_night_creature_hunter
            .add_missing_data(&source.has_any_night_creature_hunter);
        self.has_any_night_creature_nightmare
            .add_missing_data(&source.has_any_night_creature_nightmare);
        self.has_any_not_fireimmune
            .add_missing_data(&source.has_any_not_fireimmune);
        self.has_any_not_flier
            .add_missing_data(&source.has_any_not_flier);
        self.has_any_not_living
            .add_missing_data(&source.has_any_not_living);
        self.has_any_outsider_controllable
            .add_missing_data(&source.has_any_outsider_controllable);
        self.has_any_power.add_missing_data(&source.has_any_power);
        self.has_any_race_gait
            .add_missing_data(&source.has_any_race_gait);
        self.has_any_semimegabeast
            .add_missing_data(&source.has_any_semimegabeast);
        self.has_any_slow_learner
            .add_missing_data(&source.has_any_slow_learner);
        self.has_any_supernatural
            .add_missing_data(&source.has_any_supernatural);
        self.has_any_titan.add_missing_data(&source.has_any_titan);
        self.has_any_unique_demon
            .add_missing_data(&source.has_any_unique_demon);
        self.has_any_utterances
            .add_missing_data(&source.has_any_utterances);
        self.has_any_vermin_hateable
            .add_missing_data(&source.has_any_vermin_hateable);
        self.has_any_vermin_micro
            .add_missing_data(&source.has_any_vermin_micro);
        self.has_female.add_missing_data(&source.has_female);
        self.has_male.add_missing_data(&source.has_male);
    }
}

impl PartialEq for CreatureHH2 {
    fn eq(&self, other: &Self) -> bool {
        self.cr_id == other.cr_id
    }
}

impl Hash for CreatureHH2 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cr_id.hash(state);
    }
}

impl PartialEq<CreatureHH2> for df_st_core::Creature {
    fn eq(&self, other: &CreatureHH2) -> bool {
        self.id == other.cr_id
    }
}

impl PartialEq<df_st_core::Creature> for CreatureHH2 {
    fn eq(&self, other: &df_st_core::Creature) -> bool {
        self.cr_id == other.id
    }
}
