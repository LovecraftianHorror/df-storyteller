#![forbid(unsafe_code)]
#![deny(clippy::all)]
#![doc(html_root_url = "https://docs.dfstoryteller.com/rust-docs/")]
#![doc(html_favicon_url = "https://docs.dfstoryteller.com/favicon/favicon-16x16.png")]
#![doc(html_logo_url = "https://docs.dfstoryteller.com/logo.svg")]

//! # DF Storyteller - Main CLI Documentation
//!
//! This application is hosted on [GitLab](https://gitlab.com/df_storyteller/df-storyteller/).
//! If you have questions, join out [Discord](https://discord.gg/aAXt6uu) server.
//!
//! These documentation pages are documenting the code itself. If you are looking for
//! the API documentation, look [here](https://docs.dfstoryteller.com/rapidoc/).
//!
//! If you find any mistakes, typo's, bug, ... please report them
//! [here](https://gitlab.com/df_storyteller/df-storyteller/issues).
//!
//! To download DF Storyteller or find outer links look on [our website](https://dfstoryteller.com/).
//!
//! ## Workspaces
//! DF Storyteller is split up into different workspaces:
//! * [`df_st_cli`](crate): (in documentation referred to as `df_storyteller`)
//! This is the start of the CLI (Command-line-interface) application.
//! This has mostly to do with logging, error handling and the command line interface.
//!
//! * [`df_st_core`](df_st_core): One of the most important workspaces as it includes all the
//! structures that are returned via the API and the structure that is filled using
//! the parser. This workspace also includes most of the structure documentation
//! that is used to create the various documentation. Structures (`structs`) in this
//! workspace should be changes with care as they are used by almost all other workspaces.
//!
//! * [`df_st_db`](df_st_db): This workspace includes all the code and structures to store and
//! query data from the database. It also include code to copy all the data from the
//! database object to the Core structures.
//!
//! * [`df_st_derive`](../df_st_derive/): This is a special workspace it includes code for
//! `#\[derive()\]` macros. Working on this workspace is not easy and you should only
//! touch this if you know how
//! [procedural macros](https://doc.rust-lang.org/reference/procedural-macros.html)
//! work. This is basically code that writes code during compile time.
//!
//! * [`df_st_guide`](df_st_guide): A guide to help you setup DF Storyteller.
//! This workspace includes all the webpages for the guide.
//!
//! * [`df_st_api`](df_st_api): This includes all the RESTfull and GraphQL API code and static
//! pages. The documentation generation code for the APIs also live here.
//!
//! * [`df_st_legends`](../df_st_legends/): Everything to do with parsing the `...-legends.xml` files.
//! And changing this data into Core structures.
//!
//! * [`df_st_legends_plus`](../df_st_legends_plus/): Everything to do with parsing the
//! `...-legends_plus.xml` files from [DFHack](https://github.com/DFHack/dfhack) and
//! changing this data into Core structures.
//!
//! * [`df_st_world_history`](../df_st_world_history/): Everything to do with parsing the
//! `...-world_history.txt` files. And changing this data into Core structures.
//!
//! * [`df_st_world_sites_and_pops`](../df_st_world_sites_and_pops/): Everything to do with parsing the
//! `...-world_sites_and_pops.txt` files. And changing this data into Core structures.
//!
//! * [`df_st_parser`](df_st_parser): Code that interacts with all the parser workspaces
//! (like `df_st_legends`) and handling of unknown data found in the parsed files.
//!
//! * [`df_st_updater`](df_st_updater): Code for checking for updates and deprecating old or unsafe
//! versions.
//!
//! * [`df_st_image_maps`](../df_st_image_maps/): Everything to do with parsing the
//! world map image files and converting them to png.
//!
//! * [`df_st_image_site_maps`](../df_st_image_site_maps/): Everything to do with parsing the
//! site map image files and converting them to png.
//!
//!

/// Points to the modules with the main code.
/// See [main module here](bin::cli).
mod bin;

/// Start of the DF Storyteller CLI application.
fn main() -> std::io::Result<()> {
    bin::cli::main()
}
