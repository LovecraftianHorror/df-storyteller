table! {
    artifacts (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        site_id -> Nullable<Int4>,
        structure_local_id -> Nullable<Int4>,
        holder_hfid -> Nullable<Int4>,
        abs_tile_x -> Nullable<Int4>,
        abs_tile_y -> Nullable<Int4>,
        abs_tile_z -> Nullable<Int4>,
        subregion_id -> Nullable<Int4>,
        item_name -> Nullable<Varchar>,
        item_type -> Nullable<Varchar>,
        item_subtype -> Nullable<Varchar>,
        item_writing -> Nullable<Int4>,
        item_page_number -> Nullable<Int4>,
        item_page_written_content_id -> Nullable<Int4>,
        item_writing_written_content_id -> Nullable<Int4>,
        item_description -> Nullable<Varchar>,
        item_mat -> Nullable<Varchar>,
    }
}

table! {
    coordinates (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        x -> Int4,
        y -> Int4,
        region_id -> Nullable<Int4>,
        underground_region_id -> Nullable<Int4>,
        world_construction_id -> Nullable<Int4>,
        entity_id -> Nullable<Int4>,
    }
}

table! {
    creature_biomes_1 (cr_id, world_id) {
        cr_id -> Int4,
        world_id -> Int4,
        desert_badland -> Nullable<Bool>,
        desert_rock -> Nullable<Bool>,
        desert_sand -> Nullable<Bool>,
        forest_taiga -> Nullable<Bool>,
        forest_temperate_broadleaf -> Nullable<Bool>,
        forest_temperate_conifer -> Nullable<Bool>,
        forest_tropical_conifer -> Nullable<Bool>,
        forest_tropical_dry_broadleaf -> Nullable<Bool>,
        forest_tropical_moist_broadleaf -> Nullable<Bool>,
        glacier -> Nullable<Bool>,
        grassland_temperate -> Nullable<Bool>,
        grassland_tropical -> Nullable<Bool>,
        lake_temperate_brackishwater -> Nullable<Bool>,
        lake_temperate_freshwater -> Nullable<Bool>,
        lake_temperate_saltwater -> Nullable<Bool>,
        lake_tropical_brackishwater -> Nullable<Bool>,
        lake_tropical_freshwater -> Nullable<Bool>,
        lake_tropical_saltwater -> Nullable<Bool>,
        marsh_temperate_freshwater -> Nullable<Bool>,
        marsh_temperate_saltwater -> Nullable<Bool>,
        marsh_tropical_freshwater -> Nullable<Bool>,
        marsh_tropical_saltwater -> Nullable<Bool>,
        mountain -> Nullable<Bool>,
    }
}

table! {
    creature_biomes_2 (cr_id, world_id) {
        cr_id -> Int4,
        world_id -> Int4,
        ocean_arctic -> Nullable<Bool>,
        ocean_temperate -> Nullable<Bool>,
        ocean_tropical -> Nullable<Bool>,
        pool_temperate_brackishwater -> Nullable<Bool>,
        pool_temperate_freshwater -> Nullable<Bool>,
        pool_temperate_saltwater -> Nullable<Bool>,
        pool_tropical_brackishwater -> Nullable<Bool>,
        pool_tropical_freshwater -> Nullable<Bool>,
        pool_tropical_saltwater -> Nullable<Bool>,
        river_temperate_brackishwater -> Nullable<Bool>,
        river_temperate_freshwater -> Nullable<Bool>,
        river_temperate_saltwater -> Nullable<Bool>,
        river_tropical_brackishwater -> Nullable<Bool>,
        river_tropical_freshwater -> Nullable<Bool>,
        river_tropical_saltwater -> Nullable<Bool>,
        savanna_temperate -> Nullable<Bool>,
        savanna_tropical -> Nullable<Bool>,
        shrubland_temperate -> Nullable<Bool>,
        shrubland_tropical -> Nullable<Bool>,
        subterranean_chasm -> Nullable<Bool>,
        subterranean_lava -> Nullable<Bool>,
        subterranean_water -> Nullable<Bool>,
        swamp_mangrove -> Nullable<Bool>,
        swamp_temperate_freshwater -> Nullable<Bool>,
        swamp_temperate_saltwater -> Nullable<Bool>,
        swamp_tropical_freshwater -> Nullable<Bool>,
        swamp_tropical_saltwater -> Nullable<Bool>,
        tundra -> Nullable<Bool>,
    }
}

table! {
    creatures (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        creature_id -> Nullable<Varchar>,
        name_singular -> Nullable<Varchar>,
        name_plural -> Nullable<Varchar>,
    }
}

table! {
    creatures_a_g (cr_id, world_id) {
        cr_id -> Int4,
        world_id -> Int4,
        all_castes_alive -> Nullable<Bool>,
        artificial_hiveable -> Nullable<Bool>,
        does_not_exist -> Nullable<Bool>,
        equipment -> Nullable<Bool>,
        equipment_wagon -> Nullable<Bool>,
        evil -> Nullable<Bool>,
        fanciful -> Nullable<Bool>,
        generated -> Nullable<Bool>,
        good -> Nullable<Bool>,
    }
}

table! {
    creatures_h_h_1 (cr_id, world_id) {
        cr_id -> Int4,
        world_id -> Int4,
        has_any_benign -> Nullable<Bool>,
        has_any_can_swim -> Nullable<Bool>,
        has_any_cannot_breathe_air -> Nullable<Bool>,
        has_any_cannot_breathe_water -> Nullable<Bool>,
        has_any_carnivore -> Nullable<Bool>,
        has_any_common_domestic -> Nullable<Bool>,
        has_any_curious_beast -> Nullable<Bool>,
        has_any_demon -> Nullable<Bool>,
        has_any_feature_beast -> Nullable<Bool>,
        has_any_flier -> Nullable<Bool>,
        has_any_fly_race_gait -> Nullable<Bool>,
        has_any_grasp -> Nullable<Bool>,
        has_any_grazer -> Nullable<Bool>,
        has_any_has_blood -> Nullable<Bool>,
        has_any_immobile -> Nullable<Bool>,
        has_any_intelligent_learns -> Nullable<Bool>,
        has_any_intelligent_speaks -> Nullable<Bool>,
        has_any_large_predator -> Nullable<Bool>,
        has_any_local_pops_controllable -> Nullable<Bool>,
        has_any_local_pops_produce_heroes -> Nullable<Bool>,
    }
}

table! {
    creatures_h_h_2 (cr_id, world_id) {
        cr_id -> Int4,
        world_id -> Int4,
        has_any_megabeast -> Nullable<Bool>,
        has_any_mischievous -> Nullable<Bool>,
        has_any_natural_animal -> Nullable<Bool>,
        has_any_night_creature -> Nullable<Bool>,
        has_any_night_creature_bogeyman -> Nullable<Bool>,
        has_any_night_creature_experimenter -> Nullable<Bool>,
        has_any_night_creature_hunter -> Nullable<Bool>,
        has_any_night_creature_nightmare -> Nullable<Bool>,
        has_any_not_fireimmune -> Nullable<Bool>,
        has_any_not_flier -> Nullable<Bool>,
        has_any_not_living -> Nullable<Bool>,
        has_any_outsider_controllable -> Nullable<Bool>,
        has_any_power -> Nullable<Bool>,
        has_any_race_gait -> Nullable<Bool>,
        has_any_semimegabeast -> Nullable<Bool>,
        has_any_slow_learner -> Nullable<Bool>,
        has_any_supernatural -> Nullable<Bool>,
        has_any_titan -> Nullable<Bool>,
        has_any_unique_demon -> Nullable<Bool>,
        has_any_utterances -> Nullable<Bool>,
        has_any_vermin_hateable -> Nullable<Bool>,
        has_any_vermin_micro -> Nullable<Bool>,
        has_female -> Nullable<Bool>,
        has_male -> Nullable<Bool>,
    }
}

table! {
    creatures_l_z (cr_id, world_id) {
        cr_id -> Int4,
        world_id -> Int4,
        large_roaming -> Nullable<Bool>,
        loose_clusters -> Nullable<Bool>,
        mates_to_breed -> Nullable<Bool>,
        mundane -> Nullable<Bool>,
        occurs_as_entity_race -> Nullable<Bool>,
        savage -> Nullable<Bool>,
        small_race -> Nullable<Bool>,
        two_genders -> Nullable<Bool>,
        ubiquitous -> Nullable<Bool>,
        vermin_eater -> Nullable<Bool>,
        vermin_fish -> Nullable<Bool>,
        vermin_grounder -> Nullable<Bool>,
        vermin_rotter -> Nullable<Bool>,
        vermin_soil -> Nullable<Bool>,
        vermin_soil_colony -> Nullable<Bool>,
    }
}

table! {
    dance_forms (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        description -> Nullable<Varchar>,
    }
}

table! {
    entities (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        race -> Nullable<Varchar>,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
    }
}

table! {
    entity_child_en_ids (en_id, child_en_id, world_id) {
        en_id -> Int4,
        child_en_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    entity_hf_ids (en_id, hf_id, world_id) {
        en_id -> Int4,
        hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    entity_honors (en_id, local_id, world_id) {
        en_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        gives_precedence -> Nullable<Int4>,
        requires_any_melee_or_ranged_skill -> Nullable<Bool>,
        required_skill_ip_total -> Nullable<Int4>,
        required_battles -> Nullable<Int4>,
        required_years -> Nullable<Int4>,
        required_skill -> Nullable<Varchar>,
        required_kills -> Nullable<Int4>,
        exempt_ep_id -> Nullable<Int4>,
        exempt_former_ep_id -> Nullable<Int4>,
        granted_to_everybody -> Nullable<Bool>,
    }
}

table! {
    entity_links (en_id, local_id, world_id) {
        en_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        target -> Nullable<Int4>,
        strength -> Nullable<Int4>,
    }
}

table! {
    entity_occasions (en_id, local_id, world_id) {
        en_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        event -> Nullable<Int4>,
    }
}

table! {
    entity_occasion_schedule_features (en_id, en_occ_id, en_occ_sch_id, local_id, world_id) {
        en_id -> Int4,
        en_occ_id -> Int4,
        en_occ_sch_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        reference -> Nullable<Int4>,
    }
}

table! {
    entity_occasion_schedules (en_id, en_occ_id, local_id, world_id) {
        en_id -> Int4,
        en_occ_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        type_ -> Nullable<Varchar>,
        item_type -> Nullable<Varchar>,
        item_subtype -> Nullable<Varchar>,
        reference -> Nullable<Int4>,
        reference2 -> Nullable<Int4>,
    }
}

table! {
    entity_population_races (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        entity_population_id -> Int4,
        race -> Nullable<Varchar>,
        amount -> Nullable<Int4>,
    }
}

table! {
    entity_populations (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        civ_id -> Nullable<Int4>,
    }
}

table! {
    entity_position_assignments (en_id, local_id, world_id) {
        en_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        hf_id -> Nullable<Int4>,
        position_id -> Nullable<Int4>,
        squad_id -> Nullable<Int4>,
    }
}

table! {
    entity_positions (en_id, local_id, world_id) {
        en_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        name_male -> Nullable<Varchar>,
        name_female -> Nullable<Varchar>,
        spouse -> Nullable<Varchar>,
        spouse_male -> Nullable<Varchar>,
        spouse_female -> Nullable<Varchar>,
    }
}

table! {
    entity_professions (en_id, profession, world_id) {
        en_id -> Int4,
        profession -> Varchar,
        world_id -> Int4,
    }
}

table! {
    entity_weapons (en_id, weapon, world_id) {
        en_id -> Int4,
        weapon -> Varchar,
        world_id -> Int4,
    }
}

table! {
    entity_worship_ids (en_id, worship_id, world_id) {
        en_id -> Int4,
        worship_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_bodies_hf_ids (he_id, hf_id, world_id) {
        he_id -> Int4,
        hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hec_a_support_merc_hf_ids (hec_id, a_support_merc_hf_id, world_id) {
        hec_id -> Int4,
        a_support_merc_hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hec_attacking_hf_ids (hec_id, attacking_hf_id, world_id) {
        hec_id -> Int4,
        attacking_hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hec_defending_hf_ids (hec_id, defending_hf_id, world_id) {
        hec_id -> Int4,
        defending_hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hec_d_support_merc_hf_ids (hec_id, d_support_merc_hf_id, world_id) {
        hec_id -> Int4,
        d_support_merc_hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hec_he_ids (hec_id, he_id, world_id) {
        hec_id -> Int4,
        he_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hec_individual_mercs (hec_id, individual_merc, world_id) {
        hec_id -> Int4,
        individual_merc -> Bool,
        world_id -> Int4,
    }
}

table! {
    he_circumstances (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        death -> Nullable<Int4>,
        prayer -> Nullable<Int4>,
        dream_about -> Nullable<Int4>,
        defeated -> Nullable<Int4>,
        murdered -> Nullable<Int4>,
        hec_id -> Nullable<Int4>,
    }
}

table! {
    hec_noncom_hf_ids (hec_id, noncom_hf_id, world_id) {
        hec_id -> Int4,
        noncom_hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_competitor_hf_ids (he_id, hf_id, world_id) {
        he_id -> Int4,
        hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_conspirator_hf_ids (he_id, hf_id, world_id) {
        he_id -> Int4,
        hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hec_outcomes (hec_id, outcome, world_id) {
        hec_id -> Int4,
        outcome -> Varchar,
        world_id -> Int4,
    }
}

table! {
    hec_related_ids (hec_id, rel_hec_id, world_id) {
        hec_id -> Int4,
        rel_hec_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_expelled_creatures (he_id, creature_id, world_id) {
        he_id -> Int4,
        creature_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_expelled_hf_ids (he_id, hf_id, world_id) {
        he_id -> Int4,
        hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_expelled_numbers (he_id, number, world_id) {
        he_id -> Int4,
        number -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_expelled_pop_ids (he_id, pop_id, world_id) {
        he_id -> Int4,
        pop_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_groups_hf_ids (he_id, hf_id, world_id) {
        he_id -> Int4,
        hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_implicated_hf_ids (he_id, hf_id, world_id) {
        he_id -> Int4,
        hf_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_joining_en_ids (he_id, en_id, world_id) {
        he_id -> Int4,
        en_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    he_pets (he_id, pet, world_id) {
        he_id -> Int4,
        pet -> Varchar,
        world_id -> Int4,
    }
}

table! {
    he_reasons (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        glorify_hf_id -> Nullable<Int4>,
        artifact_is_heirloom_of_family_hf_id -> Nullable<Int4>,
        artifact_is_symbol_of_entity_position -> Nullable<Int4>,
    }
}

table! {
    he_relationships (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        event -> Nullable<Int4>,
        relationship -> Nullable<Varchar>,
        source_hf_id -> Nullable<Int4>,
        target_hf_id -> Nullable<Int4>,
        year -> Nullable<Int4>,
    }
}

table! {
    he_relationship_supplements (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        event -> Nullable<Int4>,
        occasion_type -> Nullable<Int4>,
        site_id -> Nullable<Int4>,
        unk_1 -> Nullable<Int4>,
    }
}

table! {
    hf_entity_links (hf_id, entity_id, link_type, world_id) {
        hf_id -> Int4,
        entity_id -> Int4,
        link_type -> Varchar,
        world_id -> Int4,
        link_strength -> Nullable<Int4>,
    }
}

table! {
    hf_entity_position_links (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        hf_id -> Int4,
        entity_id -> Int4,
        start_year -> Nullable<Int4>,
        position_profile_id -> Nullable<Int4>,
        end_year -> Nullable<Int4>,
    }
}

table! {
    hf_entity_reputations (hf_id, entity_id, world_id) {
        hf_id -> Int4,
        entity_id -> Int4,
        world_id -> Int4,
        first_ageless_year -> Nullable<Int4>,
        first_ageless_season_count -> Nullable<Int4>,
        unsolved_murders -> Nullable<Int4>,
    }
}

table! {
    hf_entity_squad_links (hf_id, entity_id, squad_id, world_id) {
        hf_id -> Int4,
        entity_id -> Int4,
        squad_id -> Int4,
        world_id -> Int4,
        squad_position -> Nullable<Int4>,
        start_year -> Nullable<Int4>,
    }
}

table! {
    hf_honor_entities (hf_id, entity_id, world_id) {
        hf_id -> Int4,
        entity_id -> Int4,
        world_id -> Int4,
        battles -> Nullable<Int4>,
        kills -> Nullable<Int4>,
    }
}

table! {
    hf_honor_ids (hf_id, entity_id, honor_id, world_id) {
        hf_id -> Int4,
        entity_id -> Int4,
        honor_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hf_interaction_knowledges (hf_id, interaction_knowledge, world_id) {
        hf_id -> Int4,
        interaction_knowledge -> Varchar,
        world_id -> Int4,
    }
}

table! {
    hf_intrigue_actors (hf_id, local_id, world_id) {
        hf_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        entity_id -> Nullable<Int4>,
        hf_id_other -> Nullable<Int4>,
        role -> Nullable<Varchar>,
        strategy -> Nullable<Varchar>,
        strategy_en_id -> Nullable<Int4>,
        strategy_epp_id -> Nullable<Int4>,
        handle_actor_id -> Nullable<Int4>,
        promised_actor_immortality -> Nullable<Bool>,
        promised_me_immortality -> Nullable<Bool>,
    }
}

table! {
    hf_intrigue_plots (hf_id, local_id, world_id) {
        hf_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        on_hold -> Nullable<Bool>,
        actor_id -> Nullable<Int4>,
        artifact_id -> Nullable<Int4>,
        delegated_plot_id -> Nullable<Int4>,
        delegated_plot_hf_id -> Nullable<Int4>,
        entity_id -> Nullable<Int4>,
        parent_plot_hf_id -> Nullable<Int4>,
        parent_plot_id -> Nullable<Int4>,
    }
}

table! {
    hf_journey_pets (hf_id, journey_pet, world_id) {
        hf_id -> Int4,
        journey_pet -> Varchar,
        world_id -> Int4,
    }
}

table! {
    hf_links (hf_id, hf_id_other, link_type, world_id) {
        hf_id -> Int4,
        hf_id_other -> Int4,
        link_type -> Varchar,
        world_id -> Int4,
        link_strength -> Nullable<Int4>,
    }
}

table! {
    hf_plot_actors (hf_id, local_id, actor_id, world_id) {
        hf_id -> Int4,
        local_id -> Int4,
        actor_id -> Int4,
        world_id -> Int4,
        plot_role -> Nullable<Varchar>,
        agreement_id -> Nullable<Int4>,
        agreement_has_messenger -> Nullable<Bool>,
    }
}

table! {
    hf_relationship_profile_hf_historical (hf_id, hf_id_other, world_id) {
        hf_id -> Int4,
        hf_id_other -> Int4,
        world_id -> Int4,
        love -> Nullable<Int4>,
        respect -> Nullable<Int4>,
        trust -> Nullable<Int4>,
        loyalty -> Nullable<Int4>,
        fear -> Nullable<Int4>,
    }
}

table! {
    hf_relationship_profile_hf_visual (hf_id, hf_id_other, world_id) {
        hf_id -> Int4,
        hf_id_other -> Int4,
        world_id -> Int4,
        meet_count -> Nullable<Int4>,
        last_meet_year -> Nullable<Int4>,
        last_meet_seconds72 -> Nullable<Int4>,
        love -> Nullable<Int4>,
        respect -> Nullable<Int4>,
        trust -> Nullable<Int4>,
        loyalty -> Nullable<Int4>,
        fear -> Nullable<Int4>,
        known_identity_id -> Nullable<Int4>,
        rep_friendly -> Nullable<Int4>,
        rep_information_source -> Nullable<Int4>,
    }
}

table! {
    hf_site_links (hf_id, site_id, link_type, world_id) {
        hf_id -> Int4,
        site_id -> Int4,
        link_type -> Varchar,
        world_id -> Int4,
        entity_id -> Nullable<Int4>,
        occupation_id -> Nullable<Int4>,
        sub_id -> Nullable<Int4>,
    }
}

table! {
    hf_site_properties (hf_id, site_id, property_id, world_id) {
        hf_id -> Int4,
        site_id -> Int4,
        property_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    hf_skills (hf_id, skill, world_id) {
        hf_id -> Int4,
        skill -> Varchar,
        world_id -> Int4,
        total_ip -> Nullable<Int4>,
    }
}

table! {
    hf_spheres (hf_id, sphere, world_id) {
        hf_id -> Int4,
        sphere -> Varchar,
        world_id -> Int4,
    }
}

table! {
    hf_vague_relationships (hf_id, hf_id_other, world_id) {
        hf_id -> Int4,
        hf_id_other -> Int4,
        world_id -> Int4,
        war_buddy -> Nullable<Bool>,
        artistic_buddy -> Nullable<Bool>,
        atheletic_rival -> Nullable<Bool>,
        athlete_buddy -> Nullable<Bool>,
        business_rival -> Nullable<Bool>,
        childhood_friend -> Nullable<Bool>,
        grudge -> Nullable<Bool>,
        jealous_obsession -> Nullable<Bool>,
        jealous_relationship_grudge -> Nullable<Bool>,
        persecution_grudge -> Nullable<Bool>,
        religious_persecution_grudge -> Nullable<Bool>,
        scholar_buddy -> Nullable<Bool>,
        supernatural_grudge -> Nullable<Bool>,
    }
}

table! {
    historical_eras (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        start_year -> Nullable<Int4>,
    }
}

table! {
    historical_event_collections (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        start_year -> Nullable<Int4>,
        start_seconds72 -> Nullable<Int4>,
        end_year -> Nullable<Int4>,
        end_seconds72 -> Nullable<Int4>,
    }
}

table! {
    historical_event_collections_a_c (hec_id, world_id) {
        hec_id -> Int4,
        world_id -> Int4,
        a_support_merc_en_id -> Nullable<Int4>,
        adjective -> Nullable<Varchar>,
        aggressor_ent_id -> Nullable<Int4>,
        attacking_en_id -> Nullable<Int4>,
        attacking_merc_enid -> Nullable<Int4>,
        civ_id -> Nullable<Int4>,
        company_merc -> Nullable<Bool>,
        coords -> Nullable<Varchar>,
    }
}

table! {
    historical_event_collections_d_z (hec_id, world_id) {
        hec_id -> Int4,
        world_id -> Int4,
        d_support_merc_en_id -> Nullable<Int4>,
        defender_ent_id -> Nullable<Int4>,
        defending_en_id -> Nullable<Int4>,
        defending_merc_en_id -> Nullable<Int4>,
        feature_layer_id -> Nullable<Int4>,
        name -> Nullable<Varchar>,
        occasion_id -> Nullable<Int4>,
        ordinal -> Nullable<Int4>,
        parent_hec_id -> Nullable<Int4>,
        site_id -> Nullable<Int4>,
        subregion_id -> Nullable<Int4>,
        target_entity_id -> Nullable<Int4>,
        war_hec_id -> Nullable<Int4>,
    }
}

table! {
    historical_events (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        year -> Nullable<Int4>,
        seconds72 -> Nullable<Int4>,
    }
}

table! {
    historical_events_a_a (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        a_support_merc_en_id -> Nullable<Int4>,
        account_shift -> Nullable<Int4>,
        acquirer_en_id -> Nullable<Int4>,
        acquirer_hf_id -> Nullable<Int4>,
        action -> Nullable<Varchar>,
        actor_hf_id -> Nullable<Int4>,
        agreement_id -> Nullable<Int4>,
        allotment -> Nullable<Int4>,
        allotment_index -> Nullable<Int4>,
        ally_defense_bonus -> Nullable<Int4>,
        appointer_hf_id -> Nullable<Int4>,
        arresting_en_id -> Nullable<Int4>,
        artifact_id -> Nullable<Int4>,
        attacker_civ_id -> Nullable<Int4>,
        attacker_general_hf_id -> Nullable<Int4>,
        attacker_hf_id -> Nullable<Int4>,
        attacker_merc_en_id -> Nullable<Int4>,
        abuse_type -> Nullable<Varchar>,
        anon_3 -> Nullable<Int4>,
        anon_4 -> Nullable<Int4>,
    }
}

table! {
    historical_events_b_b (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        body_state -> Nullable<Varchar>,
        builder_hf_id -> Nullable<Int4>,
        building_profile_id -> Nullable<Int4>,
        body_part -> Nullable<Int4>,
        building_type -> Nullable<Varchar>,
        building_subtype -> Nullable<Varchar>,
    }
}

table! {
    historical_events_c_c (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        cause -> Nullable<Varchar>,
        changee_hf_id -> Nullable<Int4>,
        changer_hf_id -> Nullable<Int4>,
        circumstance -> Nullable<Varchar>,
        circumstance_id -> Nullable<Int4>,
        civ_entity_id -> Nullable<Int4>,
        civ_id -> Nullable<Int4>,
        claim -> Nullable<Varchar>,
        coconspirator_bonus -> Nullable<Int4>,
        coconspirator_hf_id -> Nullable<Int4>,
        confessed_after_apb_arrest_en_id -> Nullable<Int4>,
        contact_hf_id -> Nullable<Int4>,
        convict_is_contact -> Nullable<Bool>,
        convicted_hf_id -> Nullable<Int4>,
        convicter_en_id -> Nullable<Int4>,
        coord -> Nullable<Varchar>,
        corrupt_convicter_hf_id -> Nullable<Int4>,
        corruptor_hf_id -> Nullable<Int4>,
        corruptor_identity -> Nullable<Int4>,
        corruptor_seen_as -> Nullable<Varchar>,
        creator_hf_id -> Nullable<Int4>,
        crime -> Nullable<Varchar>,
        caste -> Nullable<Varchar>,
        creator_unit_id -> Nullable<Int4>,
    }
}

table! {
    historical_events_d_d (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        d_support_merc_en_id -> Nullable<Int4>,
        death_penalty -> Nullable<Bool>,
        defender_civ_id -> Nullable<Int4>,
        defender_general_hf_id -> Nullable<Int4>,
        defender_merc_en_id -> Nullable<Int4>,
        delegated -> Nullable<Bool>,
        dest_entity_id -> Nullable<Int4>,
        dest_site_id -> Nullable<Int4>,
        dest_structure_id -> Nullable<Int4>,
        destroyed_structure_id -> Nullable<Int4>,
        destroyer_en_id -> Nullable<Int4>,
        detected -> Nullable<Bool>,
        did_not_reveal_all_in_interrogation -> Nullable<Bool>,
        disturbance -> Nullable<Bool>,
        dispute -> Nullable<Varchar>,
        doer_hf_id -> Nullable<Int4>,
        death_cause -> Nullable<Varchar>,
        destination -> Nullable<Int4>,
        doer -> Nullable<Int4>,
        dye_mat -> Nullable<Varchar>,
        dye_mat_index -> Nullable<Int4>,
        dye_mat_type -> Nullable<Int4>,
    }
}

table! {
    historical_events_e_g (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        enslaved_hf_id -> Nullable<Int4>,
        entity_id -> Nullable<Int4>,
        entity_id_1 -> Nullable<Int4>,
        entity_id_2 -> Nullable<Int4>,
        exiled -> Nullable<Bool>,
        eater_hf_id -> Nullable<Int4>,
        failed_judgment_test -> Nullable<Bool>,
        feature_layer_id -> Nullable<Int4>,
        first -> Nullable<Bool>,
        fooled_hf_id -> Nullable<Int4>,
        form_id -> Nullable<Int4>,
        framer_hf_id -> Nullable<Int4>,
        from_original -> Nullable<Bool>,
        gambler_hf_id -> Nullable<Int4>,
        giver_entity_id -> Nullable<Int4>,
        giver_hf_id -> Nullable<Int4>,
        group_1_hf_id -> Nullable<Int4>,
        group_2_hf_id -> Nullable<Int4>,
        group_hf_id -> Nullable<Int4>,
    }
}

table! {
    historical_events_h_h (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        held_firm_in_interrogation -> Nullable<Bool>,
        hf_rep_1_of_2 -> Nullable<Varchar>,
        hf_rep_2_of_1 -> Nullable<Varchar>,
        hf_id -> Nullable<Int4>,
        hf_id1 -> Nullable<Int4>,
        hf_id2 -> Nullable<Int4>,
        hf_id_target -> Nullable<Int4>,
        honor_id -> Nullable<Int4>,
    }
}

table! {
    historical_events_i_i (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        identity_id -> Nullable<Int4>,
        identity_id1 -> Nullable<Int4>,
        identity_id2 -> Nullable<Int4>,
        inherited -> Nullable<Bool>,
        initiating_en_id -> Nullable<Int4>,
        instigator_hf_id -> Nullable<Int4>,
        interaction -> Nullable<Int4>,
        interrogator_hf_id -> Nullable<Int4>,
        identity_caste -> Nullable<Varchar>,
        identity_hf_id -> Nullable<Int4>,
        identity_name -> Nullable<Varchar>,
        identity_nemesis_id -> Nullable<Int4>,
        identity_race -> Nullable<Varchar>,
        imp_mat -> Nullable<Varchar>,
        imp_mat_index -> Nullable<Int4>,
        imp_mat_type -> Nullable<Int4>,
        improvement_type -> Nullable<Int4>,
        injury_type -> Nullable<Varchar>,
        interaction_action -> Nullable<Varchar>,
        interaction_string -> Nullable<Varchar>,
        item -> Nullable<Int4>,
        item_mat -> Nullable<Varchar>,
        item_subtype -> Nullable<Varchar>,
        item_type -> Nullable<Varchar>,
    }
}

table! {
    historical_events_j_m (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        join_entity_id -> Nullable<Int4>,
        joined_entity_id -> Nullable<Int4>,
        joiner_entity_id -> Nullable<Int4>,
        knowledge -> Nullable<Varchar>,
        last_owner_hf_id -> Nullable<Int4>,
        law_add -> Nullable<Varchar>,
        law_remove -> Nullable<Varchar>,
        leader_hf_id -> Nullable<Int4>,
        link -> Nullable<Varchar>,
        lure_hf_id -> Nullable<Int4>,
        link_type -> Nullable<Varchar>,
        master_wc_id -> Nullable<Int4>,
        method -> Nullable<Varchar>,
        modification -> Nullable<Varchar>,
        modifier_hf_id -> Nullable<Int4>,
        mood -> Nullable<Varchar>,
        moved_to_site_id -> Nullable<Int4>,
        mat -> Nullable<Varchar>,
        mat_type -> Nullable<Int4>,
        mat_index -> Nullable<Int4>,
    }
}

table! {
    historical_events_n_o (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        name_only -> Nullable<Bool>,
        new_ab_id -> Nullable<Int4>,
        new_account -> Nullable<Int4>,
        new_caste -> Nullable<Int4>,
        new_equipment_level -> Nullable<Int4>,
        new_leader_hf_id -> Nullable<Int4>,
        new_race_id -> Nullable<Varchar>,
        new_site_civ_id -> Nullable<Int4>,
        new_job -> Nullable<Varchar>,
        new_structure_id -> Nullable<Int4>,
        occasion_id -> Nullable<Int4>,
        old_ab_id -> Nullable<Int4>,
        old_account -> Nullable<Int4>,
        old_caste -> Nullable<Int4>,
        old_race_id -> Nullable<Varchar>,
        overthrown_hf_id -> Nullable<Int4>,
        old_job -> Nullable<Varchar>,
        old_structure_id -> Nullable<Int4>,
    }
}

table! {
    historical_events_p_p (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        partial_incorporation -> Nullable<Bool>,
        payer_entity_id -> Nullable<Int4>,
        payer_hf_id -> Nullable<Int4>,
        persecutor_en_id -> Nullable<Int4>,
        persecutor_hf_id -> Nullable<Int4>,
        plotter_hf_id -> Nullable<Int4>,
        pop_fl_id -> Nullable<Int4>,
        pop_number_moved -> Nullable<Int4>,
        pop_race -> Nullable<Int4>,
        pop_sr_id -> Nullable<Int4>,
        pos_taker_hf_id -> Nullable<Int4>,
        position_id -> Nullable<Int4>,
        position_profile_id -> Nullable<Int4>,
        prison_months -> Nullable<Int4>,
        production_zone_id -> Nullable<Int4>,
        promise_to_hf_id -> Nullable<Int4>,
        property_confiscated_from_hf_id -> Nullable<Int4>,
        purchased_unowned -> Nullable<Bool>,
        part_lost -> Nullable<Bool>,
        pile_type -> Nullable<Varchar>,
        position -> Nullable<Varchar>,
        props_item_mat -> Nullable<Varchar>,
        props_item_mat_index -> Nullable<Int4>,
        props_item_mat_type -> Nullable<Int4>,
        props_item_subtype -> Nullable<Varchar>,
        props_item_type -> Nullable<Varchar>,
        props_pile_type -> Nullable<Int4>,
    }
}

table! {
    historical_events_q_r (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        quality -> Nullable<Int4>,
        ransomed_hf_id -> Nullable<Int4>,
        ransomer_hf_id -> Nullable<Int4>,
        reason -> Nullable<Int4>,
        reason_id -> Nullable<Int4>,
        rebuilt_ruined -> Nullable<Bool>,
        receiver_entity_id -> Nullable<Int4>,
        receiver_hf_id -> Nullable<Int4>,
        relationship -> Nullable<Varchar>,
        relevant_entity_id -> Nullable<Int4>,
        relevant_id_for_method -> Nullable<Int4>,
        relevant_position_profile_id -> Nullable<Int4>,
        religion_id -> Nullable<Int4>,
        resident_civ_id -> Nullable<Int4>,
        #[sql_name = "return"]
        return_ -> Nullable<Bool>,
        race_id -> Nullable<Varchar>,
        rebuild -> Nullable<Bool>,
        region_id -> Nullable<Int4>,
    }
}

table! {
    historical_events_s_s1 (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        saboteur_hf_id -> Nullable<Int4>,
        sanctify_hf_id -> Nullable<Int4>,
        schedule_id -> Nullable<Int4>,
        season -> Nullable<Varchar>,
        secret_goal -> Nullable<Varchar>,
        seeker_hf_id -> Nullable<Int4>,
        seller_hf_id -> Nullable<Int4>,
        shrine_amount_destroyed -> Nullable<Int4>,
        site_civ_id -> Nullable<Int4>,
        site_entity_id -> Nullable<Int4>,
        site_hf_id -> Nullable<Int4>,
        site_id -> Nullable<Int4>,
        site_id_1 -> Nullable<Int4>,
        site_id_2 -> Nullable<Int4>,
        site_property_id -> Nullable<Int4>,
        situation -> Nullable<Varchar>,
        slayer_caste -> Nullable<Int4>,
        slayer_hf_id -> Nullable<Int4>,
        slayer_item_id -> Nullable<Int4>,
        slayer_race -> Nullable<Varchar>,
        slayer_shooter_item_id -> Nullable<Int4>,
        snatcher_hf_id -> Nullable<Int4>,
        source_entity_id -> Nullable<Int4>,
        source_site_id -> Nullable<Int4>,
        source_structure_id -> Nullable<Int4>,
        stash_site_id -> Nullable<Int4>,
        speaker_hf_id -> Nullable<Int4>,
    }
}

table! {
    historical_events_s_s2 (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        state -> Nullable<Varchar>,
        structure_id -> Nullable<Int4>,
        student_hf_id -> Nullable<Int4>,
        subregion_id -> Nullable<Int4>,
        subtype -> Nullable<Varchar>,
        successful -> Nullable<Bool>,
        surveiled_coconspirator -> Nullable<Bool>,
        surveiled_contact -> Nullable<Bool>,
        surveiled_convicted -> Nullable<Bool>,
        surveiled_target -> Nullable<Bool>,
        secret_text -> Nullable<Varchar>,
        shooter_artifact_id -> Nullable<Int4>,
        shooter_item -> Nullable<Varchar>,
        shooter_item_subtype -> Nullable<Varchar>,
        shooter_item_type -> Nullable<Varchar>,
        shooter_mat -> Nullable<Varchar>,
        source -> Nullable<Int4>,
    }
}

table! {
    historical_events_t_t (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        target_en_id -> Nullable<Int4>,
        target_hf_id -> Nullable<Int4>,
        target_identity -> Nullable<Int4>,
        target_seen_as -> Nullable<Varchar>,
        teacher_hf_id -> Nullable<Int4>,
        theft_method -> Nullable<Varchar>,
        top_facet -> Nullable<Varchar>,
        top_facet_modifier -> Nullable<Int4>,
        top_facet_rating -> Nullable<Int4>,
        top_relationship_factor -> Nullable<Varchar>,
        top_relationship_modifier -> Nullable<Int4>,
        top_relationship_rating -> Nullable<Int4>,
        top_value -> Nullable<Varchar>,
        top_value_modifier -> Nullable<Int4>,
        top_value_rating -> Nullable<Int4>,
        topic -> Nullable<Varchar>,
        trader_entity_id -> Nullable<Int4>,
        trader_hf_id -> Nullable<Int4>,
        trickster_hf_id -> Nullable<Int4>,
        tree -> Nullable<Int4>,
        trickster -> Nullable<Int4>,
    }
}

table! {
    historical_events_u_w (he_id, world_id) {
        he_id -> Int4,
        world_id -> Int4,
        unit_id -> Nullable<Int4>,
        unit_type -> Nullable<Varchar>,
        victim -> Nullable<Int4>,
        victim_entity -> Nullable<Int4>,
        victim_hf_id -> Nullable<Int4>,
        wanted_and_recognized -> Nullable<Bool>,
        was_torture -> Nullable<Bool>,
        wc_id -> Nullable<Int4>,
        winner_hf_id -> Nullable<Int4>,
        woundee_hf_id -> Nullable<Int4>,
        wounder_hf_id -> Nullable<Int4>,
        wrongful_conviction -> Nullable<Bool>,
        woundee_caste -> Nullable<Int4>,
        woundee_race -> Nullable<Int4>,
    }
}

table! {
    historical_figures (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        race -> Nullable<Varchar>,
        race_id -> Nullable<Varchar>,
        caste -> Nullable<Varchar>,
        appeared -> Nullable<Int4>,
        birth_year -> Nullable<Int4>,
        birth_seconds72 -> Nullable<Int4>,
        death_year -> Nullable<Int4>,
        death_seconds72 -> Nullable<Int4>,
        associated_type -> Nullable<Varchar>,
        deity -> Nullable<Varchar>,
        goal -> Nullable<Varchar>,
        ent_pop_id -> Nullable<Int4>,
        active_interaction -> Nullable<Varchar>,
        force -> Nullable<Bool>,
        current_identity_id -> Nullable<Int4>,
        holds_artifact -> Nullable<Int4>,
        used_identity_id -> Nullable<Int4>,
        animated -> Nullable<Bool>,
        animated_string -> Nullable<Varchar>,
    }
}

table! {
    identities (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        hf_id -> Nullable<Int4>,
        race -> Nullable<Varchar>,
        caste -> Nullable<Varchar>,
        birth_year -> Nullable<Int4>,
        birth_second -> Nullable<Int4>,
        profession -> Nullable<Varchar>,
        entity_id -> Nullable<Int4>,
        nemesis_id -> Nullable<Int4>,
    }
}

table! {
    landmasses (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        coord_1_id -> Nullable<Int4>,
        coord_2_id -> Nullable<Int4>,
    }
}

table! {
    map_images (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Varchar,
        data -> Binary,
        format -> Varchar,
    }
}

table! {
    mountain_peaks (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        coord_id -> Nullable<Int4>,
        height -> Nullable<Int4>,
        is_volcano -> Nullable<Bool>,
    }
}

table! {
    musical_forms (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        description -> Nullable<Varchar>,
    }
}

table! {
    paths (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        x -> Int4,
        y -> Int4,
        flow -> Int4,
        exit_tile -> Int4,
        elevation -> Int4,
        river_id -> Nullable<Int4>,
    }
}

table! {
    poetic_forms (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        description -> Nullable<Varchar>,
    }
}

table! {
    rectangles (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        x1 -> Int4,
        y1 -> Int4,
        x2 -> Int4,
        y2 -> Int4,
    }
}

table! {
    regions (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        evilness -> Nullable<Varchar>,
    }
}

table! {
    regions_forces (region_id, force_id, world_id) {
        region_id -> Int4,
        force_id -> Int4,
        world_id -> Int4,
    }
}

table! {
    rivers (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        end_pos_id -> Nullable<Int4>,
    }
}

table! {
    site_map_images (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        data -> Binary,
        format -> Varchar,
    }
}

table! {
    sites (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        name -> Nullable<Varchar>,
        coordinate_id -> Nullable<Int4>,
        rectangle_id -> Nullable<Int4>,
        civ_id -> Nullable<Int4>,
        cur_owner_id -> Nullable<Int4>,
    }
}

table! {
    sites_properties (site_id, local_id, world_id) {
        site_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        structure_id -> Nullable<Int4>,
        owner_hfid -> Nullable<Int4>,
    }
}

table! {
    sites_structures (site_id, local_id, world_id) {
        site_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        subtype -> Nullable<Varchar>,
        name -> Nullable<Varchar>,
        name2 -> Nullable<Varchar>,
        entity_id -> Nullable<Int4>,
        worship_hfid -> Nullable<Int4>,
        deity_type -> Nullable<Int4>,
        deity_id -> Nullable<Int4>,
        religion_id -> Nullable<Int4>,
        dungeon_type -> Nullable<Int4>,
    }
}

table! {
    structures_copied_artifacts (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        site_id -> Int4,
        structure_local_id -> Int4,
        copied_artifact_id -> Int4,
    }
}

table! {
    structures_inhabitants (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        site_id -> Int4,
        structure_local_id -> Int4,
        inhabitant_id -> Int4,
    }
}

table! {
    underground_regions (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        depth -> Nullable<Int4>,
    }
}

table! {
    wc_reference (written_content_parent_id, local_id, world_id) {
        written_content_parent_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        type_id -> Nullable<Int4>,
        written_content_id -> Nullable<Int4>,
        he_id -> Nullable<Int4>,
        site_id -> Nullable<Int4>,
        poetic_form_id -> Nullable<Int4>,
        musical_form_id -> Nullable<Int4>,
        dance_form_id -> Nullable<Int4>,
        hf_id -> Nullable<Int4>,
        entity_id -> Nullable<Int4>,
        artifact_id -> Nullable<Int4>,
        subregion_id -> Nullable<Int4>,
    }
}

table! {
    wc_style (written_content_id, local_id, world_id) {
        written_content_id -> Int4,
        local_id -> Int4,
        world_id -> Int4,
        label -> Nullable<Varchar>,
        weight -> Nullable<Int4>,
    }
}

table! {
    world_constructions (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        name -> Nullable<Varchar>,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
    }
}

table! {
    worlds (id) {
        id -> Int4,
        save_name -> Nullable<Varchar>,
        name -> Nullable<Varchar>,
        alternative_name -> Nullable<Varchar>,
        region_number -> Nullable<Int4>,
        year -> Nullable<Int4>,
        month -> Nullable<Int4>,
        day -> Nullable<Int4>,
    }
}

table! {
    written_contents (id, world_id) {
        id -> Int4,
        world_id -> Int4,
        title -> Nullable<Varchar>,
        author_hf_id -> Nullable<Int4>,
        author_roll -> Nullable<Int4>,
        form -> Nullable<Varchar>,
        form_id -> Nullable<Int4>,
        page_start -> Nullable<Int4>,
        page_end -> Nullable<Int4>,
        #[sql_name = "type"]
        type_ -> Nullable<Varchar>,
        author -> Nullable<Int4>,
    }
}

allow_tables_to_appear_in_same_query!(
    artifacts,
    coordinates,
    creature_biomes_1,
    creature_biomes_2,
    creatures,
    creatures_a_g,
    creatures_h_h_1,
    creatures_h_h_2,
    creatures_l_z,
    dance_forms,
    entities,
    entity_child_en_ids,
    entity_hf_ids,
    entity_honors,
    entity_links,
    entity_occasions,
    entity_occasion_schedule_features,
    entity_occasion_schedules,
    entity_population_races,
    entity_populations,
    entity_position_assignments,
    entity_positions,
    entity_professions,
    entity_weapons,
    entity_worship_ids,
    he_bodies_hf_ids,
    hec_a_support_merc_hf_ids,
    hec_attacking_hf_ids,
    hec_defending_hf_ids,
    hec_d_support_merc_hf_ids,
    hec_he_ids,
    hec_individual_mercs,
    he_circumstances,
    hec_noncom_hf_ids,
    he_competitor_hf_ids,
    he_conspirator_hf_ids,
    hec_outcomes,
    hec_related_ids,
    he_expelled_creatures,
    he_expelled_hf_ids,
    he_expelled_numbers,
    he_expelled_pop_ids,
    he_groups_hf_ids,
    he_implicated_hf_ids,
    he_joining_en_ids,
    he_pets,
    he_reasons,
    he_relationships,
    he_relationship_supplements,
    hf_entity_links,
    hf_entity_position_links,
    hf_entity_reputations,
    hf_entity_squad_links,
    hf_honor_entities,
    hf_honor_ids,
    hf_interaction_knowledges,
    hf_intrigue_actors,
    hf_intrigue_plots,
    hf_journey_pets,
    hf_links,
    hf_plot_actors,
    hf_relationship_profile_hf_historical,
    hf_relationship_profile_hf_visual,
    hf_site_links,
    hf_site_properties,
    hf_skills,
    hf_spheres,
    hf_vague_relationships,
    historical_eras,
    historical_event_collections,
    historical_event_collections_a_c,
    historical_event_collections_d_z,
    historical_events,
    historical_events_a_a,
    historical_events_b_b,
    historical_events_c_c,
    historical_events_d_d,
    historical_events_e_g,
    historical_events_h_h,
    historical_events_i_i,
    historical_events_j_m,
    historical_events_n_o,
    historical_events_p_p,
    historical_events_q_r,
    historical_events_s_s1,
    historical_events_s_s2,
    historical_events_t_t,
    historical_events_u_w,
    historical_figures,
    identities,
    landmasses,
    map_images,
    mountain_peaks,
    musical_forms,
    paths,
    poetic_forms,
    rectangles,
    regions,
    regions_forces,
    rivers,
    site_map_images,
    sites,
    sites_properties,
    sites_structures,
    structures_copied_artifacts,
    structures_inhabitants,
    underground_regions,
    wc_reference,
    wc_style,
    world_constructions,
    worlds,
    written_contents,
);
