use df_st_core::{Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct Artifact {
    pub id: i32,
    pub item_type: Option<String>,
    pub item_subtype: Option<String>,
    pub writing: Option<i32>,
    pub page_count: Option<i32>,
    pub item_description: Option<String>,
    pub mat: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Artifacts {
    pub artifact: Option<Vec<Artifact>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Artifact, Artifact> for df_st_core::Artifact {
    fn add_missing_data(&mut self, source: &Artifact) {
        self.id.add_missing_data(&source.id);
        self.item_type.add_missing_data(&source.item_type);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.item_writing.add_missing_data(&source.writing);
        self.item_page_number.add_missing_data(&source.page_count);
        self.item_description
            .add_missing_data(&source.item_description);
        self.item_mat.add_missing_data(&source.mat);
    }
}

impl PartialEq<df_st_core::Artifact> for Artifact {
    fn eq(&self, other: &df_st_core::Artifact) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::Artifact>, Artifacts> for Vec<df_st_core::Artifact> {
    fn add_missing_data(&mut self, source: &Artifacts) {
        self.add_missing_data(&source.artifact);
    }
}

impl Filler<IndexMap<u64, df_st_core::Artifact>, Artifacts>
    for IndexMap<u64, df_st_core::Artifact>
{
    fn add_missing_data(&mut self, source: &Artifacts) {
        self.add_missing_data(&source.artifact);
    }
}
