use crate::deserializers::coordinates_deserializer;
use df_st_core::{Coordinate, Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, Default, HashAndPartialEqById)]
pub struct Entity {
    pub id: i32,
    pub race: Option<String>,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    // for Religion and MilitaryUnit
    pub worship_id: Option<Vec<i32>>,
    // for MilitaryUnit: favorite weapons
    pub weapon: Option<Vec<String>>,
    // for Guild
    pub profession: Option<Vec<String>>,
    pub entity_link: Option<Vec<EntityLink>>,
    pub entity_position: Option<Vec<EntityPosition>>,
    pub entity_position_assignment: Option<Vec<EntityPositionAssignment>>,
    pub histfig_id: Option<Vec<i32>>,
    pub child: Option<Vec<i32>>,
    #[serde(deserialize_with = "coordinates_deserializer")]
    #[serde(default)]
    pub claims: Option<Vec<Coordinate>>,
    pub occasion: Option<Vec<Occasion>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Entities {
    pub entity: Option<Vec<Entity>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct EntityLink {
    // ID not given in xml, but later generated.
    // Might be added later to xml
    pub id: Option<i32>,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub target: Option<i32>,
    pub strength: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct EntityPosition {
    pub id: i32,
    pub name: Option<String>,
    pub name_male: Option<String>,
    pub name_female: Option<String>,
    pub spouse: Option<String>,
    pub spouse_male: Option<String>,
    pub spouse_female: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct EntityPositionAssignment {
    pub id: i32,
    pub histfig: Option<i32>,
    pub position_id: Option<i32>,
    pub squad_id: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Occasion {
    pub id: i32,
    pub name: Option<String>,
    pub event: Option<i32>,
    pub schedule: Option<Vec<Schedule>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Schedule {
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    // for THROWING_COMPETITION
    pub item_type: Option<String>,
    pub item_subtype: Option<String>,

    pub reference: Option<i32>,
    pub reference2: Option<i32>,

    pub feature: Option<Vec<Feature>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Feature {
    // ID not given in xml, but later generated.
    // Might be added later to xml
    pub id: Option<i32>,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub reference: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Entity, Entity> for df_st_core::Entity {
    fn add_missing_data(&mut self, source: &Entity) {
        self.id.add_missing_data(&source.id);
        self.race.add_missing_data(&source.race);
        self.type_.add_missing_data(&source.type_);
        self.worship_id.add_missing_data(&source.worship_id);
        self.weapon.add_missing_data(&source.weapon);
        self.profession.add_missing_data(&source.profession);
        self.entity_link.add_missing_data(&source.entity_link);
        self.entity_position
            .add_missing_data(&source.entity_position);
        self.entity_position_assignment
            .add_missing_data(&source.entity_position_assignment);
        self.hf_ids.add_missing_data(&source.histfig_id);
        self.child_en_ids.add_missing_data(&source.child);
        self.claims.add_missing_data(&source.claims);
        self.occasion.add_missing_data(&source.occasion);
    }
}

impl PartialEq<df_st_core::Entity> for Entity {
    fn eq(&self, other: &df_st_core::Entity) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::Entity>, Entities> for Vec<df_st_core::Entity> {
    fn add_missing_data(&mut self, source: &Entities) {
        self.add_missing_data(&source.entity);
    }
}

impl Filler<IndexMap<u64, df_st_core::Entity>, Entities> for IndexMap<u64, df_st_core::Entity> {
    fn add_missing_data(&mut self, source: &Entities) {
        self.add_missing_data(&source.entity);
    }
}

impl Filler<df_st_core::EntityLink, EntityLink> for df_st_core::EntityLink {
    fn add_missing_data(&mut self, source: &EntityLink) {
        self.type_.add_missing_data(&source.type_);
        self.target.add_missing_data(&source.target);
        self.strength.add_missing_data(&source.strength);
    }

    fn add_missing_data_indexed(&mut self, source: &EntityLink, index: u64) {
        if source.id.is_none() {
            // If no ID set, use index
            self.local_id.add_missing_data(&(index as i32));
        }
        self.add_missing_data(source);
    }
}

impl PartialEq<df_st_core::EntityLink> for EntityLink {
    fn eq(&self, other: &df_st_core::EntityLink) -> bool {
        self.type_ == other.type_ && self.target == other.target
    }
}

impl Filler<df_st_core::EntityPosition, EntityPosition> for df_st_core::EntityPosition {
    fn add_missing_data(&mut self, source: &EntityPosition) {
        self.local_id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.name_male.add_missing_data(&source.name_male);
        self.name_female.add_missing_data(&source.name_female);
        self.spouse.add_missing_data(&source.spouse);
        self.spouse_male.add_missing_data(&source.spouse_male);
        self.spouse_female.add_missing_data(&source.spouse_female);
    }
}

impl PartialEq<df_st_core::EntityPosition> for EntityPosition {
    fn eq(&self, other: &df_st_core::EntityPosition) -> bool {
        self.id == other.local_id
    }
}

impl Filler<df_st_core::EntityPositionAssignment, EntityPositionAssignment>
    for df_st_core::EntityPositionAssignment
{
    fn add_missing_data(&mut self, source: &EntityPositionAssignment) {
        self.local_id.add_missing_data(&source.id);
        self.hf_id.add_missing_data(&source.histfig);
        self.position_id.add_missing_data(&source.position_id);
        self.squad_id.add_missing_data(&source.squad_id);
    }
}

impl PartialEq<df_st_core::EntityPositionAssignment> for EntityPositionAssignment {
    fn eq(&self, other: &df_st_core::EntityPositionAssignment) -> bool {
        self.id == other.local_id
    }
}

impl Filler<df_st_core::EntityOccasion, Occasion> for df_st_core::EntityOccasion {
    fn add_missing_data(&mut self, source: &Occasion) {
        self.local_id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.event.add_missing_data(&source.event);
        self.schedule.add_missing_data(&source.schedule);
    }
}

impl PartialEq<df_st_core::EntityOccasion> for Occasion {
    fn eq(&self, other: &df_st_core::EntityOccasion) -> bool {
        self.id == other.local_id
    }
}

impl Filler<df_st_core::EntityOccasionSchedule, Schedule> for df_st_core::EntityOccasionSchedule {
    fn add_missing_data(&mut self, source: &Schedule) {
        self.local_id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.item_type.add_missing_data(&source.item_type);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.reference.add_missing_data(&source.reference);
        self.reference2.add_missing_data(&source.reference2);
        self.feature.add_missing_data(&source.feature);
    }
}

impl PartialEq<df_st_core::EntityOccasionSchedule> for Schedule {
    fn eq(&self, other: &df_st_core::EntityOccasionSchedule) -> bool {
        self.id == other.local_id
    }
}

impl Filler<df_st_core::EntityOccasionScheduleFeature, Feature>
    for df_st_core::EntityOccasionScheduleFeature
{
    fn add_missing_data(&mut self, source: &Feature) {
        self.type_.add_missing_data(&source.type_);
        self.reference.add_missing_data(&source.reference);
    }

    fn add_missing_data_indexed(&mut self, source: &Feature, index: u64) {
        if source.id.is_none() {
            // If no ID set, use index
            self.local_id.add_missing_data(&(index as i32));
        }
        self.add_missing_data(source);
    }
}

impl PartialEq<df_st_core::EntityOccasionScheduleFeature> for Feature {
    fn eq(&self, other: &df_st_core::EntityOccasionScheduleFeature) -> bool {
        self.type_ == other.type_ && self.reference == other.reference
    }
}
