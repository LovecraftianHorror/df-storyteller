use crate::db_object::DBObject;
use crate::DbConnection;
use df_st_core::{Fillable, Filler};
use df_st_derive::Fillable;
use indicatif::ProgressBar;

mod site_map_image;
pub use site_map_image::SiteMapImage;

#[derive(Clone, Fillable, Default, PartialEq, Debug)]
pub struct SiteMapImages {
    pub site_maps: Vec<SiteMapImage>,
}

macro_rules! progress_code(
    { $progress_bar:ident, $($label:expr => $code:block),* $(,)* } => {
        {
            let labels = vec![$($label),*];
            $progress_bar.set_length(labels.len() as u64);
            $(
                $progress_bar.set_message($label);
                $progress_bar.inc(1);
                $code
            )*
            $progress_bar.finish_with_message("Done ✔️");
        }
     };
);

impl SiteMapImages {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set_world_id(&mut self, world_id: i32) {
        for item in self.site_maps.iter_mut() {
            item.world_id = world_id;
        }
    }

    pub fn insert_into_db(&self, conn: &DbConnection, progress_bar: ProgressBar) {
        progress_code! { progress_bar,
            "Site Maps" => {SiteMapImage::insert_into_db(&conn, &self.site_maps);},
        }
    }
}

/// From Core to DB (World data)
impl Filler<SiteMapImages, df_st_core::SiteMapImages> for SiteMapImages {
    fn add_missing_data(&mut self, source: &df_st_core::SiteMapImages) {
        self.site_maps.add_missing_data(&source.site_maps);
    }
}
