use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{Coordinate, DBDFWorld};
use crate::schema::world_constructions;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;
use std::convert::TryInto;

#[allow(unused_imports)]
use failure::Error;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "world_constructions"]
pub struct WorldConstruction {
    pub id: i32,
    pub world_id: i32,
    pub name: Option<String>,
    pub type_: Option<String>,
}

impl WorldConstruction {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::WorldConstruction, WorldConstruction> for WorldConstruction {
    /// Add Coordinates to the world that are part of WorldConstruction
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for world_construction in core_world.world_constructions.values() {
            for coord in &world_construction.coords {
                let new_id: i32 = world.coordinates.len().try_into().unwrap();
                world.coordinates.push(Coordinate {
                    id: new_id,
                    x: coord.x,
                    y: coord.y,
                    region_id: Some(world_construction.id),
                    ..Default::default()
                });
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, world_constructions: &[WorldConstruction]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(world_constructions::table)
            .values(world_constructions)
            .on_conflict((world_constructions::id, world_constructions::world_id))
            .do_update()
            .set((
                world_constructions::name.eq(excluded(world_constructions::name)),
                world_constructions::type_.eq(excluded(world_constructions::type_)),
            ))
            .execute(conn)
            .expect("Error saving world_constructions");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, world_constructions: &[WorldConstruction]) {
        diesel::insert_into(world_constructions::table)
            .values(world_constructions)
            .execute(conn)
            .expect("Error saving world_constructions");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<WorldConstruction>, Error> {
        use crate::schema::world_constructions::dsl::*;
        let query = world_constructions;
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        return Ok(query.first::<WorldConstruction>(conn).optional()?);
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<WorldConstruction>, Error> {
        use crate::schema::world_constructions::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = world_constructions.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "name" => name,
                "type" => type_,
            });},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "name" => "name",
            "type" => "type",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[WorldConstruction],
        _core_list: Vec<df_st_core::WorldConstruction>,
    ) -> Result<Vec<df_st_core::WorldConstruction>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add Coordinates
        let coord_list = Coordinate::belonging_to(db_list)
            .filter(crate::schema::coordinates::world_id.eq(world_id))
            .load::<Coordinate>(conn)?
            .grouped_by(db_list);
        let core_list2 = db_list
            .into_iter()
            .zip(coord_list)
            .map(|(wc, coord)| {
                let mut core_wc = df_st_core::WorldConstruction::new();
                core_wc.add_missing_data(wc);
                let mut core_coords: Vec<df_st_core::Coordinate> = Vec::new();
                core_coords.add_missing_data(&coord);
                core_wc.add_missing_data(&df_st_core::WorldConstruction {
                    id: core_wc.id,
                    coords: core_coords,
                    ..Default::default()
                });
                core_wc
            })
            .collect();
        // There is currently no data created before this
        // function that is not in this object.
        // So just swapping it saves a bit of time.
        // core_list.add_missing_data(&core_list2);
        Ok(core_list2)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::world_constructions::dsl::*;
        let query = world_constructions
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "name" => {name: Option<String>},
                "type" => {type_: Option<String>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<WorldConstruction, df_st_core::WorldConstruction> for WorldConstruction {
    fn add_missing_data(&mut self, source: &df_st_core::WorldConstruction) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.type_.add_missing_data(&source.type_);
    }
}

/// From DB to Core
impl Filler<df_st_core::WorldConstruction, WorldConstruction> for df_st_core::WorldConstruction {
    fn add_missing_data(&mut self, source: &WorldConstruction) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.type_.add_missing_data(&source.type_);
    }
}

impl PartialEq<WorldConstruction> for df_st_core::WorldConstruction {
    fn eq(&self, other: &WorldConstruction) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::WorldConstruction> for WorldConstruction {
    fn eq(&self, other: &df_st_core::WorldConstruction) -> bool {
        self.id == other.id
    }
}
