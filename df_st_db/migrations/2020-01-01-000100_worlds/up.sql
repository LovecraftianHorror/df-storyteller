
CREATE TABLE worlds (
  id INTEGER NOT NULL,

  save_name VARCHAR,
  name VARCHAR,
  alternative_name VARCHAR,
  region_number INTEGER,
  year INTEGER,
  month INTEGER,
  day INTEGER,
  
  PRIMARY KEY (id)
);
