use df_st_core::{Coordinate, Rectangle};
use regex::Regex;
use serde::{Deserialize, Deserializer};

pub fn coordinate_deserializer<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<Coordinate>, D::Error> {
    let coords: String = Deserialize::deserialize(deserializer)?;
    if coords.is_empty() {
        return Ok(None);
    }
    let re = Regex::new(r"^([0-9]+),([0-9]+)$").unwrap();
    let values = re.captures(&coords).unwrap();
    Ok(Some(Coordinate {
        x: values[1].parse::<i32>().unwrap(),
        y: values[2].parse::<i32>().unwrap(),
        ..Default::default()
    }))
}

pub fn rectangle_deserializer<'de, D: Deserializer<'de>>(
    deserializer: D,
) -> Result<Option<Rectangle>, D::Error> {
    let rectangle: String = Deserialize::deserialize(deserializer)?;
    if rectangle.is_empty() {
        return Ok(None);
    }
    let re = Regex::new(r"^([0-9]+),([0-9]+):([0-9]+),([0-9]+)$").unwrap();
    let values = re.captures(&rectangle).unwrap();
    Ok(Some(Rectangle {
        x1: values[1].parse::<i32>().unwrap(),
        y1: values[2].parse::<i32>().unwrap(),
        x2: values[3].parse::<i32>().unwrap(),
        y2: values[4].parse::<i32>().unwrap(),
        ..Default::default()
    }))
}
