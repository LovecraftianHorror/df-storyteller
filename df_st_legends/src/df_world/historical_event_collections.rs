use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct HistoricalEventCollection {
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,

    pub start_year: Option<i32>,
    pub start_seconds72: Option<i32>,
    pub end_year: Option<i32>,
    pub end_seconds72: Option<i32>,

    // Items below are sometimes listed and the order does matter
    // this is currently not transferred during deserialization.
    // add special deserializers for this
    // maybe this might solve the problem: https://serde.rs/deserialize-map.html
    // https://gitlab.com/df_storyteller/df-storyteller/-/issues/3

    // All others are optional
    pub a_support_merc_enid: Option<i32>,
    pub a_support_merc_hfid: Option<Vec<i32>>,
    pub adjective: Option<String>,
    pub aggressor_ent_id: Option<i32>,
    pub attacking_enid: Option<i32>,
    pub attacking_hfid: Option<Vec<i32>>,
    pub attacking_merc_enid: Option<i32>,
    pub attacking_squad_animated: Option<Vec<Option<()>>>,
    pub attacking_squad_deaths: Option<Vec<i32>>,
    pub attacking_squad_entity_pop: Option<Vec<i32>>,
    pub attacking_squad_number: Option<Vec<i32>>,
    pub attacking_squad_race: Option<Vec<String>>,
    pub attacking_squad_site: Option<Vec<i32>>,

    pub civ_id: Option<i32>,
    pub company_merc: Option<()>,
    // TODO Parse further but DeserializeBestEffort has to be extended
    pub coords: Option<String>,
    pub d_support_merc_enid: Option<i32>,
    pub d_support_merc_hfid: Option<Vec<i32>>,
    pub defender_ent_id: Option<i32>,
    pub defending_enid: Option<i32>,
    pub defending_hfid: Option<Vec<i32>>,
    pub defending_merc_enid: Option<i32>,
    pub defending_squad_animated: Option<Vec<Option<()>>>,
    pub defending_squad_deaths: Option<Vec<i32>>,
    pub defending_squad_entity_pop: Option<Vec<i32>>,
    pub defending_squad_number: Option<Vec<i32>>,
    pub defending_squad_race: Option<Vec<String>>,
    pub defending_squad_site: Option<Vec<i32>>,
    pub event: Option<Vec<i32>>,
    pub eventcol: Option<Vec<i32>>,
    pub feature_layer_id: Option<i32>,
    pub individual_merc: Option<Vec<Option<()>>>,
    pub name: Option<String>,
    pub noncom_hfid: Option<Vec<i32>>,
    pub occasion_id: Option<i32>,
    pub ordinal: Option<i32>,
    pub outcome: Option<Vec<String>>,
    pub parent_eventcol: Option<i32>,
    pub site_id: Option<i32>,
    pub subregion_id: Option<i32>,
    pub target_entity_id: Option<i32>,
    pub war_eventcol: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HistoricalEventCollections {
    pub historical_event_collection: Option<Vec<HistoricalEventCollection>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEventCollection, HistoricalEventCollection>
    for df_st_core::HistoricalEventCollection
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollection) {
        self.id.add_missing_data(&source.id);
        self.type_.add_missing_data(&source.type_);
        self.start_year.add_missing_data(&source.start_year);
        self.start_seconds72
            .add_missing_data(&source.start_seconds72);
        self.end_year.add_missing_data(&source.end_year);
        self.end_seconds72.add_missing_data(&source.end_seconds72);
        self.a_support_merc_en_id
            .add_missing_data(&source.a_support_merc_enid);
        self.a_support_merc_hf_id
            .add_missing_data(&source.a_support_merc_hfid);
        self.adjective.add_missing_data(&source.adjective);
        self.aggressor_ent_id
            .add_missing_data(&source.aggressor_ent_id);
        self.attacking_en_id
            .add_missing_data(&source.attacking_enid);
        self.attacking_hf_id
            .add_missing_data(&source.attacking_hfid);
        self.attacking_merc_enid
            .add_missing_data(&source.attacking_merc_enid);
        if let Some(list) = &source.attacking_squad_animated {
            let source_attacking_squad_animated: Vec<bool> = list
                .iter()
                .map(|x| match x {
                    Some(_) => true,
                    None => false,
                })
                .collect();
            self.attacking_squad_animated
                .add_missing_data(&source_attacking_squad_animated);
        }
        self.attacking_squad_deaths
            .add_missing_data(&source.attacking_squad_deaths);
        self.attacking_squad_entity_pop
            .add_missing_data(&source.attacking_squad_entity_pop);
        self.attacking_squad_number
            .add_missing_data(&source.attacking_squad_number);
        self.attacking_squad_race
            .add_missing_data(&source.attacking_squad_race);
        self.attacking_squad_site
            .add_missing_data(&source.attacking_squad_site);

        self.civ_id.add_missing_data(&source.civ_id);
        self.company_merc.add_missing_data(&source.company_merc);
        self.coords.add_missing_data(&source.coords);
        self.d_support_merc_en_id
            .add_missing_data(&source.d_support_merc_enid);
        self.d_support_merc_hf_id
            .add_missing_data(&source.d_support_merc_hfid);
        self.defender_ent_id
            .add_missing_data(&source.defender_ent_id);
        self.defending_en_id
            .add_missing_data(&source.defending_enid);
        self.defending_hf_id
            .add_missing_data(&source.defending_hfid);
        self.defending_merc_en_id
            .add_missing_data(&source.defending_merc_enid);
        if let Some(list) = &source.defending_squad_animated {
            let source_defending_squad_animated: Vec<bool> = list
                .iter()
                .map(|x| match x {
                    Some(_) => true,
                    None => false,
                })
                .collect();
            self.defending_squad_animated
                .add_missing_data(&source_defending_squad_animated);
        }
        self.defending_squad_deaths
            .add_missing_data(&source.defending_squad_deaths);
        self.defending_squad_entity_pop
            .add_missing_data(&source.defending_squad_entity_pop);
        self.defending_squad_number
            .add_missing_data(&source.defending_squad_number);
        self.defending_squad_race
            .add_missing_data(&source.defending_squad_race);
        self.defending_squad_site
            .add_missing_data(&source.defending_squad_site);
        self.he_ids.add_missing_data(&source.event);
        self.hec_ids.add_missing_data(&source.eventcol);
        self.feature_layer_id
            .add_missing_data(&source.feature_layer_id);
        if let Some(list) = &source.individual_merc {
            let source_individual_merc: Vec<bool> = list
                .iter()
                .map(|x| match x {
                    Some(_) => true,
                    None => false,
                })
                .collect();
            self.individual_merc
                .add_missing_data(&source_individual_merc);
        }
        self.name.add_missing_data(&source.name);
        self.noncom_hf_id.add_missing_data(&source.noncom_hfid);
        self.occasion_id.add_missing_data(&source.occasion_id);
        self.ordinal.add_missing_data(&source.ordinal);
        self.outcome.add_missing_data(&source.outcome);
        self.parent_hec_id.add_missing_data(&source.parent_eventcol);
        self.site_id.add_missing_data(&source.site_id);
        self.subregion_id.add_missing_data(&source.subregion_id);
        self.target_entity_id
            .add_missing_data(&source.target_entity_id);
        self.war_hec_id.add_missing_data(&source.war_eventcol);
    }
}

impl PartialEq<df_st_core::HistoricalEventCollection> for HistoricalEventCollection {
    fn eq(&self, other: &df_st_core::HistoricalEventCollection) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::HistoricalEventCollection>, HistoricalEventCollections>
    for Vec<df_st_core::HistoricalEventCollection>
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollections) {
        self.add_missing_data(&source.historical_event_collection);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEventCollection>, HistoricalEventCollections>
    for IndexMap<u64, df_st_core::HistoricalEventCollection>
{
    fn add_missing_data(&mut self, source: &HistoricalEventCollections) {
        self.add_missing_data(&source.historical_event_collection);
    }
}
