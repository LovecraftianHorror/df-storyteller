use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_c_c;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_c_c"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventCC {
    pub he_id: i32,
    pub world_id: i32,

    pub cause: Option<String>,
    pub changee_hf_id: Option<i32>,
    pub changer_hf_id: Option<i32>,
    pub circumstance: Option<String>,
    pub circumstance_id: Option<i32>,
    pub civ_entity_id: Option<i32>,
    pub civ_id: Option<i32>,
    pub claim: Option<String>,
    pub coconspirator_bonus: Option<i32>,
    pub coconspirator_hf_id: Option<i32>,
    pub confessed_after_apb_arrest_en_id: Option<i32>,
    pub contact_hf_id: Option<i32>,
    pub convict_is_contact: Option<bool>,
    pub convicted_hf_id: Option<i32>,
    pub convicter_en_id: Option<i32>,
    pub coord: Option<String>,
    pub corrupt_convicter_hf_id: Option<i32>,
    pub corruptor_hf_id: Option<i32>,
    pub corruptor_identity: Option<i32>,
    pub corruptor_seen_as: Option<String>,
    pub creator_hf_id: Option<i32>,
    pub crime: Option<String>,
    pub caste: Option<String>,
    pub creator_unit_id: Option<i32>,
}

impl HistoricalEventCC {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventCC> for HistoricalEventCC {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_c_c: &[HistoricalEventCC]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_c_c::table)
            .values(historical_events_c_c)
            .on_conflict((
                historical_events_c_c::he_id,
                historical_events_c_c::world_id,
            ))
            .do_update()
            .set((
                historical_events_c_c::cause.eq(excluded(historical_events_c_c::cause)),
                historical_events_c_c::changee_hf_id
                    .eq(excluded(historical_events_c_c::changee_hf_id)),
                historical_events_c_c::changer_hf_id
                    .eq(excluded(historical_events_c_c::changer_hf_id)),
                historical_events_c_c::circumstance
                    .eq(excluded(historical_events_c_c::circumstance)),
                historical_events_c_c::circumstance_id
                    .eq(excluded(historical_events_c_c::circumstance_id)),
                historical_events_c_c::civ_entity_id
                    .eq(excluded(historical_events_c_c::civ_entity_id)),
                historical_events_c_c::civ_id.eq(excluded(historical_events_c_c::civ_id)),
                historical_events_c_c::claim.eq(excluded(historical_events_c_c::claim)),
                historical_events_c_c::coconspirator_bonus
                    .eq(excluded(historical_events_c_c::coconspirator_bonus)),
                historical_events_c_c::coconspirator_hf_id
                    .eq(excluded(historical_events_c_c::coconspirator_hf_id)),
                historical_events_c_c::confessed_after_apb_arrest_en_id.eq(excluded(
                    historical_events_c_c::confessed_after_apb_arrest_en_id,
                )),
                historical_events_c_c::contact_hf_id
                    .eq(excluded(historical_events_c_c::contact_hf_id)),
                historical_events_c_c::convict_is_contact
                    .eq(excluded(historical_events_c_c::convict_is_contact)),
                historical_events_c_c::convicted_hf_id
                    .eq(excluded(historical_events_c_c::convicted_hf_id)),
                historical_events_c_c::convicter_en_id
                    .eq(excluded(historical_events_c_c::convicter_en_id)),
                historical_events_c_c::coord.eq(excluded(historical_events_c_c::coord)),
                historical_events_c_c::corrupt_convicter_hf_id
                    .eq(excluded(historical_events_c_c::corrupt_convicter_hf_id)),
                historical_events_c_c::corruptor_hf_id
                    .eq(excluded(historical_events_c_c::corruptor_hf_id)),
                historical_events_c_c::corruptor_identity
                    .eq(excluded(historical_events_c_c::corruptor_identity)),
                historical_events_c_c::corruptor_seen_as
                    .eq(excluded(historical_events_c_c::corruptor_seen_as)),
                historical_events_c_c::creator_hf_id
                    .eq(excluded(historical_events_c_c::creator_hf_id)),
                historical_events_c_c::crime.eq(excluded(historical_events_c_c::crime)),
                historical_events_c_c::caste.eq(excluded(historical_events_c_c::caste)),
                historical_events_c_c::creator_unit_id
                    .eq(excluded(historical_events_c_c::creator_unit_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_c_c");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_c_c: &[HistoricalEventCC]) {
        diesel::insert_into(historical_events_c_c::table)
            .values(historical_events_c_c)
            .execute(conn)
            .expect("Error saving historical_events_c_c");
    }

    /// Get a list of HistoricalEventCC from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventCC>, Error> {
        /*
        use crate::schema::historical_events_c_c::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_c_c.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "cause" => cause,
                "changee_hf_id" => changee_hf_id,
                "changer_hf_id" => changer_hf_id,
                "circumstance" => circumstance,
                "circumstance_id" => circumstance_id,
                "civ_entity_id" => civ_entity_id,
                "civ_id" => civ_id,
                "claim" => claim,
                "coconspirator_bonus" => coconspirator_bonus,
                "coconspirator_hf_id" => coconspirator_hf_id,
                "confessed_after_apb_arrest_en_id" => confessed_after_apb_arrest_en_id,
                "contact_hf_id" => contact_hf_id,
                "convict_is_contact" => convict_is_contact,
                "convicted_hf_id" => convicted_hf_id,
                "convicter_en_id" => convicter_en_id,
                "coord" => coord,
                "corrupt_convicter_hf_id" => corrupt_convicter_hf_id,
                "corruptor_hf_id" => corruptor_hf_id,
                "corruptor_identity" => corruptor_identity,
                "corruptor_seen_as" => corruptor_seen_as,
                "creator_hf_id" => creator_hf_id,
                "crime" => crime,
                "caste" => caste,
                "creator_unit_id" => creator_unit_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventCC>, Error> {
        use crate::schema::historical_events_c_c::dsl::*;
        let query = historical_events_c_c;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventCC>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "cause" => "cause",
            "changee_hf_id" => "changee_hf_id",
            "changer_hf_id" => "changer_hf_id",
            "circumstance" => "circumstance",
            "circumstance_id" => "circumstance_id",
            "civ_entity_id" => "civ_entity_id",
            "civ_id" => "civ_id",
            "claim" => "claim",
            "coconspirator_bonus" => "coconspirator_bonus",
            "coconspirator_hf_id" => "coconspirator_hf_id",
            "confessed_after_apb_arrest_en_id" => "confessed_after_apb_arrest_en_id",
            "contact_hf_id" => "contact_hf_id",
            "convict_is_contact" => "convict_is_contact",
            "convicted_hf_id" => "convicted_hf_id",
            "convicter_en_id" => "convicter_en_id",
            "coord" => "coord",
            "corrupt_convicter_hf_id" => "corrupt_convicter_hf_id",
            "corruptor_hf_id" => "corruptor_hf_id",
            "corruptor_identity" => "corruptor_identity",
            "corruptor_seen_as" => "corruptor_seen_as",
            "creator_hf_id" => "creator_hf_id",
            "crime" => "crime",
            "caste" => "caste",
            "creator_unit_id" => "creator_unit_id",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventCC],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_c_c::dsl::*;
        let query = historical_events_c_c.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "cause" => {cause: Option<String>},
                "changee_hf_id" => {changee_hf_id: Option<i32>},
                "changer_hf_id" => {changer_hf_id: Option<i32>},
                "circumstance" => {circumstance: Option<String>},
                "circumstance_id" => {circumstance_id: Option<i32>},
                "civ_entity_id" => {civ_entity_id: Option<i32>},
                "civ_id" => {civ_id: Option<i32>},
                "claim" => {claim: Option<String>},
                "coconspirator_bonus" => {coconspirator_bonus: Option<i32>},
                "coconspirator_hf_id" => {coconspirator_hf_id: Option<i32>},
                "confessed_after_apb_arrest_en_id" => {confessed_after_apb_arrest_en_id: Option<i32>},
                "contact_hf_id" => {contact_hf_id: Option<i32>},
                "convict_is_contact" => {convict_is_contact: Option<bool>},
                "convicted_hf_id" => {convicted_hf_id: Option<i32>},
                "convicter_en_id" => {convicter_en_id: Option<i32>},
                "coord" => {coord: Option<String>},
                "corrupt_convicter_hf_id" => {corrupt_convicter_hf_id: Option<i32>},
                "corruptor_hf_id" => {corruptor_hf_id: Option<i32>},
                "corruptor_identity" => {corruptor_identity: Option<i32>},
                "corruptor_seen_as" => {corruptor_seen_as: Option<String>},
                "creator_hf_id" => {creator_hf_id: Option<i32>},
                "crime" => {crime: Option<String>},
                "caste" => {caste: Option<String>},
                "creator_unit_id" => {creator_unit_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventCC, df_st_core::HistoricalEvent> for HistoricalEventCC {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.cause.add_missing_data(&source.cause);
        self.changee_hf_id.add_missing_data(&source.changee_hf_id);
        self.changer_hf_id.add_missing_data(&source.changer_hf_id);
        self.circumstance.add_missing_data(&source.circumstance);
        self.circumstance_id
            .add_missing_data(&source.circumstance_id);
        self.civ_entity_id.add_missing_data(&source.civ_entity_id);
        self.civ_id.add_missing_data(&source.civ_id);
        self.claim.add_missing_data(&source.claim);
        self.coconspirator_bonus
            .add_missing_data(&source.coconspirator_bonus);
        self.coconspirator_hf_id
            .add_missing_data(&source.coconspirator_hf_id);
        self.confessed_after_apb_arrest_en_id
            .add_missing_data(&source.confessed_after_apb_arrest_en_id);
        self.contact_hf_id.add_missing_data(&source.contact_hf_id);
        self.convict_is_contact
            .add_missing_data(&source.convict_is_contact);
        self.convicted_hf_id
            .add_missing_data(&source.convicted_hf_id);
        self.convicter_en_id
            .add_missing_data(&source.convicter_en_id);
        self.coord.add_missing_data(&source.coord);
        self.corrupt_convicter_hf_id
            .add_missing_data(&source.corrupt_convicter_hf_id);
        self.corruptor_hf_id
            .add_missing_data(&source.corruptor_hf_id);
        self.corruptor_identity
            .add_missing_data(&source.corruptor_identity);
        self.corruptor_seen_as
            .add_missing_data(&source.corruptor_seen_as);
        self.creator_hf_id.add_missing_data(&source.creator_hf_id);
        self.crime.add_missing_data(&source.crime);
        self.caste.add_missing_data(&source.caste);
        self.creator_unit_id
            .add_missing_data(&source.creator_unit_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventCC> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventCC) {
        self.id.add_missing_data(&source.he_id);
        self.cause.add_missing_data(&source.cause);
        self.changee_hf_id.add_missing_data(&source.changee_hf_id);
        self.changer_hf_id.add_missing_data(&source.changer_hf_id);
        self.circumstance.add_missing_data(&source.circumstance);
        self.circumstance_id
            .add_missing_data(&source.circumstance_id);
        self.civ_entity_id.add_missing_data(&source.civ_entity_id);
        self.civ_id.add_missing_data(&source.civ_id);
        self.claim.add_missing_data(&source.claim);
        self.coconspirator_bonus
            .add_missing_data(&source.coconspirator_bonus);
        self.coconspirator_hf_id
            .add_missing_data(&source.coconspirator_hf_id);
        self.confessed_after_apb_arrest_en_id
            .add_missing_data(&source.confessed_after_apb_arrest_en_id);
        self.contact_hf_id.add_missing_data(&source.contact_hf_id);
        self.convict_is_contact
            .add_missing_data(&source.convict_is_contact);
        self.convicted_hf_id
            .add_missing_data(&source.convicted_hf_id);
        self.convicter_en_id
            .add_missing_data(&source.convicter_en_id);
        self.coord.add_missing_data(&source.coord);
        self.corrupt_convicter_hf_id
            .add_missing_data(&source.corrupt_convicter_hf_id);
        self.corruptor_hf_id
            .add_missing_data(&source.corruptor_hf_id);
        self.corruptor_identity
            .add_missing_data(&source.corruptor_identity);
        self.corruptor_seen_as
            .add_missing_data(&source.corruptor_seen_as);
        self.creator_hf_id.add_missing_data(&source.creator_hf_id);
        self.crime.add_missing_data(&source.crime);
        self.caste.add_missing_data(&source.caste);
        self.creator_unit_id
            .add_missing_data(&source.creator_unit_id);
    }
}

impl PartialEq for HistoricalEventCC {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventCC {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventCC> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventCC) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventCC {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
