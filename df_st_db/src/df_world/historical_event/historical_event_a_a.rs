use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_a_a;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_a_a"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventAA {
    pub he_id: i32,
    pub world_id: i32,

    pub a_support_merc_en_id: Option<i32>,
    pub account_shift: Option<i32>,
    pub acquirer_en_id: Option<i32>,
    pub acquirer_hf_id: Option<i32>,
    pub action: Option<String>,
    pub actor_hf_id: Option<i32>,
    pub agreement_id: Option<i32>,
    pub allotment: Option<i32>,
    pub allotment_index: Option<i32>,
    pub ally_defense_bonus: Option<i32>,
    pub appointer_hf_id: Option<i32>,
    pub arresting_en_id: Option<i32>,
    pub artifact_id: Option<i32>,
    pub attacker_civ_id: Option<i32>,
    pub attacker_general_hf_id: Option<i32>,
    pub attacker_hf_id: Option<i32>,
    pub attacker_merc_en_id: Option<i32>,
    pub abuse_type: Option<String>,
    pub anon_3: Option<i32>,
    pub anon_4: Option<i32>,
}

impl HistoricalEventAA {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventAA> for HistoricalEventAA {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_a_a: &[HistoricalEventAA]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_a_a::table)
            .values(historical_events_a_a)
            .on_conflict((
                historical_events_a_a::he_id,
                historical_events_a_a::world_id,
            ))
            .do_update()
            .set((
                historical_events_a_a::a_support_merc_en_id
                    .eq(excluded(historical_events_a_a::a_support_merc_en_id)),
                historical_events_a_a::account_shift
                    .eq(excluded(historical_events_a_a::account_shift)),
                historical_events_a_a::acquirer_en_id
                    .eq(excluded(historical_events_a_a::acquirer_en_id)),
                historical_events_a_a::acquirer_hf_id
                    .eq(excluded(historical_events_a_a::acquirer_hf_id)),
                historical_events_a_a::action.eq(excluded(historical_events_a_a::action)),
                historical_events_a_a::actor_hf_id.eq(excluded(historical_events_a_a::actor_hf_id)),
                historical_events_a_a::agreement_id
                    .eq(excluded(historical_events_a_a::agreement_id)),
                historical_events_a_a::allotment.eq(excluded(historical_events_a_a::allotment)),
                historical_events_a_a::allotment_index
                    .eq(excluded(historical_events_a_a::allotment_index)),
                historical_events_a_a::ally_defense_bonus
                    .eq(excluded(historical_events_a_a::ally_defense_bonus)),
                historical_events_a_a::appointer_hf_id
                    .eq(excluded(historical_events_a_a::appointer_hf_id)),
                historical_events_a_a::arresting_en_id
                    .eq(excluded(historical_events_a_a::arresting_en_id)),
                historical_events_a_a::artifact_id.eq(excluded(historical_events_a_a::artifact_id)),
                historical_events_a_a::attacker_civ_id
                    .eq(excluded(historical_events_a_a::attacker_civ_id)),
                historical_events_a_a::attacker_general_hf_id
                    .eq(excluded(historical_events_a_a::attacker_general_hf_id)),
                historical_events_a_a::attacker_hf_id
                    .eq(excluded(historical_events_a_a::attacker_hf_id)),
                historical_events_a_a::attacker_merc_en_id
                    .eq(excluded(historical_events_a_a::attacker_merc_en_id)),
                historical_events_a_a::abuse_type.eq(excluded(historical_events_a_a::abuse_type)),
                historical_events_a_a::anon_3.eq(excluded(historical_events_a_a::anon_3)),
                historical_events_a_a::anon_4.eq(excluded(historical_events_a_a::anon_4)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_a_a");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_a_a: &[HistoricalEventAA]) {
        diesel::insert_into(historical_events_a_a::table)
            .values(historical_events_a_a)
            .execute(conn)
            .expect("Error saving historical_events_a_a");
    }

    /// Get a list of HistoricalEventAA from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventAA>, Error> {
        /*
        use crate::schema::historical_events_a_a::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_a_a.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "a_support_merc_en_id" => a_support_merc_en_id,
                "account_shift" => account_shift,
                "acquirer_en_id" => acquirer_en_id,
                "acquirer_hf_id" => acquirer_hf_id,
                "action" => action,
                "actor_hf_id" => actor_hf_id,
                "agreement_id" => agreement_id,
                "allotment" => allotment,
                "allotment_index" => allotment_index,
                "ally_defense_bonus" => ally_defense_bonus,
                "appointer_hf_id" => appointer_hf_id,
                "arresting_en_id" => arresting_en_id,
                "artifact_id" => artifact_id,
                "attacker_civ_id" => attacker_civ_id,
                "attacker_general_hf_id" => attacker_general_hf_id,
                "attacker_hf_id" => attacker_hf_id,
                "attacker_merc_en_id" => attacker_merc_en_id,
                "abuse_type" => abuse_type,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventAA>, Error> {
        use crate::schema::historical_events_a_a::dsl::*;
        let query = historical_events_a_a;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventAA>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "a_support_merc_en_id" => "a_support_merc_en_id",
            "account_shift" => "account_shift",
            "acquirer_en_id" => "acquirer_en_id",
            "acquirer_hf_id" => "acquirer_hf_id",
            "action" => "action",
            "actor_hf_id" => "actor_hf_id",
            "agreement_id" => "agreement_id",
            "allotment" => "allotment",
            "allotment_index" => "allotment_index",
            "ally_defense_bonus" => "ally_defense_bonus",
            "appointer_hf_id" => "appointer_hf_id",
            "arresting_en_id" => "arresting_en_id",
            "artifact_id" => "artifact_id",
            "attacker_civ_id" => "attacker_civ_id",
            "attacker_general_hf_id" => "attacker_general_hf_id",
            "attacker_hf_id" => "attacker_hf_id",
            "attacker_merc_en_id" => "attacker_merc_en_id",
            "abuse_type" => "abuse_type",
            "anon_3" => "anon_3",
            "anon_4" => "anon_4",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventAA],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_a_a::dsl::*;
        let query = historical_events_a_a.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "a_support_merc_en_id" => {a_support_merc_en_id: Option<i32>},
                "account_shift" => {account_shift: Option<i32>},
                "acquirer_en_id" => {acquirer_en_id: Option<i32>},
                "acquirer_hf_id" => {acquirer_hf_id: Option<i32>},
                "action" => {action: Option<String>},
                "actor_hf_id" => {actor_hf_id: Option<i32>},
                "agreement_id" => {agreement_id: Option<i32>},
                "allotment" => {allotment: Option<i32>},
                "allotment_index" => {allotment_index: Option<i32>},
                "ally_defense_bonus" => {ally_defense_bonus: Option<i32>},
                "appointer_hf_id" => {appointer_hf_id: Option<i32>},
                "arresting_en_id" => {arresting_en_id: Option<i32>},
                "artifact_id" => {artifact_id: Option<i32>},
                "attacker_civ_id" => {attacker_civ_id: Option<i32>},
                "attacker_general_hf_id" => {attacker_general_hf_id: Option<i32>},
                "attacker_hf_id" => {attacker_hf_id: Option<i32>},
                "attacker_merc_en_id" => {attacker_merc_en_id: Option<i32>},
                "abuse_type" => {abuse_type: Option<String>},
                "anon_3" => {anon_3: Option<i32>},
                "anon_4" => {anon_4: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventAA, df_st_core::HistoricalEvent> for HistoricalEventAA {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.a_support_merc_en_id
            .add_missing_data(&source.a_support_merc_en_id);
        self.account_shift.add_missing_data(&source.account_shift);
        self.acquirer_en_id.add_missing_data(&source.acquirer_en_id);
        self.acquirer_hf_id.add_missing_data(&source.acquirer_hf_id);
        self.action.add_missing_data(&source.action);
        self.actor_hf_id.add_missing_data(&source.actor_hf_id);
        self.agreement_id.add_missing_data(&source.agreement_id);
        self.allotment.add_missing_data(&source.allotment);
        self.allotment_index
            .add_missing_data(&source.allotment_index);
        self.ally_defense_bonus
            .add_missing_data(&source.ally_defense_bonus);
        self.appointer_hf_id
            .add_missing_data(&source.appointer_hf_id);
        self.arresting_en_id
            .add_missing_data(&source.arresting_en_id);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.attacker_civ_id
            .add_missing_data(&source.attacker_civ_id);
        self.attacker_general_hf_id
            .add_missing_data(&source.attacker_general_hf_id);
        self.attacker_hf_id.add_missing_data(&source.attacker_hf_id);
        self.attacker_merc_en_id
            .add_missing_data(&source.attacker_merc_en_id);
        self.abuse_type.add_missing_data(&source.abuse_type);
        self.anon_3.add_missing_data(&source.anon_3);
        self.anon_4.add_missing_data(&source.anon_4);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventAA> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventAA) {
        self.id.add_missing_data(&source.he_id);
        self.a_support_merc_en_id
            .add_missing_data(&source.a_support_merc_en_id);
        self.account_shift.add_missing_data(&source.account_shift);
        self.acquirer_en_id.add_missing_data(&source.acquirer_en_id);
        self.acquirer_hf_id.add_missing_data(&source.acquirer_hf_id);
        self.action.add_missing_data(&source.action);
        self.actor_hf_id.add_missing_data(&source.actor_hf_id);
        self.agreement_id.add_missing_data(&source.agreement_id);
        self.allotment.add_missing_data(&source.allotment);
        self.allotment_index
            .add_missing_data(&source.allotment_index);
        self.ally_defense_bonus
            .add_missing_data(&source.ally_defense_bonus);
        self.appointer_hf_id
            .add_missing_data(&source.appointer_hf_id);
        self.arresting_en_id
            .add_missing_data(&source.arresting_en_id);
        self.artifact_id.add_missing_data(&source.artifact_id);
        self.attacker_civ_id
            .add_missing_data(&source.attacker_civ_id);
        self.attacker_general_hf_id
            .add_missing_data(&source.attacker_general_hf_id);
        self.attacker_hf_id.add_missing_data(&source.attacker_hf_id);
        self.attacker_merc_en_id
            .add_missing_data(&source.attacker_merc_en_id);
        self.abuse_type.add_missing_data(&source.abuse_type);
        self.anon_3.add_missing_data(&source.anon_3);
        self.anon_4.add_missing_data(&source.anon_4);
    }
}

impl PartialEq for HistoricalEventAA {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventAA {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventAA> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventAA) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventAA {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
