use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_relationship_profile_hf_historical;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_relationship_profile_hf_historical"]
#[primary_key(hf_id, hf_id_other)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFRelationshipProfileHFHistorical {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub world_id: i32,
    pub love: Option<i32>,
    pub respect: Option<i32>,
    pub trust: Option<i32>,
    pub loyalty: Option<i32>,
    pub fear: Option<i32>,
}

impl HFRelationshipProfileHFHistorical {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFRelationshipProfileHFHistorical, HFRelationshipProfileHFHistorical>
    for HFRelationshipProfileHFHistorical
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        hf_relationship_profile_hf_historical: &[HFRelationshipProfileHFHistorical],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_relationship_profile_hf_historical::table)
            .values(hf_relationship_profile_hf_historical)
            .on_conflict((
                hf_relationship_profile_hf_historical::hf_id,
                hf_relationship_profile_hf_historical::hf_id_other,
                hf_relationship_profile_hf_historical::world_id,
            ))
            .do_update()
            .set((
                hf_relationship_profile_hf_historical::love
                    .eq(excluded(hf_relationship_profile_hf_historical::love)),
                hf_relationship_profile_hf_historical::respect
                    .eq(excluded(hf_relationship_profile_hf_historical::respect)),
                hf_relationship_profile_hf_historical::trust
                    .eq(excluded(hf_relationship_profile_hf_historical::trust)),
                hf_relationship_profile_hf_historical::loyalty
                    .eq(excluded(hf_relationship_profile_hf_historical::loyalty)),
                hf_relationship_profile_hf_historical::fear
                    .eq(excluded(hf_relationship_profile_hf_historical::fear)),
            ))
            .execute(conn)
            .expect("Error saving hf_relationship_profile_hf_historical");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        hf_relationship_profile_hf_historical: &[HFRelationshipProfileHFHistorical],
    ) {
        diesel::insert_into(hf_relationship_profile_hf_historical::table)
            .values(hf_relationship_profile_hf_historical)
            .execute(conn)
            .expect("Error saving hf_relationship_profile_hf_historical");
    }

    /// Get a list of HFRelationshipProfileHFHistorical from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFRelationshipProfileHFHistorical>, Error> {
        use crate::schema::hf_relationship_profile_hf_historical::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_relationship_profile_hf_historical
            .limit(limit)
            .offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
                "love" => love,
                "respect" => respect,
                "trust" => trust,
                "loyalty" => loyalty,
                "fear" => fear,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFRelationshipProfileHFHistorical>, Error> {
        use crate::schema::hf_relationship_profile_hf_historical::dsl::*;
        let query = hf_relationship_profile_hf_historical;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {return Ok(query.first::<HFRelationshipProfileHFHistorical>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "hf_id_other" => "hf_id_other",
            "love" => "love",
            "respect" => "respect",
            "trust" => "trust",
            "loyalty" => "loyalty",
            "fear" => "fear",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFRelationshipProfileHFHistorical],
        core_list: Vec<df_st_core::HFRelationshipProfileHFHistorical>,
    ) -> Result<Vec<df_st_core::HFRelationshipProfileHFHistorical>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_relationship_profile_hf_historical::dsl::*;
        let query = hf_relationship_profile_hf_historical
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "hf_id_other" => {hf_id_other: i32},
                "love" => {love: Option<i32>},
                "respect" => {respect: Option<i32>},
                "trust" => {trust: Option<i32>},
                "loyalty" => {loyalty: Option<i32>},
                "fear" => {fear: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFRelationshipProfileHFHistorical, df_st_core::HFRelationshipProfileHFHistorical>
    for HFRelationshipProfileHFHistorical
{
    fn add_missing_data(&mut self, source: &df_st_core::HFRelationshipProfileHFHistorical) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFRelationshipProfileHFHistorical, HFRelationshipProfileHFHistorical>
    for df_st_core::HFRelationshipProfileHFHistorical
{
    fn add_missing_data(&mut self, source: &HFRelationshipProfileHFHistorical) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
    }
}

impl PartialEq<df_st_core::HFRelationshipProfileHFHistorical>
    for HFRelationshipProfileHFHistorical
{
    fn eq(&self, other: &df_st_core::HFRelationshipProfileHFHistorical) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq<HFRelationshipProfileHFHistorical>
    for df_st_core::HFRelationshipProfileHFHistorical
{
    fn eq(&self, other: &HFRelationshipProfileHFHistorical) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq for HFRelationshipProfileHFHistorical {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl Hash for HFRelationshipProfileHFHistorical {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}
