use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_journey_pets;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hf_journey_pets"]
#[primary_key(hf_id, journey_pet)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFJourneyPets {
    pub hf_id: i32,
    pub journey_pet: String,
    pub world_id: i32,
}

impl HFJourneyPets {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HFJourneyPets, HFJourneyPets> for HFJourneyPets {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_journey_pets: &[HFJourneyPets]) {
        diesel::insert_into(hf_journey_pets::table)
            .values(hf_journey_pets)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hf_journey_pets");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_journey_pets: &[HFJourneyPets]) {
        diesel::insert_into(hf_journey_pets::table)
            .values(hf_journey_pets)
            .execute(conn)
            .expect("Error saving hf_journey_pets");
    }

    /// Get a list of HFJourneyPets from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFJourneyPets>, Error> {
        use crate::schema::hf_journey_pets::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_journey_pets.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "journey_pet" => journey_pet,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFJourneyPets>, Error> {
        use crate::schema::hf_journey_pets::dsl::*;
        let query = hf_journey_pets;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
            ],
            {return Ok(query.first::<HFJourneyPets>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "journey_pet" => "journey_pet",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFJourneyPets],
        core_list: Vec<HFJourneyPets>,
    ) -> Result<Vec<HFJourneyPets>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_journey_pets::dsl::*;
        let query = hf_journey_pets.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "journey_pet" => {journey_pet: String},
            };},
        };
    }
}

impl PartialEq for HFJourneyPets {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.journey_pet == other.journey_pet
    }
}

impl Hash for HFJourneyPets {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.journey_pet.hash(state);
    }
}
