use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::creatures;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use failure::Error;
use std::collections::HashMap;

mod creature_a_g;
mod creature_biome_1;
mod creature_biome_2;
mod creature_h_h_1;
mod creature_h_h_2;
mod creature_l_z;
pub use creature_a_g::CreatureAG;
pub use creature_biome_1::CreatureBiome1;
pub use creature_biome_2::CreatureBiome2;
pub use creature_h_h_1::CreatureHH1;
pub use creature_h_h_2::CreatureHH2;
pub use creature_l_z::CreatureLZ;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "creatures"]
pub struct Creature {
    pub id: i32,
    pub world_id: i32,
    pub creature_id: Option<String>,
    pub name_singular: Option<String>,
    pub name_plural: Option<String>,
}

impl Creature {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Creature, Creature> for Creature {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for creature in core_world.creatures.values() {
            // Add biomes1 list
            let mut df_biomes_1 = CreatureBiome1::new();
            df_biomes_1.cr_id = creature.id;
            df_biomes_1.add_missing_data(&creature.biomes);
            world.creature_biomes_1.push(df_biomes_1);
            // Add biomes2 list
            let mut df_biomes_2 = CreatureBiome2::new();
            df_biomes_2.cr_id = creature.id;
            df_biomes_2.add_missing_data(&creature.biomes);
            world.creature_biomes_2.push(df_biomes_2);
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, creatures: &[Creature]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(creatures::table)
            .values(creatures)
            .on_conflict((creatures::id, creatures::world_id))
            .do_update()
            .set((
                creatures::creature_id.eq(excluded(creatures::creature_id)),
                creatures::name_singular.eq(excluded(creatures::name_singular)),
                creatures::name_plural.eq(excluded(creatures::name_plural)),
            ))
            .execute(conn)
            .expect("Error saving creatures");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, creatures: &[Creature]) {
        diesel::insert_into(creatures::table)
            .values(creatures)
            .execute(conn)
            .expect("Error saving creatures");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<Creature>, Error> {
        use crate::schema::creatures::dsl::*;
        let query = creatures;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        return Ok(query.first::<Creature>(conn).optional()?);
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<Creature>, Error> {
        use crate::schema::creatures::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = creatures.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "creature_id" => creature_id,
                "name_singular" => name_singular,
                "name_plural" => name_plural,
            });},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "creature_id" => "creature_id",
            "name_singular" => "name_singular",
            "name_plural" => "name_plural",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[Creature],
        _core_list: Vec<df_st_core::Creature>,
    ) -> Result<Vec<df_st_core::Creature>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add CreatureAG
        let cr_a_g_list = CreatureAG::belonging_to(db_list)
            .filter(crate::schema::creatures_a_g::world_id.eq(world_id))
            .load::<CreatureAG>(conn)?
            .grouped_by(db_list);
        // Add CreatureHH1
        let cr_h_h_1_list = CreatureHH1::belonging_to(db_list)
            .filter(crate::schema::creatures_h_h_1::world_id.eq(world_id))
            .load::<CreatureHH1>(conn)?
            .grouped_by(db_list);
        // Add CreatureHH2
        let cr_h_h_2_list = CreatureHH2::belonging_to(db_list)
            .filter(crate::schema::creatures_h_h_2::world_id.eq(world_id))
            .load::<CreatureHH2>(conn)?
            .grouped_by(db_list);
        // Add CreatureLZ
        let cr_l_z_list = CreatureLZ::belonging_to(db_list)
            .filter(crate::schema::creatures_l_z::world_id.eq(world_id))
            .load::<CreatureLZ>(conn)?
            .grouped_by(db_list);
        // Add CreatureBiome1
        let cr_biome_1_list = CreatureBiome1::belonging_to(db_list)
            .filter(crate::schema::creature_biomes_1::world_id.eq(world_id))
            .load::<CreatureBiome1>(conn)?
            .grouped_by(db_list);
        // Add CreatureBiome2
        let cr_biome_2_list = CreatureBiome2::belonging_to(db_list)
            .filter(crate::schema::creature_biomes_2::world_id.eq(world_id))
            .load::<CreatureBiome2>(conn)?
            .grouped_by(db_list);

        // Merge all
        let mut core_list: Vec<df_st_core::Creature> = Vec::new();
        for (index, cr) in db_list.into_iter().enumerate() {
            let mut core_cr = df_st_core::Creature::new();
            core_cr.add_missing_data(cr);
            let cr_a_g = cr_a_g_list.get(index).unwrap().get(0).unwrap();
            core_cr.add_missing_data(cr_a_g);
            let cr_h_h_1 = cr_h_h_1_list.get(index).unwrap().get(0).unwrap();
            core_cr.add_missing_data(cr_h_h_1);
            let cr_h_h_2 = cr_h_h_2_list.get(index).unwrap().get(0).unwrap();
            core_cr.add_missing_data(cr_h_h_2);
            let cr_l_z = cr_l_z_list.get(index).unwrap().get(0).unwrap();
            core_cr.add_missing_data(cr_l_z);

            let cr_biome_1 = cr_biome_1_list.get(index).unwrap().get(0).unwrap();
            core_cr.biomes.add_missing_data(cr_biome_1);
            let cr_biome_2 = cr_biome_2_list.get(index).unwrap().get(0).unwrap();
            core_cr.biomes.add_missing_data(cr_biome_2);

            core_list.push(core_cr);
        }

        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::creatures::dsl::*;
        let query = creatures.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "creature_id" => {creature_id: Option<String>},
                "name_singular" => {name_singular: Option<String>},
                "name_plural" => {name_plural: Option<String>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<Creature, df_st_core::Creature> for Creature {
    fn add_missing_data(&mut self, source: &df_st_core::Creature) {
        self.id.add_missing_data(&source.id);
        self.creature_id.add_missing_data(&source.creature_id);
        self.name_singular.add_missing_data(&source.name_singular);
        self.name_plural.add_missing_data(&source.name_plural);
    }
}

/// From DB to Core
impl Filler<df_st_core::Creature, Creature> for df_st_core::Creature {
    fn add_missing_data(&mut self, source: &Creature) {
        self.id.add_missing_data(&source.id);
        self.creature_id.add_missing_data(&source.creature_id);
        self.name_singular.add_missing_data(&source.name_singular);
        self.name_plural.add_missing_data(&source.name_plural);
    }
}

impl PartialEq<Creature> for df_st_core::Creature {
    fn eq(&self, other: &Creature) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::Creature> for Creature {
    fn eq(&self, other: &df_st_core::Creature) -> bool {
        self.id == other.id
    }
}
