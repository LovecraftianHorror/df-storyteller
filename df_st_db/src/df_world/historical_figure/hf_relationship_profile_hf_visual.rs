use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_relationship_profile_hf_visual;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_relationship_profile_hf_visual"]
#[primary_key(hf_id, hf_id_other)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFRelationshipProfileHFVisual {
    pub hf_id: i32,
    pub hf_id_other: i32,
    pub world_id: i32,
    pub meet_count: Option<i32>,
    pub last_meet_year: Option<i32>,
    pub last_meet_seconds72: Option<i32>,
    pub love: Option<i32>,
    pub respect: Option<i32>,
    pub trust: Option<i32>,
    pub loyalty: Option<i32>,
    pub fear: Option<i32>,
    pub known_identity_id: Option<i32>,
    pub rep_friendly: Option<i32>,
    pub rep_information_source: Option<i32>,
}

impl HFRelationshipProfileHFVisual {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFRelationshipProfileHFVisual, HFRelationshipProfileHFVisual>
    for HFRelationshipProfileHFVisual
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        hf_relationship_profile_hf_visual: &[HFRelationshipProfileHFVisual],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_relationship_profile_hf_visual::table)
            .values(hf_relationship_profile_hf_visual)
            .on_conflict((
                hf_relationship_profile_hf_visual::hf_id,
                hf_relationship_profile_hf_visual::hf_id_other,
                hf_relationship_profile_hf_visual::world_id,
            ))
            .do_update()
            .set((
                hf_relationship_profile_hf_visual::meet_count
                    .eq(excluded(hf_relationship_profile_hf_visual::meet_count)),
                hf_relationship_profile_hf_visual::last_meet_year
                    .eq(excluded(hf_relationship_profile_hf_visual::last_meet_year)),
                hf_relationship_profile_hf_visual::last_meet_seconds72.eq(excluded(
                    hf_relationship_profile_hf_visual::last_meet_seconds72,
                )),
                hf_relationship_profile_hf_visual::love
                    .eq(excluded(hf_relationship_profile_hf_visual::love)),
                hf_relationship_profile_hf_visual::respect
                    .eq(excluded(hf_relationship_profile_hf_visual::respect)),
                hf_relationship_profile_hf_visual::trust
                    .eq(excluded(hf_relationship_profile_hf_visual::trust)),
                hf_relationship_profile_hf_visual::loyalty
                    .eq(excluded(hf_relationship_profile_hf_visual::loyalty)),
                hf_relationship_profile_hf_visual::fear
                    .eq(excluded(hf_relationship_profile_hf_visual::fear)),
                hf_relationship_profile_hf_visual::known_identity_id.eq(excluded(
                    hf_relationship_profile_hf_visual::known_identity_id,
                )),
                hf_relationship_profile_hf_visual::rep_friendly
                    .eq(excluded(hf_relationship_profile_hf_visual::rep_friendly)),
                hf_relationship_profile_hf_visual::rep_information_source.eq(excluded(
                    hf_relationship_profile_hf_visual::rep_information_source,
                )),
            ))
            .execute(conn)
            .expect("Error saving hf_relationship_profile_hf_visual");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        hf_relationship_profile_hf_visual: &[HFRelationshipProfileHFVisual],
    ) {
        diesel::insert_into(hf_relationship_profile_hf_visual::table)
            .values(hf_relationship_profile_hf_visual)
            .execute(conn)
            .expect("Error saving hf_relationship_profile_hf_visual");
    }

    /// Get a list of HFRelationshipProfileHFVisual from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFRelationshipProfileHFVisual>, Error> {
        use crate::schema::hf_relationship_profile_hf_visual::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_relationship_profile_hf_visual
            .limit(limit)
            .offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
                "meet_count" => meet_count,
                "last_meet_year" => last_meet_year,
                "last_meet_seconds72" => last_meet_seconds72,
                "love" => love,
                "respect" => respect,
                "trust" => trust,
                "loyalty" => loyalty,
                "fear" => fear,
                "known_identity_id" => known_identity_id,
                "rep_friendly" => rep_friendly,
                "rep_information_source" => rep_information_source,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFRelationshipProfileHFVisual>, Error> {
        use crate::schema::hf_relationship_profile_hf_visual::dsl::*;
        let query = hf_relationship_profile_hf_visual;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {return Ok(query.first::<HFRelationshipProfileHFVisual>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "hf_id_other" => "hf_id_other",
            "meet_count" => "meet_count",
            "last_meet_year" => "last_meet_year",
            "last_meet_seconds72" => "last_meet_seconds72",
            "love" => "love",
            "respect" => "respect",
            "trust" => "trust",
            "loyalty" => "loyalty",
            "fear" => "fear",
            "known_identity_id" => "known_identity_id",
            "rep_friendly" => "rep_friendly",
            "rep_information_source" => "rep_information_source",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFRelationshipProfileHFVisual],
        core_list: Vec<df_st_core::HFRelationshipProfileHFVisual>,
    ) -> Result<Vec<df_st_core::HFRelationshipProfileHFVisual>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_relationship_profile_hf_visual::dsl::*;
        let query = hf_relationship_profile_hf_visual
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "hf_id_other" => hf_id_other,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "hf_id_other" => {hf_id_other: i32},
                "meet_count" => {meet_count: Option<i32>},
                "last_meet_year" => {last_meet_year: Option<i32>},
                "last_meet_seconds72" => {last_meet_seconds72: Option<i32>},
                "love" => {love: Option<i32>},
                "respect" => {respect: Option<i32>},
                "trust" => {trust: Option<i32>},
                "loyalty" => {loyalty: Option<i32>},
                "fear" => {fear: Option<i32>},
                "known_identity_id" => {known_identity_id: Option<i32>},
                "rep_friendly" => {rep_friendly: Option<i32>},
                "rep_information_source" => {rep_information_source: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFRelationshipProfileHFVisual, df_st_core::HFRelationshipProfileHFVisual>
    for HFRelationshipProfileHFVisual
{
    fn add_missing_data(&mut self, source: &df_st_core::HFRelationshipProfileHFVisual) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.meet_count.add_missing_data(&source.meet_count);
        self.last_meet_year.add_missing_data(&source.last_meet_year);
        self.last_meet_seconds72
            .add_missing_data(&source.last_meet_seconds72);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
        self.known_identity_id
            .add_missing_data(&source.known_identity_id);
        self.rep_friendly.add_missing_data(&source.rep_friendly);
        self.rep_information_source
            .add_missing_data(&source.rep_information_source);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFRelationshipProfileHFVisual, HFRelationshipProfileHFVisual>
    for df_st_core::HFRelationshipProfileHFVisual
{
    fn add_missing_data(&mut self, source: &HFRelationshipProfileHFVisual) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.meet_count.add_missing_data(&source.meet_count);
        self.last_meet_year.add_missing_data(&source.last_meet_year);
        self.last_meet_seconds72
            .add_missing_data(&source.last_meet_seconds72);
        self.love.add_missing_data(&source.love);
        self.respect.add_missing_data(&source.respect);
        self.trust.add_missing_data(&source.trust);
        self.loyalty.add_missing_data(&source.loyalty);
        self.fear.add_missing_data(&source.fear);
        self.known_identity_id
            .add_missing_data(&source.known_identity_id);
        self.rep_friendly.add_missing_data(&source.rep_friendly);
        self.rep_information_source
            .add_missing_data(&source.rep_information_source);
    }
}

impl PartialEq<df_st_core::HFRelationshipProfileHFVisual> for HFRelationshipProfileHFVisual {
    fn eq(&self, other: &df_st_core::HFRelationshipProfileHFVisual) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq<HFRelationshipProfileHFVisual> for df_st_core::HFRelationshipProfileHFVisual {
    fn eq(&self, other: &HFRelationshipProfileHFVisual) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl PartialEq for HFRelationshipProfileHFVisual {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.hf_id_other == other.hf_id_other
    }
}

impl Hash for HFRelationshipProfileHFVisual {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.hf_id_other.hash(state);
    }
}
