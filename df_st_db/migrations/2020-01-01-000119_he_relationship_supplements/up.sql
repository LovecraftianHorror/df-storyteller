
CREATE TABLE he_relationship_supplements (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  event INTEGER,
  occasion_type INTEGER,
  site_id INTEGER,
  unk_1 INTEGER,
  
  PRIMARY KEY (id, world_id)
);