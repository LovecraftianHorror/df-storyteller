use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_occasion_schedule_features;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_occasion_schedule_features"]
#[primary_key(en_id, en_occ_id, en_occ_sch_id, local_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityOccasionScheduleFeature {
    pub en_id: i32,
    pub en_occ_id: i32,
    pub en_occ_sch_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub reference: Option<i32>,
}

impl EntityOccasionScheduleFeature {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityOccasionScheduleFeature, EntityOccasionScheduleFeature>
    for EntityOccasionScheduleFeature
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        entity_occasion_schedule_features: &[EntityOccasionScheduleFeature],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_occasion_schedule_features::table)
            .values(entity_occasion_schedule_features)
            .on_conflict((
                entity_occasion_schedule_features::en_id,
                entity_occasion_schedule_features::en_occ_id,
                entity_occasion_schedule_features::en_occ_sch_id,
                entity_occasion_schedule_features::local_id,
                entity_occasion_schedule_features::world_id,
            ))
            .do_update()
            .set((
                entity_occasion_schedule_features::type_
                    .eq(excluded(entity_occasion_schedule_features::type_)),
                entity_occasion_schedule_features::reference
                    .eq(excluded(entity_occasion_schedule_features::reference)),
            ))
            .execute(conn)
            .expect("Error saving entity_occasion_schedule_features");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        entity_occasion_schedule_features: &[EntityOccasionScheduleFeature],
    ) {
        diesel::insert_into(entity_occasion_schedule_features::table)
            .values(entity_occasion_schedule_features)
            .execute(conn)
            .expect("Error saving entity_occasion_schedule_features");
    }

    /// Get a list of EntityOccasionScheduleFeature from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<EntityOccasionScheduleFeature>, Error> {
        use crate::schema::entity_occasion_schedule_features::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_occasion_schedule_features
            .limit(limit)
            .offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "en_occ_id" => en_occ_id,
                "en_occ_sch_id" => en_occ_sch_id,
                "local_id" => local_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "en_occ_id" => en_occ_id,
                "en_occ_sch_id" => en_occ_sch_id,
                "local_id" => local_id,
                "type" => type_,
                "reference" => reference,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityOccasionScheduleFeature>, Error> {
        use crate::schema::entity_occasion_schedule_features::dsl::*;
        let query = entity_occasion_schedule_features;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "en_occ_id" => en_occ_id,
                "en_occ_sch_id" => en_occ_sch_id,
                "local_id" => local_id,
            ],
            {return Ok(query.first::<EntityOccasionScheduleFeature>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "en_occ_id" => "en_occ_id",
            "en_occ_sch_id" => "en_occ_sch_id",
            "local_id" => "local_id",
            "type" => "type",
            "reference" => "reference",
            "en_id" | _ => "en_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityOccasionScheduleFeature],
        core_list: Vec<df_st_core::EntityOccasionScheduleFeature>,
    ) -> Result<Vec<df_st_core::EntityOccasionScheduleFeature>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_occasion_schedule_features::dsl::*;
        let query = entity_occasion_schedule_features
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "en_occ_id" => en_occ_id,
                "en_occ_sch_id" => en_occ_sch_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "en_occ_id" => {en_occ_id: i32},
                "en_occ_sch_id" => {en_occ_sch_id: i32},
                "local_id" => {local_id: i32},
                "type" => {type_: Option<String>},
                "reference" => {reference: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityOccasionScheduleFeature, df_st_core::EntityOccasionScheduleFeature>
    for EntityOccasionScheduleFeature
{
    fn add_missing_data(&mut self, source: &df_st_core::EntityOccasionScheduleFeature) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.reference.add_missing_data(&source.reference);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityOccasionScheduleFeature, EntityOccasionScheduleFeature>
    for df_st_core::EntityOccasionScheduleFeature
{
    fn add_missing_data(&mut self, source: &EntityOccasionScheduleFeature) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.reference.add_missing_data(&source.reference);
    }
}

impl PartialEq for EntityOccasionScheduleFeature {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id
            && self.en_occ_id == other.en_occ_id
            && self.en_occ_sch_id == other.en_occ_sch_id
            && self.local_id == other.local_id
    }
}

impl Hash for EntityOccasionScheduleFeature {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.en_occ_id.hash(state);
        self.en_occ_sch_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq<EntityOccasionScheduleFeature> for df_st_core::EntityOccasionScheduleFeature {
    fn eq(&self, other: &EntityOccasionScheduleFeature) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::EntityOccasionScheduleFeature> for EntityOccasionScheduleFeature {
    fn eq(&self, other: &df_st_core::EntityOccasionScheduleFeature) -> bool {
        self.local_id == other.local_id
    }
}
