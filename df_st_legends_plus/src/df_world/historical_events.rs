use df_st_core::{Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct HistoricalEvent {
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,

    /// Was a number before DFHack 0.47.04-r2
    pub abuse_type: Option<String>,
    /// Was a number before DFHack 0.47.04-r2
    pub action: Option<String>,
    pub anon_3: Option<i32>,
    pub anon_4: Option<i32>,
    pub appointer_hfid: Option<i32>, // Value = -1
    pub artifact: Option<i32>,
    pub artifact_id: Option<i32>,

    pub bodies: Option<Vec<i32>>,
    pub body_part: Option<i32>,
    pub builder_hf: Option<i32>,
    pub building_type: Option<String>,    // TODO check type
    pub building_subtype: Option<String>, // TODO check type

    pub caste: Option<String>,
    pub changee: Option<i32>,
    pub changer: Option<i32>,
    pub circumstance: Option<Circumstance>,
    pub circumstance_id: Option<i32>,
    pub civ: Option<i32>,
    pub creator_hfid: Option<i32>,
    pub creator_unit_id: Option<i32>, // Value = -1

    pub death_cause: Option<String>,
    pub destination: Option<i32>,
    pub doer: Option<i32>,
    pub dye_mat: Option<String>,    // TODO check type
    pub dye_mat_index: Option<i32>, // TODO check type
    pub dye_mat_type: Option<i32>,  // TODO check type

    pub eater: Option<i32>,
    pub entity: Option<i32>,

    pub group: Option<Vec<i32>>,

    pub hf: Option<i32>,
    pub hf_target: Option<i32>,
    pub hfid: Option<i32>,
    pub histfig: Option<i32>,

    pub identity_caste: Option<String>,
    pub identity_hf: Option<i32>,
    pub identity_histfig_id: Option<i32>,
    pub identity_name: Option<String>,
    pub identity_nemesis_id: Option<i32>,
    pub identity_race: Option<String>,
    pub imp_mat: Option<String>,       // TODO check type
    pub imp_mat_index: Option<i32>,    // TODO check type
    pub imp_mat_type: Option<i32>,     // TODO check type
    pub improvement_type: Option<i32>, // TODO check type
    /// Was a number before DFHack 0.47.04-r2
    pub injury_type: Option<String>,
    pub interaction: Option<i32>,
    pub interaction_action: Option<String>,
    pub interaction_string: Option<String>,
    pub item: Option<i32>,
    pub item_mat: Option<String>,
    pub item_subtype: Option<String>,
    pub item_type: Option<String>,

    pub link_type: Option<String>,

    pub mat: Option<String>,
    pub mat_type: Option<i32>,
    pub mat_index: Option<i32>,
    pub matindex: Option<i32>,
    pub mattype: Option<i32>,

    pub new_caste: Option<String>,
    pub new_job: Option<String>,
    pub new_race: Option<String>,
    pub new_structure: Option<i32>,

    pub old_caste: Option<String>,
    pub old_job: Option<String>,
    pub old_race: Option<String>,
    pub old_structure: Option<i32>,

    pub part_lost: Option<bool>,
    pub pets: Option<Vec<String>>,
    pub pile_type: Option<String>,
    pub position: Option<String>,
    pub promise_to_hfid: Option<i32>, // Value = -1
    pub props_item_mat: Option<String>,
    pub props_item_mat_index: Option<i32>,
    pub props_item_mat_type: Option<i32>,
    pub props_item_subtype: Option<String>,
    pub props_item_type: Option<String>,
    pub props_pile_type: Option<i32>,

    pub race: Option<String>,
    pub reason: Option<Reason>, // TODO in lua. This can be an object and a value
    pub reason_id: Option<i32>,
    pub rebuild: Option<bool>,
    pub region: Option<i32>,

    pub sanctify_hf: Option<i32>,
    pub secret_text: Option<String>,
    pub shooter_artifact_id: Option<i32>,     // TODO check type
    pub shooter_item: Option<String>,         // TODO check type
    pub shooter_item_subtype: Option<String>, // TODO check type
    pub shooter_item_type: Option<String>,    // TODO check type
    pub shooter_mat: Option<String>,          // TODO check type (shooter_mattype)
    // pub shooter_matindex: Option<String>, // TODO check type (Skipped atm)
    pub site: Option<i32>,
    pub site_civ: Option<i32>,
    pub slayer_caste: Option<String>,
    pub slayer_hf: Option<i32>,
    // Similar problem to https://github.com/DFHack/dfhack/issues/1594
    pub slayer_race: Option<String>,
    pub source: Option<i32>,
    pub stash_site: Option<i32>,
    /// Was a number before DFHack 0.47.04-r2
    /// TODO: Conversion to be added.
    pub state: Option<String>,
    pub structure: Option<i32>,
    pub student: Option<i32>,

    pub target: Option<i32>,
    pub teacher: Option<i32>,
    pub theft_method: Option<String>,
    pub topic: Option<String>,
    pub tree: Option<i32>,
    pub trickster: Option<i32>,

    pub unit_id: Option<i32>,
    pub victim: Option<i32>,
    pub victim_entity: Option<i32>,
    pub victim_hf: Option<i32>,

    pub woundee: Option<i32>,
    pub woundee_caste: Option<String>,
    pub woundee_race: Option<i32>,
    pub wounder: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HistoricalEvents {
    pub historical_event: Option<Vec<HistoricalEvent>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Circumstance {
    #[serde(alias = "type")]
    pub type_: Option<String>,
    #[serde(alias = "Death")]
    pub death: Option<i32>,
    #[serde(alias = "Prayer")]
    pub prayer: Option<i32>,
    #[serde(alias = "DreamAbout")]
    pub dream_about: Option<i32>,
    #[serde(alias = "Defeated")]
    pub defeated: Option<i32>,
    #[serde(alias = "Murdered")]
    pub murdered: Option<i32>,
    #[serde(alias = "HistEventCollection")]
    pub hist_event_collection: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Reason {
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub glorify_hf: Option<i32>,
    pub artifact_is_heirloom_of_family_hfid: Option<i32>,
    pub artifact_is_symbol_of_entity_position: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEvent, HistoricalEvent> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEvent) {
        self.id.add_missing_data(&source.id);
        let mut source_type = source.type_.clone();
        if let Some(type_) = &source.type_ {
            // Map `legends_plus types` to `legends types`
            source_type = Some(match type_.as_ref() {
                "hist_figure_died" => "hf_died".to_owned(),
                "created_building" => "created_structure".to_owned(),
                "hist_figure_wounded" => "hf_wounded".to_owned(),
                "war_peace_accepted" => "peace_accepted".to_owned(),
                "war_peace_rejected" => "peace_rejected".to_owned(),
                "change_creature_type" => "changed_creature_type".to_owned(),
                "hist_figure_new_pet" => "hf_new_pet".to_owned(),
                "hf_act_on_building" => "hf_profaned_structure".to_owned(),
                // "hf_profaned_structure" => "hf_prayed_inside_structure".to_owned(),
                "entity_action" => "entity_relocate".to_owned(),
                "entity_relocate" => "entity_primary_criminals".to_owned(),
                x => x.to_owned(),
            });
        }
        self.type_.never_replace_data(&source_type);

        // Pre DFHack v0.47.04-r2 `abuse_type` was a number.
        if let Some(abuse_type) = &source.abuse_type {
            match abuse_type.parse::<i32>() {
                Ok(_number) => {
                    // TODO convert number to its string counterparts
                }
                Err(_) => {
                    self.abuse_type.add_missing_data(&source.abuse_type);
                }
            }
        }
        // Pre DFHack v0.47.04-r2 `action` was a number.
        if let Some(action) = &source.action {
            match action.parse::<i32>() {
                Ok(_number) => {
                    // TODO convert number to its string counterparts
                }
                Err(_) => {
                    self.action.add_missing_data(&source.action);
                }
            }
        }
        self.anon_3.add_missing_data(&source.anon_3);
        self.anon_4.add_missing_data(&source.anon_4);
        self.appointer_hf_id
            .add_missing_data(&source.appointer_hfid);
        // reuse artifact_id
        self.artifact_id.add_missing_data(&source.artifact);
        self.artifact_id.add_missing_data(&source.artifact_id);

        self.bodies_hf_id.add_missing_data(&source.bodies);
        self.body_part.add_missing_data(&source.body_part);
        self.builder_hf_id.add_missing_data(&source.builder_hf);
        self.building_type.add_missing_data(&source.building_type);
        self.building_subtype
            .add_missing_data(&source.building_subtype);

        self.caste.add_missing_data(&source.caste);
        self.changee_hf_id.add_missing_data(&source.changee);
        self.changer_hf_id.add_missing_data(&source.changer);
        self.circumstance_obj.add_missing_data(&source.circumstance);
        self.circumstance_id
            .add_missing_data(&source.circumstance_id);
        self.civ_id.add_missing_data(&source.civ);
        self.creator_hf_id.add_missing_data(&source.creator_hfid);
        self.creator_unit_id
            .add_missing_data(&source.creator_unit_id);

        self.death_cause.add_missing_data(&source.death_cause);
        self.destination.add_missing_data(&source.destination);
        self.doer.add_missing_data(&source.doer);
        self.dye_mat.add_missing_data(&source.dye_mat);
        self.dye_mat_index.add_missing_data(&source.dye_mat_index);
        self.dye_mat_type.add_missing_data(&source.dye_mat_type);

        self.eater_hf_id.add_missing_data(&source.entity);

        // Maps a hfid but can ben multiple
        self.groups_hf_id.add_missing_data(&source.group);

        // reuse hfid
        self.hf_id.add_missing_data(&source.hf);
        self.hf_id_target.add_missing_data(&source.hf_target);
        self.hf_id.add_missing_data(&source.hfid);
        self.hf_id.add_missing_data(&source.histfig);

        self.identity_caste.add_missing_data(&source.identity_caste);
        self.identity_hf_id.add_missing_data(&source.identity_hf);
        self.identity_hf_id
            .add_missing_data(&source.identity_histfig_id);
        self.identity_name.add_missing_data(&source.identity_name);
        self.identity_nemesis_id
            .add_missing_data(&source.identity_nemesis_id);
        self.identity_race.add_missing_data(&source.identity_race);
        self.imp_mat.add_missing_data(&source.imp_mat);
        self.imp_mat_index.add_missing_data(&source.imp_mat_index);
        self.imp_mat_type.add_missing_data(&source.imp_mat_type);
        self.improvement_type
            .add_missing_data(&source.improvement_type);
        // Pre DFHack v0.47.04-r2 `injury_type` was a number.
        if let Some(injury_type) = &source.injury_type {
            match injury_type.parse::<i32>() {
                Ok(_number) => {
                    // TODO convert number to its string counterparts
                }
                Err(_) => {
                    self.injury_type.add_missing_data(&source.injury_type);
                }
            }
        }
        //"DEITY_CURSE_WEREBEAST_10_BITE"
        // !=
        // "[I_TARGET:A:CREATURE]"
        self.interaction.add_missing_data(&source.interaction);
        self.interaction_action
            .add_missing_data(&source.interaction_action);
        // self.interaction_string.add_missing_data(&source.interaction_string);
        self.item.add_missing_data(&source.item);
        self.item_mat.add_missing_data(&source.item_mat);
        self.item_subtype.add_missing_data(&source.item_subtype);
        self.item_type.add_missing_data(&source.item_type);

        self.link_type.add_missing_data(&source.link_type);

        self.mat.add_missing_data(&source.mat);
        self.mat_type.add_missing_data(&source.mat_type);
        // reuse mat_type
        self.mat_type.add_missing_data(&source.mattype);
        self.mat_index.add_missing_data(&source.mat_index);
        // reuse mat_index
        self.mat_index.add_missing_data(&source.matindex);

        // See: https://github.com/DFHack/dfhack/issues/1594
        // self.new_caste.never_replace_data(&source.new_caste);
        self.new_job.add_missing_data(&source.new_job);
        // Change to All uppercase and do not replace
        // This is because the names of `NIGHT_CREATURE`s do change for each world.
        let mut source_new_race = source.new_race.clone();
        if let Some(new_race) = &source.new_race {
            // Replace spaces
            let mut temp_race = new_race.replace(" ", "_");
            temp_race.make_ascii_uppercase();
            source_new_race = Some(temp_race);
        }
        self.new_race_id.never_replace_data(&source_new_race);
        self.new_structure_id
            .add_missing_data(&source.new_structure);

        // See: https://github.com/DFHack/dfhack/issues/1594
        // self.old_caste.add_missing_data(&source.old_caste);
        self.old_job.add_missing_data(&source.old_job);
        // Change to All uppercase and do not replace
        // This is because the names of `NIGHT_CREATURE`s do change for each world.
        let mut source_old_race = source.old_race.clone();
        if let Some(old_race) = &source.old_race {
            // Replace spaces
            let mut temp_race = old_race.replace(" ", "_");
            temp_race.make_ascii_uppercase();
            source_old_race = Some(temp_race);
        }
        self.old_race_id.never_replace_data(&source_old_race);
        self.old_structure_id
            .add_missing_data(&source.old_structure);

        self.part_lost.add_missing_data(&source.part_lost);
        self.pets.add_missing_data(&source.pets);
        self.pile_type.add_missing_data(&source.pile_type);
        self.position.add_missing_data(&source.position);
        self.promise_to_hf_id
            .add_missing_data(&source.promise_to_hfid);
        self.props_item_mat.add_missing_data(&source.props_item_mat);
        self.props_item_mat_index
            .add_missing_data(&source.props_item_mat_index);
        self.props_item_mat_type
            .add_missing_data(&source.props_item_mat_type);
        self.props_item_subtype
            .add_missing_data(&source.props_item_subtype);
        self.props_item_type
            .add_missing_data(&source.props_item_type);
        self.props_pile_type
            .add_missing_data(&source.props_pile_type);

        self.race_id.add_missing_data(&source.race);
        self.reason_obj.add_missing_data(&source.reason);
        self.reason_id.add_missing_data(&source.reason_id);
        self.rebuild.add_missing_data(&source.rebuild);
        self.region_id.add_missing_data(&source.region);

        self.sanctify_hf_id.add_missing_data(&source.sanctify_hf);
        self.secret_text.add_missing_data(&source.secret_text);
        self.shooter_artifact_id
            .add_missing_data(&source.shooter_artifact_id);
        self.shooter_item.add_missing_data(&source.shooter_item);
        self.shooter_item_subtype
            .add_missing_data(&source.shooter_item_subtype);
        self.shooter_item_type
            .add_missing_data(&source.shooter_item_type);
        self.shooter_mat.add_missing_data(&source.shooter_mat);
        self.site_id.add_missing_data(&source.site);
        self.site_civ_id.add_missing_data(&source.site_civ);
        // See: https://github.com/DFHack/dfhack/issues/1594
        // self.slayer_caste.never_replace_data(&source.slayer_caste);
        self.slayer_hf_id.add_missing_data(&source.slayer_hf);
        // See: https://github.com/DFHack/dfhack/issues/1594
        // self.slayer_race.add_missing_data(&source.slayer_race);
        self.source.add_missing_data(&source.source);
        self.stash_site_id.add_missing_data(&source.stash_site);
        // Pre DFHack v0.47.04-r2 `state` was a number.
        if let Some(state) = &source.state {
            match state.parse::<i32>() {
                Ok(_number) => {
                    // TODO convert number to its string counterparts
                }
                Err(_) => {
                    self.state.add_missing_data(&source.state);
                }
            }
        }
        self.structure_id.add_missing_data(&source.structure);
        self.student_hf_id.add_missing_data(&source.student);

        self.target_hf_id.add_missing_data(&source.target);
        self.teacher_hf_id.add_missing_data(&source.teacher);
        self.theft_method.add_missing_data(&source.theft_method);
        self.topic.add_missing_data(&source.topic);
        self.tree.add_missing_data(&source.tree);
        self.trickster.add_missing_data(&source.trickster);

        self.unit_id.add_missing_data(&source.unit_id);
        self.victim.add_missing_data(&source.victim);
        self.victim_entity.add_missing_data(&source.victim_entity);
        self.victim_hf_id.add_missing_data(&source.victim_hf);

        self.woundee_hf_id.add_missing_data(&source.woundee);
        // See: https://github.com/DFHack/dfhack/issues/1594
        // self.woundee_caste.add_missing_data(&source.woundee_caste);
        self.woundee_race.add_missing_data(&source.woundee_race);
        self.wounder_hf_id.add_missing_data(&source.wounder);
    }
}

impl Filler<df_st_core::HECircumstance, Circumstance> for df_st_core::HECircumstance {
    fn add_missing_data(&mut self, source: &Circumstance) {
        self.type_.add_missing_data(&source.type_);
        self.death.replace_data(&source.death);
        self.prayer.add_missing_data(&source.prayer);
        self.dream_about.add_missing_data(&source.dream_about);
        self.defeated.add_missing_data(&source.defeated);
        self.murdered.add_missing_data(&source.murdered);
        self.hec_id.add_missing_data(&source.hist_event_collection);
    }
}

impl Filler<df_st_core::HEReason, Reason> for df_st_core::HEReason {
    fn add_missing_data(&mut self, source: &Reason) {
        self.type_.add_missing_data(&source.type_);
        self.glorify_hf_id.replace_data(&source.glorify_hf);
        self.artifact_is_heirloom_of_family_hf_id
            .add_missing_data(&source.artifact_is_heirloom_of_family_hfid);
        self.artifact_is_symbol_of_entity_position
            .add_missing_data(&source.artifact_is_symbol_of_entity_position);
    }
}

impl PartialEq<df_st_core::HEReason> for Reason {
    fn eq(&self, other: &df_st_core::HEReason) -> bool {
        self.type_ == other.type_
    }
}

impl std::hash::Hash for Reason {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.type_.hash(state);
    }
}

impl PartialEq<df_st_core::HECircumstance> for Circumstance {
    fn eq(&self, other: &df_st_core::HECircumstance) -> bool {
        self.type_ == other.type_
    }
}

impl std::hash::Hash for Circumstance {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.type_.hash(state);
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEvent {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::HistoricalEvent>, HistoricalEvents>
    for Vec<df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HistoricalEvents) {
        self.add_missing_data(&source.historical_event);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEvent>, HistoricalEvents>
    for IndexMap<u64, df_st_core::HistoricalEvent>
{
    fn add_missing_data(&mut self, source: &HistoricalEvents) {
        self.add_missing_data(&source.historical_event);
    }
}
