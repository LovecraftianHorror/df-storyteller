# Version support for DF Storyteller
DF Storyteller has a build in version checker. Every time DF Storyteller is opened it downloads a 
small file from one of its sources and checks if the version used is still supported and if a new 
version is available.

This system is build in to automatically deprecate old version. This way we can guarantee that you
version is not vulnerable to found security issues. We do not collect personal data. 
Server logs are disabled.

Every version of DF Storyteller will work for ~1 year. After 1 year the version will be marked as
outdated and you are forced to upgrade. This is build in for the protection of our users.[^1]
When a vulnerability is found in one or more versions of our application we will mark them as unsafe.
This will then deprecate the version earlier. In this case you are forced to update earlier.

[^1]: If you for some reason do not want to update you can compile DF Storyteller from source and
disable this protection. Although we do not advise you to do this. 
(As a programmer specialized in security this is for your own good and all applications should
have something like this build in.)

## File support for Dwarf Fortress versions

We want to support most if not all possible versions of Dwarf Fortress and DFHack exports. 
Once we have good support for both we will take a look at modded versions of Dwarf Fortress. 
Although DF Storyteller is very tolerant to what what inputs it get so it might already work 
for most modifications.

- ✓: The version is tested and should work in all cases (if not open an issue)
- ❌: The version does not work or is not tested yet. It might work, who knows.
- 🕗: The version is being worked on. Some things might work others might not. 
(please report issues to help)

Minor version usually don't change the exports. So they are grouped together when they have similar support.

| DF Version | Legends | Legends_plus | World History | World Sites and Pops |
| ---------- | ------- | ------------ | ------------- | -------------------- |
| 0.47.04    | ✓       | ✓            | ❌             | ❌                    |
| 0.47.03    | 🕗       | 🕗            | ❌             | ❌                    |
| 0.47.02    | 🕗       | 🕗            | ❌             | ❌                    |
| 0.47.01    | 🕗       | 🕗            | ❌             | ❌                    |
| 0.44.12    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.11    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.10    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.09    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.08    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.07    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.06    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.05    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.04    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.03    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.02    | ❌       | ❌            | ❌             | ❌                    |
| 0.44.01    | ❌       | ❌            | ❌             | ❌                    |
| 0.43.01-05 | ❌       | ❌            | ❌             | ❌                    |
| 0.42.01-06 | ❌       | ❌            | ❌             | ❌                    |
| 0.40.01-24 | ❌       | ❌            | ❌             | ❌                    |

#### Mods tested:

* ...Nothing tested yet. [Please take a look here](https://gitlab.com/df_storyteller/df-storyteller/-/issues/48).