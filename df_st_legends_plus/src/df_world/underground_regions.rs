use crate::deserializers::coordinates_deserializer;
use df_st_core::{Coordinate, Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct UndergroundRegion {
    pub id: i32,
    #[serde(deserialize_with = "coordinates_deserializer")]
    #[serde(default)]
    pub coords: Option<Vec<Coordinate>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct UndergroundRegions {
    pub underground_region: Option<Vec<UndergroundRegion>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::UndergroundRegion, UndergroundRegion> for df_st_core::UndergroundRegion {
    fn add_missing_data(&mut self, source: &UndergroundRegion) {
        self.id.add_missing_data(&source.id);
        self.coords.add_missing_data(&source.coords);
    }
}

impl PartialEq<df_st_core::UndergroundRegion> for UndergroundRegion {
    fn eq(&self, other: &df_st_core::UndergroundRegion) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::UndergroundRegion>, UndergroundRegions>
    for Vec<df_st_core::UndergroundRegion>
{
    fn add_missing_data(&mut self, source: &UndergroundRegions) {
        self.add_missing_data(&source.underground_region);
    }
}

impl Filler<IndexMap<u64, df_st_core::UndergroundRegion>, UndergroundRegions>
    for IndexMap<u64, df_st_core::UndergroundRegion>
{
    fn add_missing_data(&mut self, source: &UndergroundRegions) {
        self.add_missing_data(&source.underground_region);
    }
}
