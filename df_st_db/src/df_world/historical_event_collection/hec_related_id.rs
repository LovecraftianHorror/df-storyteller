use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::hec_related_ids;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hec_related_ids"]
#[primary_key(hec_id, rel_hec_id)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
pub struct HECRelatedID {
    pub hec_id: i32,
    pub rel_hec_id: i32,
    pub world_id: i32,
}

impl HECRelatedID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HECRelatedID, HECRelatedID> for HECRelatedID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hec_related_ids: &[HECRelatedID]) {
        diesel::insert_into(hec_related_ids::table)
            .values(hec_related_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hec_related_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hec_related_ids: &[HECRelatedID]) {
        diesel::insert_into(hec_related_ids::table)
            .values(hec_related_ids)
            .execute(conn)
            .expect("Error saving hec_related_ids");
    }

    /// Get a list of HECRelatedID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HECRelatedID>, Error> {
        use crate::schema::hec_related_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hec_related_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "rel_hec_id" => rel_hec_id,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HECRelatedID>, Error> {
        use crate::schema::hec_related_ids::dsl::*;
        let query = hec_related_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(query.first::<HECRelatedID>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "rel_hec_id" => "rel_hec_id",
            "hec_id" | _ => "hec_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HECRelatedID],
        core_list: Vec<HECRelatedID>,
    ) -> Result<Vec<HECRelatedID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hec_related_ids::dsl::*;
        let query = hec_related_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "rel_hec_id" => {rel_hec_id: i32},
            };},
        };
    }
}

impl PartialEq for HECRelatedID {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id && self.rel_hec_id == other.rel_hec_id
    }
}

impl Hash for HECRelatedID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
        self.rel_hec_id.hash(state);
    }
}
