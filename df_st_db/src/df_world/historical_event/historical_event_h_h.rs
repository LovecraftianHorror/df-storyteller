use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_h_h;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_h_h"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventHH {
    pub he_id: i32,
    pub world_id: i32,

    pub held_firm_in_interrogation: Option<bool>,
    pub hf_rep_1_of_2: Option<String>,
    pub hf_rep_2_of_1: Option<String>,
    pub hf_id: Option<i32>,
    pub hf_id1: Option<i32>,
    pub hf_id2: Option<i32>,
    pub hf_id_target: Option<i32>,
    pub honor_id: Option<i32>,
}

impl HistoricalEventHH {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventHH> for HistoricalEventHH {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_h_h: &[HistoricalEventHH]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_h_h::table)
            .values(historical_events_h_h)
            .on_conflict((
                historical_events_h_h::he_id,
                historical_events_h_h::world_id,
            ))
            .do_update()
            .set((
                historical_events_h_h::held_firm_in_interrogation
                    .eq(excluded(historical_events_h_h::held_firm_in_interrogation)),
                historical_events_h_h::hf_rep_1_of_2
                    .eq(excluded(historical_events_h_h::hf_rep_1_of_2)),
                historical_events_h_h::hf_rep_2_of_1
                    .eq(excluded(historical_events_h_h::hf_rep_2_of_1)),
                historical_events_h_h::hf_id.eq(excluded(historical_events_h_h::hf_id)),
                historical_events_h_h::hf_id1.eq(excluded(historical_events_h_h::hf_id1)),
                historical_events_h_h::hf_id2.eq(excluded(historical_events_h_h::hf_id2)),
                historical_events_h_h::hf_id_target
                    .eq(excluded(historical_events_h_h::hf_id_target)),
                historical_events_h_h::honor_id.eq(excluded(historical_events_h_h::honor_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_h_h");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_h_h: &[HistoricalEventHH]) {
        diesel::insert_into(historical_events_h_h::table)
            .values(historical_events_h_h)
            .execute(conn)
            .expect("Error saving historical_events_h_h");
    }

    /// Get a list of HistoricalEventHH from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventHH>, Error> {
        /*
        use crate::schema::historical_events_h_h::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_h_h.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "held_firm_in_interrogation" => held_firm_in_interrogation,
                "hf_rep_1_of_2" => hf_rep_1_of_2,
                "hf_rep_2_of_1" => hf_rep_2_of_1,
                "hf_id" => hf_id,
                "hf_id1" => hf_id1,
                "hf_id2" => hf_id2,
                "hf_id_target" => hf_id_target,
                "honor_id" => honor_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventHH>, Error> {
        use crate::schema::historical_events_h_h::dsl::*;
        let query = historical_events_h_h;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventHH>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "held_firm_in_interrogation" => "held_firm_in_interrogation",
            "hf_rep_1_of_2" => "hf_rep_1_of_2",
            "hf_rep_2_of_1" => "hf_rep_2_of_1",
            "hf_id" => "hf_id",
            "hf_id1" => "hf_id1",
            "hf_id2" => "hf_id2",
            "hf_id_target" => "hf_id_target",
            "honor_id" => "honor_id",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventHH],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_h_h::dsl::*;
        let query = historical_events_h_h.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "held_firm_in_interrogation" => {held_firm_in_interrogation: Option<bool>},
                "hf_rep_1_of_2" => {hf_rep_1_of_2: Option<String>},
                "hf_rep_2_of_1" => {hf_rep_2_of_1: Option<String>},
                "hf_id" => {hf_id: Option<i32>},
                "hf_id1" => {hf_id1: Option<i32>},
                "hf_id2" => {hf_id2: Option<i32>},
                "hf_id_target" => {hf_id_target: Option<i32>},
                "honor_id" => {honor_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventHH, df_st_core::HistoricalEvent> for HistoricalEventHH {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.held_firm_in_interrogation
            .add_missing_data(&source.held_firm_in_interrogation);
        self.hf_rep_1_of_2.add_missing_data(&source.hf_rep_1_of_2);
        self.hf_rep_2_of_1.add_missing_data(&source.hf_rep_2_of_1);
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id1.add_missing_data(&source.hf_id1);
        self.hf_id2.add_missing_data(&source.hf_id2);
        self.hf_id_target.add_missing_data(&source.hf_id_target);
        self.honor_id.add_missing_data(&source.honor_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventHH> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventHH) {
        self.id.add_missing_data(&source.he_id);
        self.held_firm_in_interrogation
            .add_missing_data(&source.held_firm_in_interrogation);
        self.hf_rep_1_of_2.add_missing_data(&source.hf_rep_1_of_2);
        self.hf_rep_2_of_1.add_missing_data(&source.hf_rep_2_of_1);
        self.hf_id.add_missing_data(&source.hf_id);
        self.hf_id1.add_missing_data(&source.hf_id1);
        self.hf_id2.add_missing_data(&source.hf_id2);
        self.hf_id_target.add_missing_data(&source.hf_id_target);
        self.honor_id.add_missing_data(&source.honor_id);
    }
}

impl PartialEq for HistoricalEventHH {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventHH {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventHH> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventHH) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventHH {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
