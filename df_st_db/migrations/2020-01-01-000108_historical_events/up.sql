
CREATE TABLE historical_events (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  type VARCHAR,
  year INTEGER,
  seconds72 INTEGER,

  PRIMARY KEY (id, world_id)
);

CREATE TABLE historical_events_a_a (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  a_support_merc_en_id INTEGER,
  account_shift INTEGER,
  acquirer_en_id INTEGER,
  acquirer_hf_id INTEGER,
  action VARCHAR,
  actor_hf_id INTEGER,
  agreement_id INTEGER,
  allotment INTEGER,
  allotment_index INTEGER,
  ally_defense_bonus INTEGER,
  appointer_hf_id INTEGER,
  arresting_en_id INTEGER,
  artifact_id INTEGER,
  attacker_civ_id INTEGER,
  attacker_general_hf_id INTEGER,
  attacker_hf_id INTEGER,
  attacker_merc_en_id INTEGER,
  abuse_type VARCHAR,
  anon_3 INTEGER,
  anon_4 INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_b_b (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  body_state VARCHAR,
  builder_hf_id INTEGER,
  building_profile_id INTEGER,
  body_part INTEGER,
  building_type VARCHAR,
  building_subtype VARCHAR,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_c_c (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  cause VARCHAR,
  changee_hf_id INTEGER,
  changer_hf_id INTEGER,
  circumstance VARCHAR,
  circumstance_id INTEGER,
  civ_entity_id INTEGER,
  civ_id INTEGER,
  claim VARCHAR,
  coconspirator_bonus INTEGER,
  coconspirator_hf_id INTEGER,
  confessed_after_apb_arrest_en_id INTEGER,
  contact_hf_id INTEGER,
  convict_is_contact BOOLEAN,
  convicted_hf_id INTEGER,
  convicter_en_id INTEGER,
  coord VARCHAR,
  corrupt_convicter_hf_id INTEGER,
  corruptor_hf_id INTEGER,
  corruptor_identity INTEGER,
  corruptor_seen_as VARCHAR,
  creator_hf_id INTEGER,
  crime VARCHAR,
  caste VARCHAR,
  creator_unit_id INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_d_d (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  d_support_merc_en_id INTEGER,
  death_penalty BOOLEAN,
  defender_civ_id INTEGER,
  defender_general_hf_id INTEGER,
  defender_merc_en_id INTEGER,
  delegated BOOLEAN,
  dest_entity_id INTEGER,
  dest_site_id INTEGER,
  dest_structure_id INTEGER,
  destroyed_structure_id INTEGER,
  destroyer_en_id INTEGER,
  detected BOOLEAN,
  did_not_reveal_all_in_interrogation BOOLEAN,
  disturbance BOOLEAN,
  dispute VARCHAR,
  doer_hf_id INTEGER,
  death_cause VARCHAR,
  destination INTEGER,
  doer INTEGER,
  dye_mat VARCHAR,
  dye_mat_index INTEGER,
  dye_mat_type INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_e_g (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  enslaved_hf_id INTEGER,
  entity_id INTEGER,
  entity_id_1 INTEGER,
  entity_id_2 INTEGER,
  exiled BOOLEAN,
  eater_hf_id INTEGER,

  failed_judgment_test BOOLEAN,
  feature_layer_id INTEGER,
  first BOOLEAN,
  fooled_hf_id INTEGER,
  form_id INTEGER,
  framer_hf_id INTEGER,
  from_original BOOLEAN,

  gambler_hf_id INTEGER,
  giver_entity_id INTEGER,
  giver_hf_id INTEGER,
  group_1_hf_id INTEGER,
  group_2_hf_id INTEGER,
  group_hf_id INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_h_h (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  held_firm_in_interrogation BOOLEAN,
  hf_rep_1_of_2 VARCHAR,
  hf_rep_2_of_1 VARCHAR,
  hf_id INTEGER,
  hf_id1 INTEGER,
  hf_id2 INTEGER,
  hf_id_target INTEGER,
  honor_id INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_i_i (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  identity_id INTEGER,
  identity_id1 INTEGER,
  identity_id2 INTEGER,
  inherited BOOLEAN,
  initiating_en_id INTEGER,
  instigator_hf_id INTEGER,
  interaction INTEGER,
  interrogator_hf_id INTEGER,
  identity_caste VARCHAR,
  identity_hf_id INTEGER,
  identity_name VARCHAR,
  identity_nemesis_id INTEGER,
  identity_race VARCHAR,
  imp_mat VARCHAR,
  imp_mat_index INTEGER,
  imp_mat_type INTEGER,
  improvement_type INTEGER,
  injury_type VARCHAR,
  -- interaction_id INTEGER,
  interaction_action VARCHAR,
  interaction_string VARCHAR,
  item INTEGER,
  item_mat VARCHAR,
  item_subtype VARCHAR,
  item_type VARCHAR,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_j_m (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  join_entity_id INTEGER,
  joined_entity_id INTEGER,
  joiner_entity_id INTEGER,

  knowledge VARCHAR,
  last_owner_hf_id INTEGER,
  law_add VARCHAR,
  law_remove VARCHAR,
  leader_hf_id INTEGER,
  link VARCHAR,
  lure_hf_id INTEGER,
  link_type VARCHAR,

  master_wc_id INTEGER,
  method VARCHAR,
  modification VARCHAR,
  modifier_hf_id INTEGER,
  mood VARCHAR,
  moved_to_site_id INTEGER,
  mat VARCHAR,
  mat_type INTEGER,
  mat_index INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_n_o (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  name_only BOOLEAN,
  new_ab_id INTEGER,
  new_account INTEGER,
  new_caste INTEGER,
  new_equipment_level INTEGER,
  new_leader_hf_id INTEGER,
  new_race_id VARCHAR,
  new_site_civ_id INTEGER,
  new_job VARCHAR,
  new_structure_id INTEGER,

  occasion_id INTEGER,
  old_ab_id INTEGER,
  old_account INTEGER,
  old_caste INTEGER,
  old_race_id VARCHAR,
  overthrown_hf_id INTEGER,
  old_job VARCHAR,
  old_structure_id INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_p_p (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  partial_incorporation BOOLEAN,
  payer_entity_id INTEGER,
  payer_hf_id INTEGER,
  persecutor_en_id INTEGER,
  persecutor_hf_id INTEGER,
  plotter_hf_id INTEGER,
  pop_fl_id INTEGER,
  pop_number_moved INTEGER,
  pop_race INTEGER,
  pop_sr_id INTEGER,
  pos_taker_hf_id INTEGER,
  position_id INTEGER,
  position_profile_id INTEGER,
  prison_months INTEGER,
  production_zone_id INTEGER,
  promise_to_hf_id INTEGER,
  property_confiscated_from_hf_id INTEGER,
  purchased_unowned BOOLEAN,
  part_lost BOOLEAN,
  pile_type VARCHAR,
  position VARCHAR,
  props_item_mat VARCHAR,
  props_item_mat_index INTEGER,
  props_item_mat_type INTEGER,
  props_item_subtype VARCHAR,
  props_item_type VARCHAR,
  props_pile_type INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_q_r (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  quality INTEGER,
  ransomed_hf_id INTEGER,
  ransomer_hf_id INTEGER,
  reason INTEGER,
  reason_id INTEGER,
  rebuilt_ruined BOOLEAN,
  receiver_entity_id INTEGER,
  receiver_hf_id INTEGER,
  relationship VARCHAR,
  relevant_entity_id INTEGER,
  relevant_id_for_method INTEGER,
  relevant_position_profile_id INTEGER,
  religion_id INTEGER,
  resident_civ_id INTEGER,
  return BOOLEAN,
  race_id VARCHAR,
  rebuild BOOLEAN,
  region_id INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_s_s1 (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  saboteur_hf_id INTEGER,
  sanctify_hf_id INTEGER,
  schedule_id INTEGER,
  season VARCHAR,
  secret_goal VARCHAR,
  seeker_hf_id INTEGER,
  seller_hf_id INTEGER,
  shrine_amount_destroyed INTEGER,
  site_civ_id INTEGER,
  site_entity_id INTEGER,
  site_hf_id INTEGER,
  site_id INTEGER,
  site_id_1 INTEGER,
  site_id_2 INTEGER,
  site_property_id INTEGER,
  situation VARCHAR,
  slayer_caste INTEGER,
  slayer_hf_id INTEGER,
  slayer_item_id INTEGER,
  slayer_race VARCHAR,
  slayer_shooter_item_id INTEGER,
  snatcher_hf_id INTEGER,
  source_entity_id INTEGER,
  source_site_id INTEGER,
  source_structure_id INTEGER,
  stash_site_id INTEGER,
  speaker_hf_id INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_s_s2 (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  state VARCHAR,
  structure_id INTEGER,
  student_hf_id INTEGER,
  subregion_id INTEGER,
  subtype VARCHAR,
  successful BOOLEAN,
  surveiled_coconspirator BOOLEAN,
  surveiled_contact BOOLEAN,
  surveiled_convicted BOOLEAN,
  surveiled_target BOOLEAN,
  secret_text VARCHAR,
  shooter_artifact_id INTEGER,
  shooter_item VARCHAR,
  shooter_item_subtype VARCHAR,
  shooter_item_type VARCHAR,
  shooter_mat VARCHAR,
  source INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_t_t (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  target_en_id INTEGER,
  target_hf_id INTEGER,
  target_identity INTEGER,
  target_seen_as VARCHAR,
  teacher_hf_id INTEGER,
  theft_method VARCHAR,
  top_facet VARCHAR,
  top_facet_modifier INTEGER,
  top_facet_rating INTEGER,
  top_relationship_factor VARCHAR,
  top_relationship_modifier INTEGER,
  top_relationship_rating INTEGER,
  top_value VARCHAR,
  top_value_modifier INTEGER,
  top_value_rating INTEGER,
  topic VARCHAR,
  trader_entity_id INTEGER,
  trader_hf_id INTEGER,
  trickster_hf_id INTEGER,
  tree INTEGER,
  trickster INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE historical_events_u_w (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  unit_id INTEGER,
  unit_type VARCHAR,
  victim INTEGER,
  victim_entity INTEGER,
  victim_hf_id INTEGER,

  wanted_and_recognized BOOLEAN,
  was_torture BOOLEAN,
  wc_id INTEGER,
  winner_hf_id INTEGER,
  woundee_hf_id INTEGER,
  wounder_hf_id INTEGER,
  wrongful_conviction BOOLEAN,
  woundee_caste INTEGER,
  woundee_race INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);


CREATE TABLE he_circumstances (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  type VARCHAR,
  death INTEGER,
  prayer INTEGER,
  dream_about INTEGER,
  defeated INTEGER,
  murdered INTEGER,
  hec_id INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_reasons (
  he_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  type VARCHAR,
  glorify_hf_id INTEGER,
  artifact_is_heirloom_of_family_hf_id INTEGER,
  artifact_is_symbol_of_entity_position INTEGER,

  PRIMARY KEY (he_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_bodies_hf_ids (
  he_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, hf_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_competitor_hf_ids (
  he_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, hf_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_conspirator_hf_ids (
  he_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, hf_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_expelled_creatures (
  he_id INTEGER NOT NULL,
  creature_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, creature_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_expelled_hf_ids (
  he_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, hf_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_expelled_numbers (
  he_id INTEGER NOT NULL,
  number INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, number, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);

CREATE TABLE he_expelled_pop_ids (
  he_id INTEGER NOT NULL,
  pop_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, pop_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
  -- FOREIGN KEY (hf_id) REFERENCES rectangle (id)
);

CREATE TABLE he_groups_hf_ids (
  he_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, hf_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
  -- FOREIGN KEY (hf_id) REFERENCES rectangle (id)
);

CREATE TABLE he_implicated_hf_ids (
  he_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, hf_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
  -- FOREIGN KEY (hf_id) REFERENCES rectangle (id)
);

CREATE TABLE he_joining_en_ids (
  he_id INTEGER NOT NULL,
  en_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (he_id, en_id, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
  -- FOREIGN KEY (en_id) REFERENCES rectangle (id)
);

CREATE TABLE he_pets (
  he_id INTEGER NOT NULL,
  pet VARCHAR NOT NULL,
  world_id INTEGER NOT NULL,
  
  PRIMARY KEY (he_id, pet, world_id),
  FOREIGN KEY (he_id, world_id) REFERENCES historical_events (id, world_id)
);