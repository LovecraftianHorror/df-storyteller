use df_st_core::{Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct Site {
    pub id: i32,
    pub civ_id: Option<i32>,
    pub cur_owner_id: Option<i32>,
    pub structures: Option<Structures>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Sites {
    pub site: Option<Vec<Site>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Structure {
    /// Local_id in site
    pub id: i32,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    pub name: Option<String>,
    pub name2: Option<String>,
    // temple
    pub deity_type: Option<i32>,
    pub deity: Option<i32>,
    pub religion: Option<i32>,
    // dungeon
    pub dungeon_type: Option<i32>,
    // inhabitants
    pub inhabitant: Option<Vec<i32>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct Structures {
    pub structure: Option<Vec<Structure>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Site, Site> for df_st_core::Site {
    fn add_missing_data(&mut self, source: &Site) {
        self.id.add_missing_data(&source.id);
        if let Some(civ_id) = &source.civ_id {
            if civ_id >= &0 {
                // Do not add minus values for this
                self.civ_id.add_missing_data(&source.civ_id);
            }
        }
        if let Some(cur_owner_id) = &source.cur_owner_id {
            if cur_owner_id >= &0 {
                // Do not add minus values for this
                self.cur_owner_id.add_missing_data(&source.cur_owner_id);
            }
        }
        if let Some(structures) = &source.structures {
            self.structures.add_missing_data(&structures.structure);
        }
    }
}

impl Filler<df_st_core::Structure, Structure> for df_st_core::Structure {
    fn add_missing_data(&mut self, source: &Structure) {
        self.local_id.add_missing_data(&source.id);
        // Replace the underscore with a space
        // "mead_hall" -> "mead hall"
        let mut source_type = source.type_.clone();
        if let Some(type_) = &source.type_ {
            source_type = Some(type_.replace("_", " "));
        }
        self.type_.add_missing_data(&source_type);
        // Replace here, name contains uppercase letters.
        self.name.replace_data(&source.name);
        self.name2.add_missing_data(&source.name2);
        self.deity_type.add_missing_data(&source.deity_type);
        self.deity_id.add_missing_data(&source.deity);
        self.religion_id.add_missing_data(&source.religion);
        self.dungeon_type.add_missing_data(&source.dungeon_type);
        self.inhabitant.add_missing_data(&source.inhabitant);
    }
}

impl PartialEq<df_st_core::Site> for Site {
    fn eq(&self, other: &df_st_core::Site) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::Structure> for Structure {
    fn eq(&self, other: &df_st_core::Structure) -> bool {
        self.id == other.local_id
    }
}

impl Filler<Vec<df_st_core::Site>, Sites> for Vec<df_st_core::Site> {
    fn add_missing_data(&mut self, source: &Sites) {
        self.add_missing_data(&source.site);
    }
}

impl Filler<IndexMap<u64, df_st_core::Site>, Sites> for IndexMap<u64, df_st_core::Site> {
    fn add_missing_data(&mut self, source: &Sites) {
        self.add_missing_data(&source.site);
    }
}
