use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::hec_noncom_hf_ids;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hec_noncom_hf_ids"]
#[primary_key(hec_id, noncom_hf_id)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
pub struct HECNoncomHFID {
    pub hec_id: i32,
    pub noncom_hf_id: i32,
    pub world_id: i32,
}

impl HECNoncomHFID {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HECNoncomHFID, HECNoncomHFID> for HECNoncomHFID {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hec_noncom_hf_ids: &[HECNoncomHFID]) {
        diesel::insert_into(hec_noncom_hf_ids::table)
            .values(hec_noncom_hf_ids)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hec_noncom_hf_ids");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hec_noncom_hf_ids: &[HECNoncomHFID]) {
        diesel::insert_into(hec_noncom_hf_ids::table)
            .values(hec_noncom_hf_ids)
            .execute(conn)
            .expect("Error saving hec_noncom_hf_ids");
    }

    /// Get a list of HECNoncomHFID from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HECNoncomHFID>, Error> {
        use crate::schema::hec_noncom_hf_ids::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hec_noncom_hf_ids.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "noncom_hf_id" => noncom_hf_id,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HECNoncomHFID>, Error> {
        use crate::schema::hec_noncom_hf_ids::dsl::*;
        let query = hec_noncom_hf_ids;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(query.first::<HECNoncomHFID>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "noncom_hf_id" => "noncom_hf_id",
            "hec_id" | _ => "hec_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HECNoncomHFID],
        core_list: Vec<HECNoncomHFID>,
    ) -> Result<Vec<HECNoncomHFID>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hec_noncom_hf_ids::dsl::*;
        let query = hec_noncom_hf_ids.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "noncom_hf_id" => {noncom_hf_id: i32},
            };},
        };
    }
}

impl PartialEq for HECNoncomHFID {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id && self.noncom_hf_id == other.noncom_hf_id
    }
}

impl Hash for HECNoncomHFID {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
        self.noncom_hf_id.hash(state);
    }
}
