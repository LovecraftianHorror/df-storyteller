use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::Coordinate;
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An Landmass is an interconnected piece of land in the world.
/// It is usually surrounded by water or the map borders.
/// Island are also considered a landmass.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct Landmass {
    /// Identifier for the mountain peak.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub name: Option<String>,
    // top left?
    pub coord_1: Option<Coordinate>, //TODO rename depending on corner
    // bottom right?
    pub coord_2: Option<Coordinate>, //TODO rename depending on corner
}

impl Landmass {
    pub fn new() -> Self {
        Self::default()
    }
}

impl CreateNew for Landmass {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for Landmass {
    fn example() -> Self {
        Self::default()
    }
}
