#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use rocket::config::{Config, Environment, Limits, LoggingLevel, Value};
use std::collections::HashMap;

use df_st_core::config::{get_database_url, RootConfig};

/// Read config file and create Rocket Config Object
pub fn set_server_config(server_config: &RootConfig) -> Config {
    // This replaces the Rocket.toml file.
    // See: https://api.rocket.rs/v0.4/rocket/config/struct.ConfigBuilder.html
    // Set Database url
    let mut df_st_database: HashMap<String, Value> = HashMap::new();
    if let Some(url) = get_database_url(&server_config) {
        df_st_database.insert("url".to_string(), Value::String(url));
    }
    // Set Pool Size
    if let Some(pool_size) = &server_config.database.pool_size {
        df_st_database.insert("pool_size".to_string(), Value::Integer(*pool_size));
    }
    let mut database: HashMap<String, HashMap<String, Value>> = HashMap::new();
    database.insert("df_st_database".to_string(), df_st_database);
    // Clone it so we don't have to deal with all the references below
    let server_conf = server_config.server.clone();

    // Start creating config
    let mut config = Config::build(Environment::Staging)
        // "0.0.0.0" Allows all ips (so anything that can reach this computer)
        // If behind NAT this means that port forwarding COULD be done
        // Although exposing this is a very bad idea!
        // otherwise use "localhost"
        .address(
            server_conf
                .address
                .unwrap_or_else(|| "127.0.0.1".to_string()),
        )
        .port(server_conf.port.unwrap_or(20350));
    // Set Workers
    if server_conf.workers.is_some() {
        config = config.workers(server_conf.workers.unwrap_or(16));
    }
    // Set Keep Alive duration
    config = config.keep_alive(server_conf.keep_alive.unwrap_or(5));
    // Set logging Level for Rocket (not DF Storyteller)
    config = config.log_level(match server_conf.log_level.unwrap_or_default().as_ref() {
        "Critical" => LoggingLevel::Critical,
        "Normal" => LoggingLevel::Normal,
        "Debug" => LoggingLevel::Debug,
        "Off" => LoggingLevel::Off,
        _ => LoggingLevel::Normal,
    });
    // Set secret key for cookies
    if server_conf.secret_key.is_some() {
        config = config.secret_key(server_conf.secret_key.unwrap());
    }
    // Set TLS settings
    if let Some(tls_conf) = server_conf.tls {
        config = config.tls(tls_conf.certs, tls_conf.private_key);
    }
    // Set Limits: Max size accepted for data type.
    if let Some(limits) = server_conf.limits {
        let mut limit = Limits::new();
        for (key, value) in limits.values {
            limit = limit.limit(key, value);
        }
        config = config.limits(limit);
    }
    // Set database settings from above.
    config = config.extra("databases", database);
    config.finalize().unwrap()
}

#[derive(Clone, Debug, Default)]
pub struct ServerInfo {
    pub page_max_limit: u32,
    pub default_max_page_size: u32,
    pub base_url: String,
    pub world_id: i32,
}

impl ServerInfo {
    pub fn new(server_config: &RootConfig, api_path: &str, world_id: u32) -> Self {
        let mut server_info = ServerInfo::default();
        let server_conf = server_config.server.clone();

        let protocol = match server_conf.tls {
            // lets asume that when a tls configuration is set it will work.
            Some(_) => "https".to_owned(),
            None => "http".to_owned(),
        };

        server_info.base_url = format!(
            "{}://{}:{}{}",
            protocol,
            server_config.local_address.trim_matches('/'),
            server_conf.port.unwrap_or(20350),
            api_path.trim_end_matches('/'),
        );
        server_info.page_max_limit = 500;
        server_info.default_max_page_size = 100;
        server_info.world_id = world_id as i32;
        server_info
    }
}
