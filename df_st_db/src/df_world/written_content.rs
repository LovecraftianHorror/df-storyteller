use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::written_contents;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use std::collections::HashMap;

mod wc_reference;
mod wc_style;
pub use wc_reference::WCReference;
pub use wc_style::WCStyle;

#[allow(unused_imports)]
use failure::Error;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "written_contents"]
pub struct WrittenContent {
    pub id: i32,
    pub world_id: i32,
    pub title: Option<String>,
    pub author_hf_id: Option<i32>,
    pub author_roll: Option<i32>,
    pub form: Option<String>,
    pub form_id: Option<i32>,
    pub page_start: Option<i32>,
    pub page_end: Option<i32>,
    pub type_: Option<String>,
    pub author: Option<i32>,
}

impl WrittenContent {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::WrittenContent, WrittenContent> for WrittenContent {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, written_contents: &[WrittenContent]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(written_contents::table)
            .values(written_contents)
            .on_conflict((written_contents::id, written_contents::world_id))
            .do_update()
            .set((
                written_contents::title.eq(excluded(written_contents::title)),
                written_contents::author_hf_id.eq(excluded(written_contents::author_hf_id)),
                written_contents::author_roll.eq(excluded(written_contents::author_roll)),
                written_contents::form.eq(excluded(written_contents::form)),
                written_contents::form_id.eq(excluded(written_contents::form_id)),
                written_contents::page_start.eq(excluded(written_contents::page_start)),
                written_contents::page_end.eq(excluded(written_contents::page_end)),
                written_contents::type_.eq(excluded(written_contents::type_)),
                written_contents::author.eq(excluded(written_contents::author)),
            ))
            .execute(conn)
            .expect("Error saving written_contents");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, written_contents: &[WrittenContent]) {
        diesel::insert_into(written_contents::table)
            .values(written_contents)
            .execute(conn)
            .expect("Error saving written_contents");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<WrittenContent>, Error> {
        use crate::schema::written_contents::dsl::*;
        let query = written_contents;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        return Ok(query.first::<WrittenContent>(conn).optional()?);
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<WrittenContent>, Error> {
        use crate::schema::written_contents::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = written_contents.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "title" => title,
                "author_hf_id" => author_hf_id,
                "author_roll" => author_roll,
                "form" => form,
                "form_id" => form_id,
                "page_start" => page_start,
                "page_end" => page_end,
                "type" => type_,
                "author" => author,
            });},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "title" => "title",
            "author_hf_id" => "author_hf_id",
            "author_roll" => "author_roll",
            "form" => "form",
            "form_id" => "form_id",
            "page_start" => "page_start",
            "page_end" => "page_end",
            "type" => "type",
            "author" => "author",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        conn: &DbConnection,
        db_list: &[WrittenContent],
        _core_list: Vec<df_st_core::WrittenContent>,
    ) -> Result<Vec<df_st_core::WrittenContent>, Error> {
        let world_id = match db_list.first() {
            Some(x) => x.world_id,
            None => 0,
        };
        // Add Style
        let style_list = WCStyle::belonging_to(db_list)
            .filter(crate::schema::wc_style::world_id.eq(world_id))
            .load::<WCStyle>(conn)?
            .grouped_by(db_list);
        // Add Reference
        let reference_list = WCReference::belonging_to(db_list)
            .filter(crate::schema::wc_reference::world_id.eq(world_id))
            .load::<WCReference>(conn)?
            .grouped_by(db_list);

        let mut core_list: Vec<df_st_core::WrittenContent> = Vec::new();
        for (index, wc) in db_list.into_iter().enumerate() {
            let mut core_wc = df_st_core::WrittenContent::new();
            core_wc.add_missing_data(wc);
            // Add Style
            let style = style_list.get(index).unwrap();
            core_wc.style.add_missing_data(style);
            // Add Reference
            let reference = reference_list.get(index).unwrap();
            core_wc.reference.add_missing_data(reference);

            core_list.push(core_wc);
        }
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::written_contents::dsl::*;
        let query = written_contents.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "title" => {title: Option<String>},
                "author_hf_id" => {author_hf_id: Option<i32>},
                "author_roll" => {author_roll: Option<i32>},
                "form" => {form: Option<String>},
                "form_id" => {form_id: Option<i32>},
                "page_start" => {page_start: Option<i32>},
                "page_end" => {page_end: Option<i32>},
                "type" => {type_: Option<String>},
                "author" => {author: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<WrittenContent, df_st_core::WrittenContent> for WrittenContent {
    fn add_missing_data(&mut self, source: &df_st_core::WrittenContent) {
        self.id.add_missing_data(&source.id);
        self.title.add_missing_data(&source.title);
        self.author_hf_id.add_missing_data(&source.author_hf_id);
        self.author_roll.add_missing_data(&source.author_roll);
        self.form.add_missing_data(&source.form);
        self.form_id.add_missing_data(&source.form_id);
        self.page_start.add_missing_data(&source.page_start);
        self.page_end.add_missing_data(&source.page_end);
        self.type_.add_missing_data(&source.type_);
        self.author.add_missing_data(&source.author);
    }
}

/// From DB to Core
impl Filler<df_st_core::WrittenContent, WrittenContent> for df_st_core::WrittenContent {
    fn add_missing_data(&mut self, source: &WrittenContent) {
        self.id.add_missing_data(&source.id);
        self.title.add_missing_data(&source.title);
        self.author_hf_id.add_missing_data(&source.author_hf_id);
        self.author_roll.add_missing_data(&source.author_roll);
        self.form.add_missing_data(&source.form);
        self.form_id.add_missing_data(&source.form_id);
        self.page_start.add_missing_data(&source.page_start);
        self.page_end.add_missing_data(&source.page_end);
        self.type_.add_missing_data(&source.type_);
        self.author.add_missing_data(&source.author);
    }
}

impl PartialEq<WrittenContent> for df_st_core::WrittenContent {
    fn eq(&self, other: &WrittenContent) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::WrittenContent> for WrittenContent {
    fn eq(&self, other: &df_st_core::WrittenContent) -> bool {
        self.id == other.id
    }
}
