use crate::api_errors::{APIErrorNoContent, APIErrorNotModified};
use crate::api_objects::{
    ApiCountPage, ApiItem, ApiMinimalPagination, ApiObject, ApiPage, ApiPagination,
};
use crate::DfStDatabase;
use crate::ServerInfo;
use df_st_core::item_count::ItemCount;
use df_st_db::{id_filter, DBObject};
use rocket::{get, State};
use rocket_contrib::json::Json;
use rocket_okapi_codegen::openapi;

impl ApiObject for df_st_core::Artifact {
    fn get_type() -> String {
        return "artifact".to_owned();
    }
    fn get_item_link(&self, base_url: &String) -> String {
        format!("{}/artifacts/{}", base_url, self.id)
    }
    fn get_page_link(base_url: &String) -> String {
        format!("{}/artifacts", base_url)
    }
    fn get_count_link(base_url: &String) -> String {
        format!("{}/artifacts/count", base_url)
    }
}

/// Request a `Artifact` by id.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/artifacts/<artifact_id>")]
pub fn get_artifact(
    conn: DfStDatabase,
    artifact_id: i32,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::Artifact>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::Artifact>::new(&server_info);
    let item_search = df_st_db::Artifact::get_from_db(
        &*conn,
        id_filter!["id" => artifact_id, "world_id" => server_info.world_id],
        true,
    )?;
    if let Some(item) = item_search {
        api_page.wrap(item);
        return Ok(Json(api_page));
    }
    Err(APIErrorNoContent::new())
}

/// Request a list of all `Artifacts` in the world.
/// List is not ordered and some id's might be missing!
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/artifacts?<pagination..>")]
pub fn list_artifacts(
    conn: DfStDatabase,
    pagination: ApiPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::Artifact>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::Artifact>::new(&pagination, &server_info);
    api_page.order_by = df_st_db::Artifact::match_field_by_opt(api_page.order_by);
    api_page.total_item_count = df_st_db::Artifact::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        1,
        None,
    )
    .unwrap() // TODO These errors should be better handled
    .get(0)
    .unwrap()
    .count;

    let result_list = df_st_db::Artifact::get_list_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.get_db_order(),
        api_page.order_by.clone(),
        true,
    )
    .unwrap(); // TODO These errors should be better handled

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a counts about `Artifact` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/artifacts/count?<group_by>&<pagination..>")]
pub fn get_artifact_count(
    conn: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiMinimalPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::Artifact>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::Artifact>::new(&pagination, &server_info);
    api_page.group_by = df_st_db::Artifact::match_field_by_opt(group_by);
    api_page.total_item_count = df_st_db::Artifact::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        u32::MAX,
        api_page.group_by.clone(),
    )
    .unwrap() // TODO These errors should be better handled
    .len() as u32;

    let result_list = df_st_db::Artifact::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.group_by.clone(),
    )
    .unwrap(); // TODO These errors should be better handled

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}
