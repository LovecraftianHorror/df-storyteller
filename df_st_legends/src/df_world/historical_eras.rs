use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HistoricalEra {
    pub name: Option<String>,
    pub start_year: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct HistoricalEras {
    pub historical_era: Option<Vec<HistoricalEra>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalEra, HistoricalEra> for df_st_core::HistoricalEra {
    fn add_missing_data(&mut self, source: &HistoricalEra) {
        // self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.start_year.add_missing_data(&source.start_year);
    }
}

impl PartialEq<df_st_core::HistoricalEra> for HistoricalEra {
    fn eq(&self, other: &df_st_core::HistoricalEra) -> bool {
        self.name == other.name
    }
}

impl std::hash::Hash for HistoricalEra {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl Filler<Vec<df_st_core::HistoricalEra>, HistoricalEras> for Vec<df_st_core::HistoricalEra> {
    fn add_missing_data(&mut self, source: &HistoricalEras) {
        self.add_missing_data(&source.historical_era);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalEra>, HistoricalEras>
    for IndexMap<u64, df_st_core::HistoricalEra>
{
    fn add_missing_data(&mut self, source: &HistoricalEras) {
        self.add_missing_data(&source.historical_era);
    }
}
