use df_st_core::config::RootConfig;
use df_st_db::*;

pub fn create_world(config: &RootConfig) {
    let mut world = DBDFWorld::new();

    world.artifacts.insert(
        0,
        Artifact {
            id: 0,
            name: Some("the urns of oblivion".to_owned()),
            item_name: Some("uja zitha".to_owned()),
            item_type: Some("slab".to_owned()),
            item_description: Some("the secrets of life and death".to_owned()),
            item_mat: Some("gabbro".to_owned()),
            ..Default::default()
        },
    );
    world.artifacts.insert(
        1,
        Artifact {
            id: 1,
            name: Some("the gutter, my life".to_owned()),
            item_name: Some("the gutter, my life".to_owned()),
            item_type: Some("book".to_owned()),
            item_page_number: Some(35),
            item_mat: Some("marble".to_owned()),
            ..Default::default()
        },
    );
    world.artifacts.insert(
        2,
        Artifact {
            id: 2,
            name: Some("meals ever onward".to_owned()),
            item_name: Some("meals ever onward".to_owned()),
            item_type: Some("tool".to_owned()),
            item_subtype: Some("scroll".to_owned()),
            item_mat: Some("giant armadillo parchment".to_owned()),
            ..Default::default()
        },
    );
    world.artifacts.insert(
        3,
        Artifact {
            id: 3,
            name: Some("meditations on the goblin".to_owned()),
            item_name: Some("meditations on the goblin".to_owned()),
            item_type: Some("book".to_owned()),
            item_page_number: Some(27),
            item_mat: Some("rhodolite".to_owned()),
            ..Default::default()
        },
    );

    world.set_world_id(1);
    let pool = df_st_db::establish_connection(&config).expect("Failed to connect to database.");
    let conn = pool.get().expect("Couldn't get db connection from pool.");
    world.insert_into_db(&conn, None);
}
