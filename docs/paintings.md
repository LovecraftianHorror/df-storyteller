# List of paintings / visualizers

DF Storyteller relays on other application that visualize that stories. You
might say they paint a picture for you. ;) Here you find a list of applications
that use DF Storyteller (Sorted alphabetical):

| Name                             | Windows | Linux   | OSX     | DF Storyteller version |
| -------------------------------- | ------- | ------- | ------- | ---------------------- |
| This is a dummy application name | **Yes** | **Yes** | **Yes** | v 1.0.0                |
| Placeholder name                 | No      | **Yes** | No      | v 0.1.0                |
| ...                              |         |         |         |                        |

This list was last updated on 2020/05/07.

If you want to add your application to this list, please submit a 
[new issue](https://gitlab.com/df_storyteller/df-storyteller/-/issues/new) 
with the `add_a_painting` template on our git repo with all the information. 
Your request will be reviewed and added to the list in an upcoming release.

## Requirements for this list

There are a few requirements before you can be added to the list.

1. Your application needs to use one of the recent (non-modded) builds of DF
Storyteller.
2. Your application has to be stable on at least one platform (Windows, Linux,
OSX). This means that is does not crash frequently or has a lot of unwanted behavior.
3. Your application can not collect, share or track users and/or there data
without there VERY clear consent and follow
[GDPR  rules](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)
for all users around the world, users can opt-out of this without being denied access.
4. Your application has to be available to the public as an executable or service.
5. You need to provide instructions on how to install/use the application.
6. Your application can not contain or lead to malware or other malicious applications.

This list is non extensive and it is up to the DF Storyteller developers to
reject/remove applications from the list because of these, or other reasons.
