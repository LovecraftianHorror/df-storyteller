use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq, JsonSchema)]
pub struct WorldMapImages {
    pub detailed: Option<MapImage>,
    pub world_map: Option<MapImage>,
    pub biome: Option<MapImage>,
    pub diplomacy: Option<MapImage>,
    pub drainage: Option<MapImage>,
    pub elevation: Option<MapImage>,
    pub elevation_water: Option<MapImage>,
    pub evil: Option<MapImage>,
    pub hydrologic: Option<MapImage>,
    pub nobility: Option<MapImage>,
    pub rainfall: Option<MapImage>,
    pub salinity: Option<MapImage>,
    pub savagery: Option<MapImage>,
    // `str`
    pub cadaster: Option<MapImage>,
    pub temperature: Option<MapImage>,
    pub trade: Option<MapImage>,
    pub vegetation: Option<MapImage>,
    pub volcanism: Option<MapImage>,
}

#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
)]
pub struct MapImage {
    pub id: i32,
    pub name: String,
    #[serde(skip)]
    pub data: Vec<u8>,
    pub format: String,
}

impl WorldMapImages {
    /// Create a new world map image collection
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for WorldMapImages {
    fn example() -> Self {
        Self::default()
    }
}

impl MapImage {
    /// Create a new world map image
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for MapImage {
    fn example() -> Self {
        Self::default()
    }
}
