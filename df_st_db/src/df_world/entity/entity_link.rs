use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_links;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_links"]
#[primary_key(en_id, local_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityLink {
    pub en_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub target: Option<i32>,
    pub strength: Option<i32>,
}

impl EntityLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityLink, EntityLink> for EntityLink {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_links: &[EntityLink]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_links::table)
            .values(entity_links)
            .on_conflict((
                entity_links::en_id,
                entity_links::local_id,
                entity_links::world_id,
            ))
            .do_update()
            .set((
                entity_links::type_.eq(excluded(entity_links::type_)),
                entity_links::target.eq(excluded(entity_links::target)),
                entity_links::strength.eq(excluded(entity_links::strength)),
            ))
            .execute(conn)
            .expect("Error saving entity_links");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_links: &[EntityLink]) {
        diesel::insert_into(entity_links::table)
            .values(entity_links)
            .execute(conn)
            .expect("Error saving entity_links");
    }

    /// Get a list of EntityLink from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<EntityLink>, Error> {
        use crate::schema::entity_links::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_links.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "local_id" => local_id,
                "type" => type_,
                "target" => target,
                "strength" => strength,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityLink>, Error> {
        use crate::schema::entity_links::dsl::*;
        let query = entity_links;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {return Ok(query.first::<EntityLink>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "local_id" => "local_id",
            "type" => "type",
            "target" => "target",
            "strength" => "strength",
            "en_id" | _ => "en_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityLink],
        core_list: Vec<df_st_core::EntityLink>,
    ) -> Result<Vec<df_st_core::EntityLink>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_links::dsl::*;
        let query = entity_links.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "local_id" => {local_id: i32},
                "type" => {type_: Option<String>},
                "target" => {target: Option<i32>},
                "strength" => {strength: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityLink, df_st_core::EntityLink> for EntityLink {
    fn add_missing_data(&mut self, source: &df_st_core::EntityLink) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.target.add_missing_data(&source.target);
        self.strength.add_missing_data(&source.strength);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityLink, EntityLink> for df_st_core::EntityLink {
    fn add_missing_data(&mut self, source: &EntityLink) {
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.target.add_missing_data(&source.target);
        self.strength.add_missing_data(&source.strength);
    }
}

impl PartialEq for EntityLink {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.local_id == other.local_id
    }
}

impl Hash for EntityLink {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq<EntityLink> for df_st_core::EntityLink {
    fn eq(&self, other: &EntityLink) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::EntityLink> for EntityLink {
    fn eq(&self, other: &df_st_core::EntityLink) -> bool {
        self.local_id == other.local_id
    }
}
