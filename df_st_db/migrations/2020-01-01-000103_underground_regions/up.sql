
CREATE TABLE underground_regions (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  type VARCHAR,
  depth INTEGER,
  
  PRIMARY KEY (id, world_id)
);

-- ALTER TABLE coordinates
-- ADD CONSTRAINT underground_regions_coordinates 
-- FOREIGN KEY (underground_region_id, world_id) REFERENCES underground_regions (id, world_id);
