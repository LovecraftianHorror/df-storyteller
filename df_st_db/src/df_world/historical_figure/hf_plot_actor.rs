use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_plot_actors;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_plot_actors"]
#[primary_key(hf_id, local_id, actor_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFPlotActor {
    pub hf_id: i32,
    pub local_id: i32,
    pub actor_id: i32,
    pub world_id: i32,
    pub plot_role: Option<String>,
    pub agreement_id: Option<i32>,
    pub agreement_has_messenger: Option<bool>,
}

impl HFPlotActor {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFPlotActor, HFPlotActor> for HFPlotActor {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_plot_actors: &[HFPlotActor]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_plot_actors::table)
            .values(hf_plot_actors)
            .on_conflict((
                hf_plot_actors::hf_id,
                hf_plot_actors::local_id,
                hf_plot_actors::actor_id,
                hf_plot_actors::world_id,
            ))
            .do_update()
            .set((
                hf_plot_actors::plot_role.eq(excluded(hf_plot_actors::plot_role)),
                hf_plot_actors::agreement_id.eq(excluded(hf_plot_actors::agreement_id)),
                hf_plot_actors::agreement_has_messenger
                    .eq(excluded(hf_plot_actors::agreement_has_messenger)),
            ))
            .execute(conn)
            .expect("Error saving hf_plot_actors");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_plot_actors: &[HFPlotActor]) {
        diesel::insert_into(hf_plot_actors::table)
            .values(hf_plot_actors)
            .execute(conn)
            .expect("Error saving hf_plot_actors");
    }

    /// Get a list of HFPlotActor from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFPlotActor>, Error> {
        use crate::schema::hf_plot_actors::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_plot_actors.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
                "actor_id" => actor_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "local_id" => local_id,
                "actor_id" => actor_id,
                "plot_role" => plot_role,
                "agreement_id" => agreement_id,
                "agreement_has_messenger" => agreement_has_messenger,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFPlotActor>, Error> {
        use crate::schema::hf_plot_actors::dsl::*;
        let query = hf_plot_actors;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
                "actor_id" => actor_id,
            ],
            {return Ok(query.first::<HFPlotActor>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "local_id" => "local_id",
            "actor_id" => "actor_id",
            "plot_role" => "plot_role",
            "agreement_id" => "agreement_id",
            "agreement_has_messenger" => "agreement_has_messenger",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFPlotActor],
        core_list: Vec<df_st_core::HFPlotActor>,
    ) -> Result<Vec<df_st_core::HFPlotActor>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_plot_actors::dsl::*;
        let query = hf_plot_actors.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
                "actor_id" => actor_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "local_id" => {local_id: i32},
                "actor_id" => {actor_id: i32},
                "plot_role" => {plot_role: Option<String>},
                "agreement_id" => {agreement_id: Option<i32>},
                "agreement_has_messenger" => {agreement_has_messenger: Option<bool>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFPlotActor, df_st_core::HFPlotActor> for HFPlotActor {
    fn add_missing_data(&mut self, source: &df_st_core::HFPlotActor) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.local_id.add_missing_data(&source.local_id);
        self.actor_id.add_missing_data(&source.actor_id);
        self.plot_role.add_missing_data(&source.plot_role);
        self.agreement_id.add_missing_data(&source.agreement_id);
        self.agreement_has_messenger
            .add_missing_data(&source.agreement_has_messenger);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFPlotActor, HFPlotActor> for df_st_core::HFPlotActor {
    fn add_missing_data(&mut self, source: &HFPlotActor) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.local_id.add_missing_data(&source.local_id);
        self.actor_id.add_missing_data(&source.actor_id);
        self.plot_role.add_missing_data(&source.plot_role);
        self.agreement_id.add_missing_data(&source.agreement_id);
        self.agreement_has_messenger
            .add_missing_data(&source.agreement_has_messenger);
    }
}

impl PartialEq<df_st_core::HFPlotActor> for HFPlotActor {
    fn eq(&self, other: &df_st_core::HFPlotActor) -> bool {
        self.hf_id == other.hf_id
            && self.local_id == other.local_id
            && self.actor_id == other.actor_id
    }
}

impl PartialEq<HFPlotActor> for df_st_core::HFPlotActor {
    fn eq(&self, other: &HFPlotActor) -> bool {
        self.hf_id == other.hf_id
            && self.local_id == other.local_id
            && self.actor_id == other.actor_id
    }
}

impl PartialEq for HFPlotActor {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.local_id == other.local_id
            && self.actor_id == other.actor_id
    }
}

impl Hash for HFPlotActor {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.local_id.hash(state);
        self.actor_id.hash(state);
    }
}
