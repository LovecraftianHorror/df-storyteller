use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_position_assignments;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_position_assignments"]
#[primary_key(en_id, local_id)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityPositionAssignment {
    pub en_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub hf_id: Option<i32>,
    pub position_id: Option<i32>,
    pub squad_id: Option<i32>,
}

impl EntityPositionAssignment {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::EntityPositionAssignment, EntityPositionAssignment>
    for EntityPositionAssignment
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        entity_position_assignments: &[EntityPositionAssignment],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(entity_position_assignments::table)
            .values(entity_position_assignments)
            .on_conflict((
                entity_position_assignments::en_id,
                entity_position_assignments::local_id,
                entity_position_assignments::world_id,
            ))
            .do_update()
            .set((
                entity_position_assignments::hf_id.eq(excluded(entity_position_assignments::hf_id)),
                entity_position_assignments::position_id
                    .eq(excluded(entity_position_assignments::position_id)),
                entity_position_assignments::squad_id
                    .eq(excluded(entity_position_assignments::squad_id)),
            ))
            .execute(conn)
            .expect("Error saving entity_position_assignments");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        entity_position_assignments: &[EntityPositionAssignment],
    ) {
        diesel::insert_into(entity_position_assignments::table)
            .values(entity_position_assignments)
            .execute(conn)
            .expect("Error saving entity_position_assignments");
    }

    /// Get a list of EntityPositionAssignment from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<EntityPositionAssignment>, Error> {
        use crate::schema::entity_position_assignments::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_position_assignments.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "local_id" => local_id,
                "hf_id" => hf_id,
                "position_id" => position_id,
                "squad_id" => squad_id,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityPositionAssignment>, Error> {
        use crate::schema::entity_position_assignments::dsl::*;
        let query = entity_position_assignments;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {return Ok(query.first::<EntityPositionAssignment>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "local_id" => "local_id",
            "hf_id" => "hf_id",
            "position_id" => "position_id",
            "squad_id" => "squad_id",
            "en_id" | _ => "en_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityPositionAssignment],
        core_list: Vec<df_st_core::EntityPositionAssignment>,
    ) -> Result<Vec<df_st_core::EntityPositionAssignment>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_position_assignments::dsl::*;
        let query = entity_position_assignments
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "local_id" => {local_id: i32},
                "hf_id" => {hf_id: Option<i32>},
                "position_id" => {position_id: Option<i32>},
                "squad_id" => {squad_id: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<EntityPositionAssignment, df_st_core::EntityPositionAssignment>
    for EntityPositionAssignment
{
    fn add_missing_data(&mut self, source: &df_st_core::EntityPositionAssignment) {
        self.local_id.add_missing_data(&source.local_id);
        self.hf_id.add_missing_data(&source.hf_id);
        self.position_id.add_missing_data(&source.position_id);
        self.squad_id.add_missing_data(&source.squad_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::EntityPositionAssignment, EntityPositionAssignment>
    for df_st_core::EntityPositionAssignment
{
    fn add_missing_data(&mut self, source: &EntityPositionAssignment) {
        self.local_id.add_missing_data(&source.local_id);
        self.hf_id.add_missing_data(&source.hf_id);
        self.position_id.add_missing_data(&source.position_id);
        self.squad_id.add_missing_data(&source.squad_id);
    }
}

impl PartialEq for EntityPositionAssignment {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.local_id == other.local_id
    }
}

impl Hash for EntityPositionAssignment {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.local_id.hash(state);
    }
}

impl PartialEq<EntityPositionAssignment> for df_st_core::EntityPositionAssignment {
    fn eq(&self, other: &EntityPositionAssignment) -> bool {
        self.local_id == other.local_id
    }
}

impl PartialEq<df_st_core::EntityPositionAssignment> for EntityPositionAssignment {
    fn eq(&self, other: &df_st_core::EntityPositionAssignment) -> bool {
        self.local_id == other.local_id
    }
}
