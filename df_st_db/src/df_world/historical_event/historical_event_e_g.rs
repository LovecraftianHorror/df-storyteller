use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_e_g;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_e_g"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventEG {
    pub he_id: i32,
    pub world_id: i32,

    pub enslaved_hf_id: Option<i32>,
    pub entity_id: Option<i32>,
    pub entity_id_1: Option<i32>,
    pub entity_id_2: Option<i32>,
    pub exiled: Option<bool>,
    pub eater_hf_id: Option<i32>,

    pub failed_judgment_test: Option<bool>,
    pub feature_layer_id: Option<i32>,
    pub first: Option<bool>,
    pub fooled_hf_id: Option<i32>,
    pub form_id: Option<i32>,
    pub framer_hf_id: Option<i32>,
    pub from_original: Option<bool>,

    pub gambler_hf_id: Option<i32>,
    pub giver_entity_id: Option<i32>,
    pub giver_hf_id: Option<i32>,
    pub group_1_hf_id: Option<i32>,
    pub group_2_hf_id: Option<i32>,
    pub group_hf_id: Option<i32>,
}

impl HistoricalEventEG {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventEG> for HistoricalEventEG {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_e_g: &[HistoricalEventEG]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_e_g::table)
            .values(historical_events_e_g)
            .on_conflict((
                historical_events_e_g::he_id,
                historical_events_e_g::world_id,
            ))
            .do_update()
            .set((
                historical_events_e_g::enslaved_hf_id
                    .eq(excluded(historical_events_e_g::enslaved_hf_id)),
                historical_events_e_g::entity_id.eq(excluded(historical_events_e_g::entity_id)),
                historical_events_e_g::entity_id_1.eq(excluded(historical_events_e_g::entity_id_1)),
                historical_events_e_g::entity_id_2.eq(excluded(historical_events_e_g::entity_id_2)),
                historical_events_e_g::exiled.eq(excluded(historical_events_e_g::exiled)),
                historical_events_e_g::eater_hf_id.eq(excluded(historical_events_e_g::eater_hf_id)),
                historical_events_e_g::failed_judgment_test
                    .eq(excluded(historical_events_e_g::failed_judgment_test)),
                historical_events_e_g::feature_layer_id
                    .eq(excluded(historical_events_e_g::feature_layer_id)),
                historical_events_e_g::first.eq(excluded(historical_events_e_g::first)),
                historical_events_e_g::fooled_hf_id
                    .eq(excluded(historical_events_e_g::fooled_hf_id)),
                historical_events_e_g::form_id.eq(excluded(historical_events_e_g::form_id)),
                historical_events_e_g::framer_hf_id
                    .eq(excluded(historical_events_e_g::framer_hf_id)),
                historical_events_e_g::from_original
                    .eq(excluded(historical_events_e_g::from_original)),
                historical_events_e_g::gambler_hf_id
                    .eq(excluded(historical_events_e_g::gambler_hf_id)),
                historical_events_e_g::giver_entity_id
                    .eq(excluded(historical_events_e_g::giver_entity_id)),
                historical_events_e_g::giver_hf_id.eq(excluded(historical_events_e_g::giver_hf_id)),
                historical_events_e_g::group_1_hf_id
                    .eq(excluded(historical_events_e_g::group_1_hf_id)),
                historical_events_e_g::group_2_hf_id
                    .eq(excluded(historical_events_e_g::group_2_hf_id)),
                historical_events_e_g::group_hf_id.eq(excluded(historical_events_e_g::group_hf_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_e_g");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_e_g: &[HistoricalEventEG]) {
        diesel::insert_into(historical_events_e_g::table)
            .values(historical_events_e_g)
            .execute(conn)
            .expect("Error saving historical_events_e_g");
    }

    /// Get a list of HistoricalEventEG from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventEG>, Error> {
        /*
        use crate::schema::historical_events_e_g::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_e_g.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "enslaved_hf_id" => enslaved_hf_id,
                "entity_id" => entity_id,
                "entity_id_1" => entity_id_1,
                "entity_id_2" => entity_id_2,
                "exiled" => exiled,
                "eater_hf_id" => eater_hf_id,
                "failed_judgment_test" => failed_judgment_test,
                "feature_layer_id" => feature_layer_id,
                "first" => first,
                "fooled_hf_id" => fooled_hf_id,
                "form_id" => form_id,
                "framer_hf_id" => framer_hf_id,
                "from_original" => from_original,
                "gambler_hf_id" => gambler_hf_id,
                "giver_entity_id" => giver_entity_id,
                "giver_hf_id" => giver_hf_id,
                "group_1_hf_id" => group_1_hf_id,
                "group_2_hf_id" => group_2_hf_id,
                "group_hf_id" => group_hf_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventEG>, Error> {
        use crate::schema::historical_events_e_g::dsl::*;
        let query = historical_events_e_g;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventEG>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "enslaved_hf_id" => "enslaved_hf_id",
            "entity_id" => "entity_id",
            "entity_id_1" => "entity_id_1",
            "entity_id_2" => "entity_id_2",
            "exiled" => "exiled",
            "eater_hf_id" => "eater_hf_id",
            "failed_judgment_test" => "failed_judgment_test",
            "feature_layer_id" => "feature_layer_id",
            "first" => "first",
            "fooled_hf_id" => "fooled_hf_id",
            "form_id" => "form_id",
            "framer_hf_id" => "framer_hf_id",
            "from_original" => "from_original",
            "gambler_hf_id" => "gambler_hf_id",
            "giver_entity_id" => "giver_entity_id",
            "giver_hf_id" => "giver_hf_id",
            "group_1_hf_id" => "group_1_hf_id",
            "group_2_hf_id" => "group_2_hf_id",
            "group_hf_id" => "group_hf_id",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventEG],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_e_g::dsl::*;
        let query = historical_events_e_g.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "enslaved_hf_id" => {enslaved_hf_id: Option<i32>},
                "entity_id" => {entity_id: Option<i32>},
                "entity_id_1" => {entity_id_1: Option<i32>},
                "entity_id_2" => {entity_id_2: Option<i32>},
                "exiled" => {exiled: Option<bool>},
                "eater_hf_id" => {eater_hf_id: Option<i32>},
                "failed_judgment_test" => {failed_judgment_test: Option<bool>},
                "feature_layer_id" => {feature_layer_id: Option<i32>},
                "first" => {first: Option<bool>},
                "fooled_hf_id" => {fooled_hf_id: Option<i32>},
                "form_id" => {form_id: Option<i32>},
                "framer_hf_id" => {framer_hf_id: Option<i32>},
                "from_original" => {from_original: Option<bool>},
                "gambler_hf_id" => {gambler_hf_id: Option<i32>},
                "giver_entity_id" => {giver_entity_id: Option<i32>},
                "giver_hf_id" => {giver_hf_id: Option<i32>},
                "group_1_hf_id" => {group_1_hf_id: Option<i32>},
                "group_2_hf_id" => {group_2_hf_id: Option<i32>},
                "group_hf_id" => {group_hf_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventEG, df_st_core::HistoricalEvent> for HistoricalEventEG {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.enslaved_hf_id.add_missing_data(&source.enslaved_hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.entity_id_1.add_missing_data(&source.entity_id_1);
        self.entity_id_2.add_missing_data(&source.entity_id_2);
        self.exiled.add_missing_data(&source.exiled);
        self.eater_hf_id.add_missing_data(&source.eater_hf_id);
        self.failed_judgment_test
            .add_missing_data(&source.failed_judgment_test);
        self.feature_layer_id
            .add_missing_data(&source.feature_layer_id);
        self.first.add_missing_data(&source.first);
        self.fooled_hf_id.add_missing_data(&source.fooled_hf_id);
        self.form_id.add_missing_data(&source.form_id);
        self.framer_hf_id.add_missing_data(&source.framer_hf_id);
        self.from_original.add_missing_data(&source.from_original);
        self.gambler_hf_id.add_missing_data(&source.gambler_hf_id);
        self.giver_entity_id
            .add_missing_data(&source.giver_entity_id);
        self.giver_hf_id.add_missing_data(&source.giver_hf_id);
        self.group_1_hf_id.add_missing_data(&source.group_1_hf_id);
        self.group_2_hf_id.add_missing_data(&source.group_2_hf_id);
        self.group_hf_id.add_missing_data(&source.group_hf_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventEG> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventEG) {
        self.id.add_missing_data(&source.he_id);
        self.enslaved_hf_id.add_missing_data(&source.enslaved_hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.entity_id_1.add_missing_data(&source.entity_id_1);
        self.entity_id_2.add_missing_data(&source.entity_id_2);
        self.exiled.add_missing_data(&source.exiled);
        self.eater_hf_id.add_missing_data(&source.eater_hf_id);
        self.failed_judgment_test
            .add_missing_data(&source.failed_judgment_test);
        self.feature_layer_id
            .add_missing_data(&source.feature_layer_id);
        self.first.add_missing_data(&source.first);
        self.fooled_hf_id.add_missing_data(&source.fooled_hf_id);
        self.form_id.add_missing_data(&source.form_id);
        self.framer_hf_id.add_missing_data(&source.framer_hf_id);
        self.from_original.add_missing_data(&source.from_original);
        self.gambler_hf_id.add_missing_data(&source.gambler_hf_id);
        self.giver_entity_id
            .add_missing_data(&source.giver_entity_id);
        self.giver_hf_id.add_missing_data(&source.giver_hf_id);
        self.group_1_hf_id.add_missing_data(&source.group_1_hf_id);
        self.group_2_hf_id.add_missing_data(&source.group_2_hf_id);
        self.group_hf_id.add_missing_data(&source.group_hf_id);
    }
}

impl PartialEq for HistoricalEventEG {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventEG {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventEG> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventEG) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventEG {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
