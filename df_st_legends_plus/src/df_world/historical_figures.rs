use df_st_core::{Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct HistoricalFigure {
    pub id: i32,
    pub sex: Option<i32>,
    pub race: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HistoricalFigures {
    pub historical_figure: Option<Vec<HistoricalFigure>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HistoricalFigure, HistoricalFigure> for df_st_core::HistoricalFigure {
    fn add_missing_data(&mut self, source: &HistoricalFigure) {
        self.id.add_missing_data(&source.id);
        if let Some(sex) = &source.sex {
            let new_caste = Some(
                match sex {
                    0 => "female",
                    1 => "male",
                    -1 => "default",
                    _ => {
                        warn!("HistoricalFigure: Gender could not match: {}", sex);
                        "default"
                    }
                }
                .to_owned(),
            );
            if self.caste == Some("default".to_owned()) {
                // If existing value is `default` replace.
                self.caste.replace_data(&new_caste);
            } else if new_caste != Some("default".to_owned()) {
                self.caste.add_missing_data(&new_caste);
            } else {
                // don't override with "default"
                self.caste.never_replace_data(&new_caste);
            }
        }
        // Replace the data as this is more human readable.
        // The race from legends_plus is also stored in `race_id`
        self.race.replace_data(&source.race);
    }
}

impl PartialEq<df_st_core::HistoricalFigure> for HistoricalFigure {
    fn eq(&self, other: &df_st_core::HistoricalFigure) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::HistoricalFigure>, HistoricalFigures>
    for Vec<df_st_core::HistoricalFigure>
{
    fn add_missing_data(&mut self, source: &HistoricalFigures) {
        self.add_missing_data(&source.historical_figure);
    }
}

impl Filler<IndexMap<u64, df_st_core::HistoricalFigure>, HistoricalFigures>
    for IndexMap<u64, df_st_core::HistoricalFigure>
{
    fn add_missing_data(&mut self, source: &HistoricalFigures) {
        self.add_missing_data(&source.historical_figure);
    }
}
