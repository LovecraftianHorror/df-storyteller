use df_st_core::{Filler, HasUnknown};
use df_st_derive::HasUnknown;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationship {
    // ID not given in xml, but later generated.
    // Might be added later to xml
    pub id: Option<i32>,
    pub event: Option<i32>,
    pub relationship: Option<String>,
    pub source_hf: Option<i32>,
    pub target_hf: Option<i32>,
    pub year: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationships {
    pub historical_event_relationship: Option<Vec<HERelationship>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HERelationship, HERelationship> for df_st_core::HERelationship {
    fn add_missing_data(&mut self, source: &HERelationship) {
        if let Some(id) = source.id {
            // Override generated ID if ever added to output
            self.id.add_missing_data(&id);
        }
        self.event.add_missing_data(&source.event);
        // Pre DFHack v0.47.04-r2 `relationship` was a number.
        if let Some(relationship) = &source.relationship {
            match relationship.parse::<i32>() {
                Ok(_number) => {
                    // TODO convert number to its string counterparts
                }
                Err(_) => {
                    self.relationship.add_missing_data(&source.relationship);
                }
            }
        }
        self.source_hf_id.add_missing_data(&source.source_hf);
        self.target_hf_id.add_missing_data(&source.target_hf);
        self.year.add_missing_data(&source.year);
    }

    fn add_missing_data_indexed(&mut self, source: &HERelationship, index: u64) {
        if source.id.is_none() {
            // If no ID set, use index
            self.id.add_missing_data(&(index as i32));
        }
        self.add_missing_data(source);
    }
}

impl PartialEq<df_st_core::HERelationship> for HERelationship {
    fn eq(&self, other: &df_st_core::HERelationship) -> bool {
        self.event == other.event
            && self.relationship == other.relationship
            && self.source_hf == other.source_hf_id
            && self.target_hf == other.target_hf_id
            && self.year == other.year
    }
}

impl std::hash::Hash for HERelationship {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.event.hash(state);
        self.relationship.hash(state);
        self.source_hf.hash(state);
        self.target_hf.hash(state);
        self.year.hash(state);
    }
}

impl Filler<Vec<df_st_core::HERelationship>, HERelationships> for Vec<df_st_core::HERelationship> {
    fn add_missing_data(&mut self, source: &HERelationships) {
        self.add_missing_data(&source.historical_event_relationship);
    }
}

impl Filler<IndexMap<u64, df_st_core::HERelationship>, HERelationships>
    for IndexMap<u64, df_st_core::HERelationship>
{
    fn add_missing_data(&mut self, source: &HERelationships) {
        self.add_missing_data(&source.historical_event_relationship);
    }
}
