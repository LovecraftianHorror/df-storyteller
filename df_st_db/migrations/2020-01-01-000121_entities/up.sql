
CREATE TABLE entities (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  race VARCHAR,
  type VARCHAR,

  PRIMARY KEY (id, world_id)
);


CREATE TABLE entity_honors (
  en_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  gives_precedence INTEGER,
  requires_any_melee_or_ranged_skill BOOLEAN,
  required_skill_ip_total INTEGER,
  required_battles INTEGER,
  required_years INTEGER,
  required_skill VARCHAR,
  required_kills INTEGER,
  exempt_ep_id INTEGER,
  exempt_former_ep_id INTEGER,
  granted_to_everybody BOOLEAN,

  PRIMARY KEY (en_id, local_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_links (
  en_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  type VARCHAR,
  target INTEGER,
  strength INTEGER,

  PRIMARY KEY (en_id, local_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_positions (
  en_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  name_male VARCHAR,
  name_female VARCHAR,
  spouse VARCHAR,
  spouse_male VARCHAR,
  spouse_female VARCHAR,

  PRIMARY KEY (en_id, local_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_position_assignments (
  en_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  hf_id INTEGER,
  position_id INTEGER,
  squad_id INTEGER,

  PRIMARY KEY (en_id, local_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_occasions (
  en_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  event INTEGER,

  PRIMARY KEY (en_id, local_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_occasion_schedules (
  en_id INTEGER NOT NULL,
  en_occ_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  type_ VARCHAR,
  item_type VARCHAR,
  item_subtype VARCHAR,

  reference INTEGER,
  reference2 INTEGER,

  PRIMARY KEY (en_id, en_occ_id, local_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id),
  FOREIGN KEY (en_id, en_occ_id, world_id) 
    REFERENCES entity_occasions (en_id, local_id, world_id)
);

CREATE TABLE entity_occasion_schedule_features (
  en_id INTEGER NOT NULL,
  en_occ_id INTEGER NOT NULL,
  en_occ_sch_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  type VARCHAR,
  reference INTEGER,

  PRIMARY KEY (en_id, en_occ_id, en_occ_sch_id, local_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id),
  FOREIGN KEY (en_id, en_occ_id, world_id) 
    REFERENCES entity_occasions (en_id, local_id, world_id),
  FOREIGN KEY (en_id, en_occ_id, en_occ_sch_id, world_id) 
    REFERENCES entity_occasion_schedules (en_id, en_occ_id, local_id, world_id)
);

CREATE TABLE entity_worship_ids (
  en_id INTEGER NOT NULL,
  worship_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (en_id, worship_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_weapons (
  en_id INTEGER NOT NULL,
  weapon VARCHAR NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (en_id, weapon, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_professions (
  en_id INTEGER NOT NULL,
  profession VARCHAR NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (en_id, profession, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_hf_ids (
  en_id INTEGER NOT NULL,
  hf_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (en_id, hf_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);

CREATE TABLE entity_child_en_ids (
  en_id INTEGER NOT NULL,
  child_en_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  PRIMARY KEY (en_id, child_en_id, world_id),
  FOREIGN KEY (en_id, world_id) REFERENCES entities (id, world_id)
);