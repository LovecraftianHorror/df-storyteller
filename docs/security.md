# Security in DF Storyteller
We (that is to say me, Ralph Bisschops) take security VERY serious.
This means we will do everything we can to keep out users save.
But mistakes can (and will) always happen. So if you have questions, feel free to ask them in our 
[Discord](https://discord.gg/aAXt6uu).
If you found a vulnerability you can email it to 
[contact.ralph.b@gmail.com](mailto:contact.ralph.b@gmail.com) or 
[df.storyteller@gmail.com](mailto:df.storyteller@gmail.com).
When reporting a vulnerability you can encrypt them using the PGP key(s) below.

PGP Keys:
* Ralph Bisschops: (TODO)

Because we can only fix problems if we know about them, we will encourage users to test the 
security of our application[^1]. (In the context that it is used.) We do not have a budget for 
a Bug Bounty program. But if you correctly report vulnerabilities to us we will see what we
can do for you.

[^1]: Only test the security of your own instance of the application not on others peoples instances!

## Intended use
The intended use of our application is to be used on a computer that is used behind a NAT router.
This means that the computers ports are not exposed to the public internet.
DF Storyteller was NEVER designed to be used on public servers!

## Known "problems"
There are a few thing that we know that might pose a security risk but because the intended
use of our application there should not be an issue.

If you have a good solution for any of these problems let us know!

### Public internet
If the user is in a shared/public space and/or uses public internet this guaranty is not
enough in most cases. This is why our default settings are set to only allow connections from 
the `localhost`. This setting can be changed by the user to allow all connections.
This setting should not be enabled when on public internet.
We have no way to detect if users is currently on public internet. This means it is up to the
user to keep track of this. (Which is not ideal but the best we can do.)

### HTTPS
We currently do not have HTTPS enabled by default. This is because all traffic is intended for
`localhost` or some other internal IP. You are not allowed to sign a certificate for ip 
addresses. We could do this with a self signed certificate. But in that case the certificate has 
to be create on the clients computer. This comes with a lot of extra things that have to be 
taken into account. This makes the project much bigger and complexer so for the time being we are
not intending to implement this.

All traffic should never pass the NAT boundary and thus should not be able to be monitored.
Unless there is someone malicious on you internal network. But in that case I think you have
bigger problems then someone monitoring your Dwarf Fortress world.

## Technical security implementations

### Update checking
TODO: Add more details
We use a [Curve 25519](https://en.wikipedia.org/wiki/Curve25519) in its 
[ed25519](https://en.wikipedia.org/wiki/EdDSA#Ed25519) form for signing update messages.
Note that we are NOT encrypting the message!
This message and its signature are Base64 encoded in order to store both the message and signature
in a json string.
The signature is a 512 bit signature created using SHA-512.

When the application opens it will download the validation file from one of the sources provided.
If there are multiple sources it will go though them and check until it finds a valid message that
allows the version to be used or explicitly disallows the version.
If an error occurred or it detects that the message has been tampered with it will check the next 
source. If no sources are available it will return an error and close the application.

If the user does not have an internet connection it will permit the execution of the application.
As it does not know if the version is outdated or not.

The messages are in the form:
```json
{
    "version": "0.1.0",
    "status": "latest",
    "message": null,
    "valid_until": "2020-06-30T00:00:00Z"
}
```

The `version` field should match the version of the application exactly.

The `status` field can be 3 options: `"latest"`,`"update_available"` or `"update_required"`. 
If the status is `latest` it will allow the application to start.
If the status is `update_available` it will prompt the user that there is an update and 
start the application.
If the status is `update_required` it will prompt the user that there is an update and
terminate the application. This will disallow old versions for being used.

The `message` field is an optional field. In the case that a message is given it will output this 
message to the user and continue.

The `valid_until` field is a RFC3339 timestamp of the date and time this message is valid.
If this day is passed the message will no longer be accepted. This is compared to the system time 
and the `Date` header field of the response from the server.

The `version` field does not allows tokens to be reused for other versions. The `valid_until` 
field prevents tokens form being infinitely valid. This combined with the signature that 
authenticates the messages comes from a trusted source (developers) should now allow tampering
with the tokens.

Then there should just be a good balance between how long a token is valid and how often we want
to resign the token.