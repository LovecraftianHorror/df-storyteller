use crate::api_errors::{APIErrorNoContent, APIErrorNotModified};
use crate::api_objects::{
    ApiCountPage, ApiItem, ApiMinimalPagination, ApiObject, ApiPage, ApiPagination,
};
use crate::DfStDatabase;
use crate::ServerInfo;
use df_st_core::item_count::ItemCount;
use df_st_db::{id_filter, DBObject};
use rocket::{get, State};
use rocket_contrib::json::Json;
use rocket_okapi_codegen::openapi;

impl ApiObject for df_st_core::Site {
    fn get_type() -> String {
        "site".to_owned()
    }
    fn get_item_link(&self, base_url: &String) -> String {
        format!("{}/sites/{}", base_url, self.id)
    }
    fn get_page_link(base_url: &String) -> String {
        format!("{}/sites", base_url)
    }
    fn get_count_link(base_url: &String) -> String {
        format!("{}/sites/count", base_url)
    }
}

impl ApiObject for df_st_core::Structure {
    fn get_type() -> String {
        "structure".to_owned()
    }
    fn get_item_link(&self, base_url: &String) -> String {
        format!(
            "{}/sites/{}/structures/{}",
            base_url, self.site_id, self.local_id
        )
    }
    fn get_page_link(base_url: &String) -> String {
        format!("{}/structures", base_url)
    }
    fn get_count_link(base_url: &String) -> String {
        format!("{}/structures/count", base_url)
    }
}

impl ApiObject for df_st_core::SiteProperty {
    fn get_type() -> String {
        "site_propertie".to_owned()
    }
    fn get_item_link(&self, base_url: &String) -> String {
        format!(
            "{}/sites/{}/properties/{}",
            base_url, self.site_id, self.local_id
        )
    }
    fn get_page_link(base_url: &String) -> String {
        format!("{}/site_properties", base_url)
    }
    fn get_count_link(base_url: &String) -> String {
        format!("{}/site_properties/count", base_url)
    }
}

/// Request a `Site` by id.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/<site_id>")]
pub fn get_site(
    conn: DfStDatabase,
    site_id: i32,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::Site>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::Site>::new(&server_info);
    let site_search = df_st_db::Site::get_from_db(
        &*conn,
        id_filter!["id" => site_id, "world_id" => server_info.world_id],
        true,
    )?;
    if let Some(site) = site_search {
        api_page.wrap(site);
        return Ok(Json(api_page));
    }
    Err(APIErrorNoContent::new())
}

/// Request a list of all `Sites` in the world.
/// List is not ordered and some id's might be missing!
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites?<pagination..>")]
pub fn list_sites(
    conn: DfStDatabase,
    pagination: ApiPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::Site>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::Site>::new(&pagination, &server_info);
    api_page.order_by = df_st_db::Site::match_field_by_opt(api_page.order_by);
    api_page.total_item_count = df_st_db::Site::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        1,
        None,
    )?
    .get(0)
    .unwrap()
    .count;

    let result_list = df_st_db::Site::get_list_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.get_db_order(),
        api_page.order_by.clone(),
        true,
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a counts about `Site` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/count?<group_by>&<pagination..>")]
pub fn get_site_count(
    conn: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiMinimalPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::Site>>, APIErrorNotModified> {
    let mut api_page = ApiCountPage::<ItemCount, df_st_core::Site>::new(&pagination, &server_info);
    api_page.group_by = df_st_db::Site::match_field_by_opt(group_by);
    api_page.total_item_count = df_st_db::Site::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        u32::MAX,
        api_page.group_by.clone(),
    )?
    .len() as u32;

    let result_list = df_st_db::Site::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.group_by.clone(),
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a `Structure` by id within a `Site`.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/<site_id>/structures/<structure_id>")]
pub fn get_site_structure(
    conn: DfStDatabase,
    site_id: i32,
    structure_id: i32,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::Structure>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::Structure>::new(&server_info);
    let item_search = df_st_db::Structure::get_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "local_id" => structure_id, "world_id" => server_info.world_id],
        true,
    )?;
    if let Some(item) = item_search {
        api_page.wrap(item);
        return Ok(Json(api_page));
    }
    Err(APIErrorNoContent::new())
}

/// Request a list of all `Structure` in the `Site`.
/// List is not ordered and some id's might be missing!
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/<site_id>/structures?<pagination..>")]
pub fn list_site_structures(
    conn: DfStDatabase,
    site_id: i32,
    pagination: ApiPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::Structure>::new(&pagination, &server_info);
    api_page.order_by = df_st_db::Structure::match_field_by_opt(api_page.order_by);
    api_page.total_item_count = df_st_db::Structure::get_count_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        0,
        1,
        None,
    )?
    .get(0)
    .unwrap()
    .count;

    let result_list = df_st_db::Structure::get_list_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.get_db_order(),
        api_page.order_by.clone(),
        true,
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a counts about `Structures` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/<site_id>/structures/count?<group_by>&<pagination..>")]
pub fn get_site_structure_count(
    conn: DfStDatabase,
    site_id: i32,
    group_by: Option<String>,
    pagination: ApiMinimalPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::Structure>::new(&pagination, &server_info);
    api_page.group_by = df_st_db::Structure::match_field_by_opt(group_by);
    api_page.total_item_count = df_st_db::Structure::get_count_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        0,
        u32::MAX,
        api_page.group_by.clone(),
    )?
    .len() as u32;

    let result_list = df_st_db::Structure::get_count_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.group_by.clone(),
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a `SiteProperty` by id within a `Site`.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/<site_id>/properties/<site_property_id>")]
pub fn get_site_property(
    conn: DfStDatabase,
    site_id: i32,
    site_property_id: i32,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::SiteProperty>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::SiteProperty>::new(&server_info);
    let item_search = df_st_db::SiteProperty::get_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "local_id" => site_property_id, "world_id" => server_info.world_id],
        true,
    )?;
    if let Some(item) = item_search {
        api_page.wrap(item);
        return Ok(Json(api_page));
    }
    Err(APIErrorNoContent::new())
}

/// Request a list of all `SiteProperty` in the `Site`.
/// List is not ordered and some id's might be missing!
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/<site_id>/properties?<pagination..>")]
pub fn list_site_properties(
    conn: DfStDatabase,
    site_id: i32,
    pagination: ApiPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::SiteProperty>::new(&pagination, &server_info);
    api_page.order_by = df_st_db::SiteProperty::match_field_by_opt(api_page.order_by);
    api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        0,
        1,
        None,
    )?
    .get(0)
    .unwrap()
    .count;

    let result_list = df_st_db::SiteProperty::get_list_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.get_db_order(),
        api_page.order_by.clone(),
        true,
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a counts about `SiteProperty` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/sites/<site_id>/properties/count?<group_by>&<pagination..>")]
pub fn get_site_property_count(
    conn: DfStDatabase,
    site_id: i32,
    group_by: Option<String>,
    pagination: ApiMinimalPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::SiteProperty>::new(&pagination, &server_info);
    api_page.group_by = df_st_db::SiteProperty::match_field_by_opt(group_by);
    api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        0,
        u32::MAX,
        api_page.group_by.clone(),
    )?
    .len() as u32;

    let result_list = df_st_db::SiteProperty::get_count_from_db(
        &*conn,
        id_filter!["site_id" => site_id, "world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.group_by.clone(),
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a list of all `Structure` in the world.
/// List is not ordered and some id's might be missing!
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/structures?<pagination..>")]
pub fn list_structures(
    conn: DfStDatabase,
    pagination: ApiPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::Structure>::new(&pagination, &server_info);
    api_page.order_by = df_st_db::Structure::match_field_by_opt(api_page.order_by);
    api_page.total_item_count = df_st_db::Structure::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        1,
        None,
    )?
    .get(0)
    .unwrap()
    .count;

    let result_list = df_st_db::Structure::get_list_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.get_db_order(),
        api_page.order_by.clone(),
        true,
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a counts about `Structure` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/structures/count?<group_by>&<pagination..>")]
pub fn get_structure_count(
    conn: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiMinimalPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::Structure>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::Structure>::new(&pagination, &server_info);
    api_page.group_by = df_st_db::Structure::match_field_by_opt(group_by);
    api_page.total_item_count = df_st_db::Structure::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        u32::MAX,
        api_page.group_by.clone(),
    )?
    .len() as u32;

    let result_list = df_st_db::Structure::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.group_by.clone(),
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a list of all `SiteProperty` in the world.
/// List is not ordered and some id's might be missing!
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/site_properties?<pagination..>")]
pub fn list_all_site_properties(
    conn: DfStDatabase,
    pagination: ApiPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::SiteProperty>::new(&pagination, &server_info);
    api_page.order_by = df_st_db::SiteProperty::match_field_by_opt(api_page.order_by);
    api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        1,
        None,
    )?
    .get(0)
    .unwrap()
    .count;

    let result_list = df_st_db::SiteProperty::get_list_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.get_db_order(),
        api_page.order_by.clone(),
        true,
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a counts about `SiteProperty` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/site_properties/count?<group_by>&<pagination..>")]
pub fn get_all_site_property_count(
    conn: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiMinimalPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::SiteProperty>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::SiteProperty>::new(&pagination, &server_info);
    api_page.group_by = df_st_db::SiteProperty::match_field_by_opt(group_by);
    api_page.total_item_count = df_st_db::SiteProperty::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        0,
        u32::MAX,
        api_page.group_by.clone(),
    )?
    .len() as u32;

    let result_list = df_st_db::SiteProperty::get_count_from_db(
        &*conn,
        id_filter!["world_id" => server_info.world_id],
        api_page.page_start,
        api_page.max_page_size,
        api_page.group_by.clone(),
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}
