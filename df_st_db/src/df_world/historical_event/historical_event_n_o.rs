use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::historical_events_n_o;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "historical_events_n_o"]
#[primary_key(he_id)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HistoricalEventNO {
    pub he_id: i32,
    pub world_id: i32,

    pub name_only: Option<bool>,
    pub new_ab_id: Option<i32>,
    pub new_account: Option<i32>,
    pub new_caste: Option<i32>,
    pub new_equipment_level: Option<i32>,
    pub new_leader_hf_id: Option<i32>,
    pub new_race_id: Option<String>,
    pub new_site_civ_id: Option<i32>,
    pub new_job: Option<String>,
    pub new_structure_id: Option<i32>,

    pub occasion_id: Option<i32>,
    pub old_ab_id: Option<i32>,
    pub old_account: Option<i32>,
    pub old_caste: Option<i32>,
    pub old_race_id: Option<String>,
    pub overthrown_hf_id: Option<i32>,
    pub old_job: Option<String>,
    pub old_structure_id: Option<i32>,
}

impl HistoricalEventNO {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HistoricalEvent, HistoricalEventNO> for HistoricalEventNO {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, historical_events_n_o: &[HistoricalEventNO]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(historical_events_n_o::table)
            .values(historical_events_n_o)
            .on_conflict((
                historical_events_n_o::he_id,
                historical_events_n_o::world_id,
            ))
            .do_update()
            .set((
                historical_events_n_o::name_only.eq(excluded(historical_events_n_o::name_only)),
                historical_events_n_o::new_ab_id.eq(excluded(historical_events_n_o::new_ab_id)),
                historical_events_n_o::new_account.eq(excluded(historical_events_n_o::new_account)),
                historical_events_n_o::new_caste.eq(excluded(historical_events_n_o::new_caste)),
                historical_events_n_o::new_equipment_level
                    .eq(excluded(historical_events_n_o::new_equipment_level)),
                historical_events_n_o::new_leader_hf_id
                    .eq(excluded(historical_events_n_o::new_leader_hf_id)),
                historical_events_n_o::new_race_id.eq(excluded(historical_events_n_o::new_race_id)),
                historical_events_n_o::new_site_civ_id
                    .eq(excluded(historical_events_n_o::new_site_civ_id)),
                historical_events_n_o::new_job.eq(excluded(historical_events_n_o::new_job)),
                historical_events_n_o::new_structure_id
                    .eq(excluded(historical_events_n_o::new_structure_id)),
                historical_events_n_o::occasion_id.eq(excluded(historical_events_n_o::occasion_id)),
                historical_events_n_o::old_ab_id.eq(excluded(historical_events_n_o::old_ab_id)),
                historical_events_n_o::old_account.eq(excluded(historical_events_n_o::old_account)),
                historical_events_n_o::old_caste.eq(excluded(historical_events_n_o::old_caste)),
                historical_events_n_o::old_race_id.eq(excluded(historical_events_n_o::old_race_id)),
                historical_events_n_o::overthrown_hf_id
                    .eq(excluded(historical_events_n_o::overthrown_hf_id)),
                historical_events_n_o::old_job.eq(excluded(historical_events_n_o::old_job)),
                historical_events_n_o::old_structure_id
                    .eq(excluded(historical_events_n_o::old_structure_id)),
            ))
            .execute(conn)
            .expect("Error saving historical_events_n_o");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, historical_events_n_o: &[HistoricalEventNO]) {
        diesel::insert_into(historical_events_n_o::table)
            .values(historical_events_n_o)
            .execute(conn)
            .expect("Error saving historical_events_n_o");
    }

    /// Get a list of HistoricalEventNO from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<HistoricalEventNO>, Error> {
        /*
        use crate::schema::historical_events_n_o::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = historical_events_n_o.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "name_only" => name_only,
                "new_ab_id" => new_ab_id,
                "new_account" => new_account,
                "new_caste" => new_caste,
                "new_equipment_level" => new_equipment_level,
                "new_leader_hf_id" => new_leader_hf_id,
                "new_race_id" => new_race_id,
                "new_site_civ_id" => new_site_civ_id,
                "new_job" => new_job,
                "new_structure_id" => new_structure_id,
                "occasion_id" => occasion_id,
                "old_ab_id" => old_ab_id,
                "old_account" => old_account,
                "old_caste" => old_caste,
                "old_race_id" => old_race_id,
                "overthrown_hf_id" => overthrown_hf_id,
                "old_job" => old_job,
                "old_structure_id" => old_structure_id,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HistoricalEventNO>, Error> {
        use crate::schema::historical_events_n_o::dsl::*;
        let query = historical_events_n_o;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HistoricalEventNO>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "name_only" => "name_only",
            "new_ab_id" => "new_ab_id",
            "new_account" => "new_account",
            "new_caste" => "new_caste",
            "new_equipment_level" => "new_equipment_level",
            "new_leader_hf_id" => "new_leader_hf_id",
            "new_race_id" => "new_race_id",
            "new_site_civ_id" => "new_site_civ_id",
            "new_job" => "new_job",
            "new_structure_id" => "new_structure_id",
            "occasion_id" => "occasion_id",
            "old_ab_id" => "old_ab_id",
            "old_account" => "old_account",
            "old_caste" => "old_caste",
            "old_race_id" => "old_race_id",
            "overthrown_hf_id" => "overthrown_hf_id",
            "old_job" => "old_job",
            "old_structure_id" => "old_structure_id",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HistoricalEventNO],
        core_list: Vec<df_st_core::HistoricalEvent>,
    ) -> Result<Vec<df_st_core::HistoricalEvent>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::historical_events_n_o::dsl::*;
        let query = historical_events_n_o.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "name_only" => {name_only: Option<bool>},
                "new_ab_id" => {new_ab_id: Option<i32>},
                "new_account" => {new_account: Option<i32>},
                "new_caste" => {new_caste: Option<i32>},
                "new_equipment_level" => {new_equipment_level: Option<i32>},
                "new_leader_hf_id" => {new_leader_hf_id: Option<i32>},
                "new_race_id" => {new_race_id: Option<String>},
                "new_site_civ_id" => {new_site_civ_id: Option<i32>},
                "new_job" => {new_job: Option<String>},
                "new_structure_id" => {new_structure_id: Option<i32>},
                "occasion_id" => {occasion_id: Option<i32>},
                "old_ab_id" => {old_ab_id: Option<i32>},
                "old_account" => {old_account: Option<i32>},
                "old_caste" => {old_caste: Option<i32>},
                "old_race_id" => {old_race_id: Option<String>},
                "overthrown_hf_id" => {overthrown_hf_id: Option<i32>},
                "old_job" => {old_job: Option<String>},
                "old_structure_id" => {old_structure_id: Option<i32>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<HistoricalEventNO, df_st_core::HistoricalEvent> for HistoricalEventNO {
    fn add_missing_data(&mut self, source: &df_st_core::HistoricalEvent) {
        self.he_id.add_missing_data(&source.id);
        self.name_only.add_missing_data(&source.name_only);
        self.new_ab_id.add_missing_data(&source.new_ab_id);
        self.new_account.add_missing_data(&source.new_account);
        self.new_caste.add_missing_data(&source.new_caste);
        self.new_equipment_level
            .add_missing_data(&source.new_equipment_level);
        self.new_leader_hf_id
            .add_missing_data(&source.new_leader_hf_id);
        self.new_race_id.add_missing_data(&source.new_race_id);
        self.new_site_civ_id
            .add_missing_data(&source.new_site_civ_id);
        self.new_job.add_missing_data(&source.new_job);
        self.new_structure_id
            .add_missing_data(&source.new_structure_id);
        self.occasion_id.add_missing_data(&source.occasion_id);
        self.old_ab_id.add_missing_data(&source.old_ab_id);
        self.old_account.add_missing_data(&source.old_account);
        self.old_caste.add_missing_data(&source.old_caste);
        self.old_race_id.add_missing_data(&source.old_race_id);
        self.overthrown_hf_id
            .add_missing_data(&source.overthrown_hf_id);
        self.old_job.add_missing_data(&source.old_job);
        self.old_structure_id
            .add_missing_data(&source.old_structure_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HistoricalEvent, HistoricalEventNO> for df_st_core::HistoricalEvent {
    fn add_missing_data(&mut self, source: &HistoricalEventNO) {
        self.id.add_missing_data(&source.he_id);
        self.name_only.add_missing_data(&source.name_only);
        self.new_ab_id.add_missing_data(&source.new_ab_id);
        self.new_account.add_missing_data(&source.new_account);
        self.new_caste.add_missing_data(&source.new_caste);
        self.new_equipment_level
            .add_missing_data(&source.new_equipment_level);
        self.new_leader_hf_id
            .add_missing_data(&source.new_leader_hf_id);
        self.new_race_id.add_missing_data(&source.new_race_id);
        self.new_site_civ_id
            .add_missing_data(&source.new_site_civ_id);
        self.new_job.add_missing_data(&source.new_job);
        self.new_structure_id
            .add_missing_data(&source.new_structure_id);
        self.occasion_id.add_missing_data(&source.occasion_id);
        self.old_ab_id.add_missing_data(&source.old_ab_id);
        self.old_account.add_missing_data(&source.old_account);
        self.old_caste.add_missing_data(&source.old_caste);
        self.old_race_id.add_missing_data(&source.old_race_id);
        self.overthrown_hf_id
            .add_missing_data(&source.overthrown_hf_id);
        self.old_job.add_missing_data(&source.old_job);
        self.old_structure_id
            .add_missing_data(&source.old_structure_id);
    }
}

impl PartialEq for HistoricalEventNO {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id
    }
}

impl Hash for HistoricalEventNO {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
    }
}

impl PartialEq<HistoricalEventNO> for df_st_core::HistoricalEvent {
    fn eq(&self, other: &HistoricalEventNO) -> bool {
        self.id == other.he_id
    }
}

impl PartialEq<df_st_core::HistoricalEvent> for HistoricalEventNO {
    fn eq(&self, other: &df_st_core::HistoricalEvent) -> bool {
        self.he_id == other.id
    }
}
