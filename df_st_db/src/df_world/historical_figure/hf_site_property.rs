use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_site_properties;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hf_site_properties"]
#[primary_key(hf_id, site_id, property_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFSiteProperty {
    pub hf_id: i32,
    pub site_id: i32,
    pub property_id: i32,
    pub world_id: i32,
}

impl HFSiteProperty {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFSiteProperty, HFSiteProperty> for HFSiteProperty {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_site_properties: &[HFSiteProperty]) {
        diesel::insert_into(hf_site_properties::table)
            .values(hf_site_properties)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hf_site_properties");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_site_properties: &[HFSiteProperty]) {
        diesel::insert_into(hf_site_properties::table)
            .values(hf_site_properties)
            .execute(conn)
            .expect("Error saving hf_site_properties");
    }

    /// Get a list of HFSiteProperty from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFSiteProperty>, Error> {
        use crate::schema::hf_site_properties::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_site_properties.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "site_id" => site_id,
                "property_id" => property_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "site_id" => site_id,
                "property_id" => property_id,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFSiteProperty>, Error> {
        use crate::schema::hf_site_properties::dsl::*;
        let query = hf_site_properties;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "site_id" => site_id,
                "property_id" => property_id,
            ],
            {return Ok(query.first::<HFSiteProperty>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "site_id" => "site_id",
            "property_id" => "property_id",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFSiteProperty],
        core_list: Vec<df_st_core::HFSiteProperty>,
    ) -> Result<Vec<df_st_core::HFSiteProperty>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_site_properties::dsl::*;
        let query = hf_site_properties.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "site_id" => site_id,
                "property_id" => property_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "site_id" => {site_id: i32},
                "property_id" => {property_id: i32},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFSiteProperty, df_st_core::HFSiteProperty> for HFSiteProperty {
    fn add_missing_data(&mut self, source: &df_st_core::HFSiteProperty) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.site_id.add_missing_data(&source.site_id);
        self.property_id.add_missing_data(&source.property_id);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFSiteProperty, HFSiteProperty> for df_st_core::HFSiteProperty {
    fn add_missing_data(&mut self, source: &HFSiteProperty) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.site_id.add_missing_data(&source.site_id);
        self.property_id.add_missing_data(&source.property_id);
    }
}

impl PartialEq<df_st_core::HFSiteProperty> for HFSiteProperty {
    fn eq(&self, other: &df_st_core::HFSiteProperty) -> bool {
        self.hf_id == other.hf_id
            && self.site_id == other.site_id
            && self.property_id == other.property_id
    }
}

impl PartialEq<HFSiteProperty> for df_st_core::HFSiteProperty {
    fn eq(&self, other: &HFSiteProperty) -> bool {
        self.hf_id == other.hf_id
            && self.site_id == other.site_id
            && self.property_id == other.property_id
    }
}

impl PartialEq for HFSiteProperty {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.site_id == other.site_id
            && self.property_id == other.property_id
    }
}

impl Hash for HFSiteProperty {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.site_id.hash(state);
        self.property_id.hash(state);
    }
}
