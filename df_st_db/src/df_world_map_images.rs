use df_st_core::{Fillable, Filler};
use df_st_derive::Fillable;

use crate::db_object::DBObject;
use crate::DbConnection;
use indicatif::ProgressBar;

mod map_image;
pub use map_image::MapImage;

#[derive(Clone, Fillable, Default, PartialEq, Debug)]
pub struct WorldMapImages {
    pub detailed: Option<MapImage>,
    pub world_map: Option<MapImage>,
    pub biome: Option<MapImage>,
    pub diplomacy: Option<MapImage>,
    pub drainage: Option<MapImage>,
    pub elevation: Option<MapImage>,
    pub elevation_water: Option<MapImage>,
    pub evil: Option<MapImage>,
    pub hydrologic: Option<MapImage>,
    pub nobility: Option<MapImage>,
    pub rainfall: Option<MapImage>,
    pub salinity: Option<MapImage>,
    pub savagery: Option<MapImage>,
    pub cadaster: Option<MapImage>,
    pub temperature: Option<MapImage>,
    pub trade: Option<MapImage>,
    pub vegetation: Option<MapImage>,
    pub volcanism: Option<MapImage>,
}

macro_rules! progress_code(
    { $progress_bar:ident, $($label:expr => $code:block),* $(,)* } => {
        {
            let labels = vec![$($label),*];
            $progress_bar.set_length(labels.len() as u64);
            $(
                $progress_bar.set_message($label);
                $progress_bar.inc(1);
                $code
            )*
            $progress_bar.finish_with_message("Done ✔️");
        }
     };
);

macro_rules! set_world_ids_options(
    { $self:ident, $world_id:ident: [$($lists:ident),* $(,)*] } => {
        {
            $(
                if let Some(mut x) = $self.$lists.as_mut() {
                    x.world_id = $world_id;
                }
            )*
        }
     };
);

impl WorldMapImages {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set_world_id(&mut self, world_id: i32) {
        set_world_ids_options!(self, world_id: [
            detailed, world_map, biome, diplomacy, drainage, elevation,
            elevation_water, evil, hydrologic, nobility, rainfall, salinity,
            savagery, cadaster, temperature, trade, vegetation, volcanism
        ]);
    }

    pub fn insert_into_db(&self, conn: &DbConnection, progress_bar: ProgressBar) {
        let mut maps = Vec::new();
        maps.append(&mut Self::unwrap_map(&self.detailed));
        maps.append(&mut Self::unwrap_map(&self.world_map));
        maps.append(&mut Self::unwrap_map(&self.biome));
        maps.append(&mut Self::unwrap_map(&self.diplomacy));
        maps.append(&mut Self::unwrap_map(&self.drainage));
        maps.append(&mut Self::unwrap_map(&self.elevation));
        maps.append(&mut Self::unwrap_map(&self.elevation_water));
        maps.append(&mut Self::unwrap_map(&self.evil));
        maps.append(&mut Self::unwrap_map(&self.hydrologic));
        maps.append(&mut Self::unwrap_map(&self.nobility));
        maps.append(&mut Self::unwrap_map(&self.rainfall));
        maps.append(&mut Self::unwrap_map(&self.salinity));
        maps.append(&mut Self::unwrap_map(&self.savagery));
        maps.append(&mut Self::unwrap_map(&self.cadaster));
        maps.append(&mut Self::unwrap_map(&self.temperature));
        maps.append(&mut Self::unwrap_map(&self.trade));
        maps.append(&mut Self::unwrap_map(&self.vegetation));
        maps.append(&mut Self::unwrap_map(&self.volcanism));
        progress_code! { progress_bar,
            "Maps" => {MapImage::insert_into_db(&conn, &maps);},
        }
    }
    fn unwrap_map(map: &Option<MapImage>) -> Vec<MapImage> {
        if let Some(map) = map {
            return vec![map.clone()];
        }
        return vec![];
    }
}

/// From Core to DB (World data)
impl Filler<WorldMapImages, df_st_core::WorldMapImages> for WorldMapImages {
    fn add_missing_data(&mut self, source: &df_st_core::WorldMapImages) {
        self.detailed.add_missing_data(&source.detailed);
        self.world_map.add_missing_data(&source.world_map);
        self.biome.add_missing_data(&source.biome);
        self.diplomacy.add_missing_data(&source.diplomacy);
        self.drainage.add_missing_data(&source.drainage);
        self.elevation.add_missing_data(&source.elevation);
        self.elevation_water
            .add_missing_data(&source.elevation_water);
        self.evil.add_missing_data(&source.evil);
        self.hydrologic.add_missing_data(&source.hydrologic);
        self.nobility.add_missing_data(&source.nobility);
        self.rainfall.add_missing_data(&source.rainfall);
        self.salinity.add_missing_data(&source.salinity);
        self.savagery.add_missing_data(&source.savagery);
        self.cadaster.add_missing_data(&source.cadaster);
        self.temperature.add_missing_data(&source.temperature);
        self.trade.add_missing_data(&source.trade);
        self.vegetation.add_missing_data(&source.vegetation);
        self.volcanism.add_missing_data(&source.volcanism);
    }
}
