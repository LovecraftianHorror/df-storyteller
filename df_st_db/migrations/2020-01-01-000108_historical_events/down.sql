
DROP TABLE historical_events;

DROP TABLE historical_events_a_a;
DROP TABLE historical_events_b_b;
DROP TABLE historical_events_c_c;
DROP TABLE historical_events_d_d;
DROP TABLE historical_events_e_g;
DROP TABLE historical_events_h_h;
DROP TABLE historical_events_i_i;
DROP TABLE historical_events_j_m;
DROP TABLE historical_events_n_o;
DROP TABLE historical_events_p_p;
DROP TABLE historical_events_q_r;
DROP TABLE historical_events_s_s1;
DROP TABLE historical_events_s_s2;
DROP TABLE historical_events_t_t;
DROP TABLE historical_events_u_w;

DROP TABLE he_circumstances;
DROP TABLE he_reasons;
DROP TABLE he_bodies_hf_ids;
DROP TABLE he_competitor_hf_ids;
DROP TABLE he_conspirator_hf_ids;
DROP TABLE he_expelled_creatures;
DROP TABLE he_expelled_hf_ids;
DROP TABLE he_expelled_numbers;
DROP TABLE he_expelled_pop_ids;
DROP TABLE he_groups_hf_ids;
DROP TABLE he_implicated_hf_ids;
DROP TABLE he_joining_en_ids;
DROP TABLE he_pets;
