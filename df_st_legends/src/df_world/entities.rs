use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default, HashAndPartialEqById,
)]
pub struct Entity {
    pub id: i32,
    pub name: Option<String>,
    pub honor: Option<Vec<Honor>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct Entities {
    pub entity: Option<Vec<Entity>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, HasUnknown, Default)]
pub struct Honor {
    pub id: i32,
    pub name: Option<String>,
    pub gives_precedence: Option<i32>,
    pub requires_any_melee_or_ranged_skill: Option<()>,
    pub required_skill_ip_total: Option<i32>,
    pub required_battles: Option<i32>,
    pub required_years: Option<i32>,
    pub required_skill: Option<String>,
    pub required_kills: Option<i32>,
    pub exempt_epid: Option<i32>,
    pub exempt_former_epid: Option<i32>,
    pub granted_to_everybody: Option<()>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::Entity, Entity> for df_st_core::Entity {
    fn add_missing_data(&mut self, source: &Entity) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.honor.add_missing_data(&source.honor);
    }
}

impl PartialEq<df_st_core::Entity> for Entity {
    fn eq(&self, other: &df_st_core::Entity) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::Entity>, Entities> for Vec<df_st_core::Entity> {
    fn add_missing_data(&mut self, source: &Entities) {
        self.add_missing_data(&source.entity);
    }
}

impl Filler<IndexMap<u64, df_st_core::Entity>, Entities> for IndexMap<u64, df_st_core::Entity> {
    fn add_missing_data(&mut self, source: &Entities) {
        self.add_missing_data(&source.entity);
    }
}

impl Filler<df_st_core::EntityHonor, Honor> for df_st_core::EntityHonor {
    fn add_missing_data(&mut self, source: &Honor) {
        self.local_id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.gives_precedence
            .add_missing_data(&source.gives_precedence);
        self.requires_any_melee_or_ranged_skill
            .add_missing_data(&source.requires_any_melee_or_ranged_skill);
        self.required_skill_ip_total
            .add_missing_data(&source.required_skill_ip_total);
        self.required_battles
            .add_missing_data(&source.required_battles);
        self.required_years.add_missing_data(&source.required_years);
        self.required_skill.add_missing_data(&source.required_skill);
        self.required_kills.add_missing_data(&source.required_kills);
        self.exempt_ep_id.add_missing_data(&source.exempt_epid);
        self.exempt_former_ep_id
            .add_missing_data(&source.exempt_former_epid);
        self.granted_to_everybody
            .add_missing_data(&source.granted_to_everybody);
    }
}

impl PartialEq<df_st_core::EntityHonor> for Honor {
    fn eq(&self, other: &df_st_core::EntityHonor) -> bool {
        self.id == other.local_id
    }
}
