use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_entity_squad_links;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_entity_squad_links"]
#[primary_key(hf_id, entity_id, squad_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFEntitySquadLink {
    pub hf_id: i32,
    pub entity_id: i32,
    pub squad_id: i32,
    pub world_id: i32,
    pub squad_position: Option<i32>,
    pub start_year: Option<i32>,
}

impl HFEntitySquadLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFEntitySquadLink, HFEntitySquadLink> for HFEntitySquadLink {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_entity_squad_links: &[HFEntitySquadLink]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_entity_squad_links::table)
            .values(hf_entity_squad_links)
            .on_conflict((
                hf_entity_squad_links::hf_id,
                hf_entity_squad_links::entity_id,
                hf_entity_squad_links::squad_id,
                hf_entity_squad_links::world_id,
            ))
            .do_update()
            .set((
                hf_entity_squad_links::squad_position
                    .eq(excluded(hf_entity_squad_links::squad_position)),
                hf_entity_squad_links::start_year.eq(excluded(hf_entity_squad_links::start_year)),
            ))
            .execute(conn)
            .expect("Error saving hf_entity_squad_links");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_entity_squad_links: &[HFEntitySquadLink]) {
        diesel::insert_into(hf_entity_squad_links::table)
            .values(hf_entity_squad_links)
            .execute(conn)
            .expect("Error saving hf_entity_squad_links");
    }

    /// Get a list of HFEntitySquadLink from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFEntitySquadLink>, Error> {
        use crate::schema::hf_entity_squad_links::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_entity_squad_links.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "squad_id" => squad_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "squad_id" => squad_id,
                "squad_position" => squad_position,
                "start_year" => start_year,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFEntitySquadLink>, Error> {
        use crate::schema::hf_entity_squad_links::dsl::*;
        let query = hf_entity_squad_links;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "squad_id" => squad_id,
            ],
            {return Ok(query.first::<HFEntitySquadLink>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "entity_id" => "entity_id",
            "squad_id" => "squad_id",
            "squad_position" => "squad_position",
            "start_year" => "start_year",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFEntitySquadLink],
        core_list: Vec<df_st_core::HFEntitySquadLink>,
    ) -> Result<Vec<df_st_core::HFEntitySquadLink>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_entity_squad_links::dsl::*;
        let query = hf_entity_squad_links
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "squad_id" => squad_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "entity_id" => {entity_id: i32},
                "squad_id" => {squad_id: i32},
                "squad_position" => {squad_position: Option<i32>},
                "start_year" => {start_year: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFEntitySquadLink, df_st_core::HFEntitySquadLink> for HFEntitySquadLink {
    fn add_missing_data(&mut self, source: &df_st_core::HFEntitySquadLink) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.squad_id.add_missing_data(&source.squad_id);
        self.squad_position.add_missing_data(&source.squad_position);
        self.start_year.add_missing_data(&source.start_year);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFEntitySquadLink, HFEntitySquadLink> for df_st_core::HFEntitySquadLink {
    fn add_missing_data(&mut self, source: &HFEntitySquadLink) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.squad_id.add_missing_data(&source.squad_id);
        self.squad_position.add_missing_data(&source.squad_position);
        self.start_year.add_missing_data(&source.start_year);
    }
}

impl PartialEq<df_st_core::HFEntitySquadLink> for HFEntitySquadLink {
    fn eq(&self, other: &df_st_core::HFEntitySquadLink) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && self.squad_id == other.squad_id
    }
}

impl PartialEq<HFEntitySquadLink> for df_st_core::HFEntitySquadLink {
    fn eq(&self, other: &HFEntitySquadLink) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && self.squad_id == other.squad_id
    }
}

impl PartialEq for HFEntitySquadLink {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id
            && self.entity_id == other.entity_id
            && self.squad_id == other.squad_id
    }
}

impl Hash for HFEntitySquadLink {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.entity_id.hash(state);
        self.squad_id.hash(state);
    }
}
