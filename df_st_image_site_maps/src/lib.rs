#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use regex::Regex;
use std::path::PathBuf;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct SiteMapImages {
    pub site_maps: Vec<PathBuf>,
}

impl SiteMapImages {
    /// Create a new world map image collection
    pub fn new() -> Self {
        Self::default()
    }

    pub fn some_found(&self) -> bool {
        !self.site_maps.is_empty()
    }
}

pub fn parse_site_map_images(files: SiteMapImages) -> df_st_core::SiteMapImages {
    let mut site_map_images = df_st_core::SiteMapImages::new();
    let re = Regex::new(r"[\w-]*\w-(?:site_map-([0-9]+)\.\w+)").unwrap();

    let mut sorted_site_maps = files.site_maps;
    sorted_site_maps.sort_by_key(|path| {
        match re.captures(path.file_name().unwrap().to_str().unwrap()) {
            Some(caps) => match caps.get(1) {
                Some(x) => x.as_str().parse::<i32>().unwrap(),
                None => 0,
            },
            None => 0,
        }
    });

    for file in sorted_site_maps {
        let caps = match re.captures(file.file_name().unwrap().to_str().unwrap()) {
            Some(x) => x,
            None => continue,
        };
        let site_id = match caps.get(1) {
            Some(x) => x.as_str().parse::<i32>().unwrap(),
            None => continue,
        };
        site_map_images
            .site_maps
            .push(create_map_image(&file, site_id));
    }

    site_map_images
}

fn create_map_image(file: &PathBuf, site_id: i32) -> df_st_core::SiteMapImage {
    return df_st_core::SiteMapImage {
        id: site_id,
        data: load_image(file),
        format: "png".to_owned(),
    };
}

fn load_image(filepath: &PathBuf) -> Vec<u8> {
    let img = image::open(filepath).unwrap();

    let mut data = Vec::new();

    img.write_to(&mut data, image::ImageOutputFormat::Png)
        .unwrap();

    data
}
